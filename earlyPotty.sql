-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 03, 2021 at 01:10 PM
-- Server version: 5.7.35-0ubuntu0.18.04.1
-- PHP Version: 7.3.28-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `earlyPotty`
--

-- --------------------------------------------------------

--
-- Table structure for table `addcards`
--

CREATE TABLE `addcards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addcards`
--

INSERT INTO `addcards` (`id`, `name`, `session_id`, `card_no`, `card_type`, `expiry_date`, `created_at`, `updated_at`) VALUES
(18, 'Pawan Gupta', '15', '123456789245555', 'abc', '12-12-2020', '2020-12-07 13:42:38', '2020-12-07 13:42:38'),
(19, 'Pawan Gupta', '15', '123456789245555', 'xyz', '12-12-2020', '2020-12-07 13:46:26', '2020-12-07 13:46:26'),
(53, 'Pk', '15', '123456789', 'abc', '10-12-2020', '2021-01-19 13:00:55', '2021-01-19 13:00:55'),
(54, 'bittusingh', '149', '4242424242424242', 'CreditCardType.visa', '12/23', '2021-01-22 10:46:17', '2021-01-22 10:46:17'),
(55, 'bittusingh', '66', '4242424242424242', 'visa', '12/23', '2021-01-22 12:07:17', '2021-01-22 12:07:17'),
(56, 'asdfg', '135', '4242424242424242', 'visa', '12/11', '2021-01-27 12:15:56', '2021-01-27 12:15:56'),
(57, 'Mastertsr', '135', '5151515151151515', 'mastercard', '11/25', '2021-01-27 12:29:49', '2021-01-27 12:29:49'),
(58, 'visa2', '135', '4111111111111111', 'visa', '11/35', '2021-01-27 12:31:34', '2021-01-27 12:31:34'),
(59, 'tst3', '135', '5323535323235355', 'mastercard', '22/55', '2021-01-27 12:32:18', '2021-01-27 12:32:18'),
(60, 'amricanTstCard', '135', '3782822463100055', 'amex', '12/35', '2021-01-27 12:37:37', '2021-01-27 12:37:37'),
(61, 'discoverTstCard', '135', '6011118585757548', 'discover', '12/29', '2021-01-27 12:39:30', '2021-01-27 12:39:30'),
(62, 'testcardholder', '135', '4242422400000000', 'visa', '15/15', '2021-01-29 09:46:47', '2021-01-29 09:46:47'),
(63, 'rrrkkk', '138', '4242424242424242', 'visa', '08/26', '2021-02-01 07:24:41', '2021-02-01 07:24:41'),
(64, 'kkkk', '138', '4242424242424242', 'visa', '22/22', '2021-02-01 07:28:50', '2021-02-01 07:28:50'),
(65, 'gg', '66', '4242424242424242', 'visa', '02/25', '2021-02-06 06:33:47', '2021-02-06 06:33:47'),
(66, 'l;\'', '158', '4564645645645645', 'visa', 'kl/;l', '2021-02-06 07:24:04', '2021-02-06 07:24:04'),
(67, '345345345345345345', '159', '4534534534534534', 'visa', '34/53', '2021-02-06 10:24:27', '2021-02-06 10:24:27'),
(68, 'rrrr', '95', '4242424242424242', 'visa', '02/25', '2021-02-11 11:15:10', '2021-02-11 11:15:10'),
(69, 'mnp', '66', '4242424242424242', 'visa', '02/05', '2021-02-20 05:25:38', '2021-02-20 05:25:38'),
(70, 'Davidlarocque', '154', '4540330601422025', 'visa', '08/21', '2021-02-22 17:53:10', '2021-02-22 17:53:10'),
(71, 'abc', '171', '4242424242424242', 'visa', '12/32', '2021-04-05 05:30:50', '2021-04-05 05:30:50'),
(72, 'hajwjwjw', '179', '4242424242424242', 'visa', '12/31', '2021-04-05 11:07:30', '2021-04-05 11:07:30'),
(73, 'abc', '179', '4111111111111111', 'visa', '12/11', '2021-04-05 11:07:54', '2021-04-05 11:07:54'),
(74, 'drg', '179', '4455586325555666', 'visa', '11/25', '2021-04-05 12:04:46', '2021-04-05 12:04:46'),
(75, 'adfghhd', '174', '4242424242424242', 'visa', '45/42', '2021-04-06 06:09:00', '2021-04-06 06:09:00'),
(76, 'wqa', '180', '4242424242424242', 'visa', '08/25', '2021-04-06 08:18:47', '2021-04-06 08:18:47'),
(77, 'bhj', '180', '4242424242424242', 'visa', '02/10', '2021-04-06 08:25:38', '2021-04-06 08:25:38'),
(78, 'mxhsh', '181', '4242424242424242', 'visa', '08/25', '2021-04-07 04:42:23', '2021-04-07 04:42:23'),
(79, 'fsfa', '181', '4242424242424242', 'visa', '12/34', '2021-04-07 04:42:51', '2021-04-07 04:42:51'),
(80, 'ankita', '181', '4554452535535323', 'visa', '08/25', '2021-04-07 07:02:37', '2021-04-07 07:02:37'),
(81, 'ass', '185', '4242424242424242', 'visa', '02/25', '2021-04-08 04:22:59', '2021-04-08 04:22:59'),
(82, 'ankita', '179', '2587425632896358', 'mastercard', '12/25', '2021-04-14 06:02:02', '2021-04-14 06:02:02'),
(83, 'dyfyg', '179', '5214856932558235', 'mastercard', '12/22', '2021-04-14 06:14:01', '2021-04-14 06:14:01'),
(84, 'dddddd', '179', '2354158963258452', 'mastercard', '23/22', '2021-04-14 06:17:00', '2021-04-14 06:17:00'),
(85, 'gsgsgs', '179', '4563289946665859', 'visa', '11/25', '2021-04-14 07:59:07', '2021-04-14 07:59:07'),
(86, 'asd', '187', '4242424242424242', 'visa', '12/25', '2021-04-15 05:20:18', '2021-04-15 05:20:18'),
(87, 'asdf', '187', '4242424242424242', 'visa', '08/2', '2021-04-15 06:16:17', '2021-04-15 06:16:17'),
(88, 'sdd', '188', '4242424242424242', 'visa', '08/25', '2021-04-15 10:38:00', '2021-04-15 10:38:00'),
(89, 'ffggHB', '179', '4111111111111111', 'visa', '12/25', '2021-04-16 05:56:39', '2021-04-16 05:56:39'),
(90, 'tsdName', '67', '4111111111111111', 'visa', '12/25', '2021-04-16 06:41:38', '2021-04-16 06:41:38'),
(91, 'visatsts', '67', '4242424242424242', 'visa', '12/25', '2021-04-16 06:49:19', '2021-04-16 06:49:19'),
(92, 'ankita', '67', '4563258984558777', 'visa', '12/28', '2021-04-16 06:52:48', '2021-04-16 06:52:48'),
(93, 'ankitasingh', '191', '4441588655996644', 'visa', '11/24', '2021-04-16 07:12:49', '2021-04-16 07:12:49'),
(94, 'gurpreet', '174', '4163328874255889', 'visa', '14/26', '2021-04-16 07:22:29', '2021-04-16 07:22:29'),
(95, 'dff', '192', '4242424242424242', 'visa', '08/25', '2021-04-16 09:35:51', '2021-04-16 09:35:51'),
(96, 'abctstName', '135', '4242424242424242', 'visa', '12/25', '2021-05-07 05:47:51', '2021-05-07 05:47:51'),
(97, 'ankita', '198', '4242424242424242', 'visa', '12/25', '2021-06-04 06:21:48', '2021-06-04 06:21:48'),
(98, 'ankita2', '198', '4242424242424242', 'visa', '12/30', '2021-06-04 07:11:27', '2021-06-04 07:11:27'),
(99, 'zjh', '200', '4242424242424242', 'visa', '02/25', '2021-06-04 09:29:10', '2021-06-04 09:29:10'),
(100, 'ankita', '200', '4242424242424242', 'visa', '08/25', '2021-06-04 10:05:42', '2021-06-04 10:05:42'),
(101, 'KumarKumar', '204', '4242424242424242', 'visa', '12/3', '2021-07-30 19:12:38', '2021-07-30 19:12:38'),
(102, 'SampleUser', '204', '4242424242424242', 'visa', '05/12', '2021-08-02 12:16:53', '2021-08-02 12:16:53'),
(103, 'hhhhsbs', '204', '5231684649493494', 'mastercard', '99/66', '2021-08-02 12:17:21', '2021-08-02 12:17:21'),
(104, 'bhavesh', '205', '4242424343546465', 'visa', '01/25', '2021-08-02 12:28:34', '2021-08-02 12:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profilePic` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `password`, `profilePic`, `gender`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'demo admin', 'e6e061838856bf47e1de730719fb2609', 'iINOBQ4tbL_IMG_0178.JPG', '', 'admin@mailinator.com', '654656654', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_logins`
--

CREATE TABLE `app_logins` (
  `id` int(255) NOT NULL,
  `user_id` int(30) NOT NULL,
  `session_id` text NOT NULL,
  `device_type` varchar(50) NOT NULL,
  `device_token` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_logins`
--

INSERT INTO `app_logins` (`id`, `user_id`, `session_id`, `device_type`, `device_token`, `created_at`, `updated_at`) VALUES
(11, 15, 'fDVXc5PCu47hB8KXwoNV9speV4o6I7Th', '1', 'rtyrt', '2020-11-16 06:54:43', '2020-12-01 07:25:11'),
(12, 17, 'DqrlLUOHcjA2gzUG15Dp0U7AB3voCLYf', '1', '1233', '2020-12-02 06:27:22', '2020-12-02 06:27:22'),
(13, 18, 'oTU7nxpWQZbrmU55ru3dXroIApdJW6Vd', '1', '1233', '2020-12-02 06:27:55', '2020-12-02 06:27:55'),
(14, 19, 'uV5Ncjj7iDNKjcG5JcNZF1lizDWpozOR', '1', 'rtyrt', '2020-12-02 08:49:59', '2020-12-24 10:51:06'),
(16, 15, 'Pva0uI7joo4ZRRxkvfXvAyp7QknnFCMt', '1', 'rtyrt', '2020-12-02 11:32:03', '2021-01-28 07:11:45'),
(17, 20, 'HKAUkkcfezzTCnS0MEAuXvKq36esiCqD', '1', '1233', '2020-12-02 13:09:27', '2020-12-02 13:09:27'),
(18, 21, 'ozxKH1FvW6j0XK6vj7rQ1lOTLp5Y07MU', '1', '1233', '2020-12-03 13:54:49', '2020-12-03 13:54:49'),
(19, 22, '9n7UEs8SJpxnDROsfWfTIO7ubsdd2AL4', '1', 'rtyrt', '2020-12-05 09:35:38', '2020-12-05 13:50:19'),
(20, 23, 'DKYOdlm114pDN9xukKw35qBvKuRYF2ao', '1', '1233', '2020-12-05 18:11:55', '2020-12-05 18:11:55'),
(21, 24, 'aGqJ5FHZ1OC6iovxyOHzesC2MrioOD4A', '1', 'rtyrt', '2020-12-05 18:26:18', '2021-01-05 13:14:53'),
(22, 25, 'NLokZJN4kvvo5sYFGukzRjhLp1Fn60Os', '1', '1233', '2020-12-06 18:35:52', '2020-12-06 18:35:52'),
(23, 26, 'aLmjDxeNIFugAffZtxhHOEoBIkYEeyZU', '1', 'rtyrt', '2020-12-07 06:29:29', '2020-12-07 06:30:23'),
(24, 27, 'jp5k0a2n6FBnTbpSyIYgEXu7MplegWTx', '1', 'rtyrt', '2020-12-07 06:30:48', '2020-12-07 06:31:20'),
(25, 28, 'zZaPBoD0Cg0DosHdMMuozKh41D4L7V6K', '1', 'rtyrt', '2020-12-11 09:15:19', '2020-12-14 11:24:49'),
(26, 29, 'wYMJLwH68hv2Wre1OJxAhPmtaOY6g68n', '1', '1233', '2020-12-11 10:11:35', '2020-12-11 10:11:35'),
(27, 30, 'VfBysRJ2QmN65V1slJggXcvZ61ODnTKd', '1', '1233', '2020-12-11 12:06:39', '2020-12-11 12:06:39'),
(28, 31, 'ooLW6uJ1I1al5KUqpYU0XM2sZgdMxGAU', '1', '1233', '2020-12-11 12:49:10', '2020-12-11 12:49:10'),
(29, 34, 'yuEtJPg0kusDzKB75VOk6rQTuZFr5vgT', '1', '1233', '2020-12-11 13:20:01', '2020-12-11 13:20:01'),
(30, 35, 'dPd13MsEupdblQWvJdDbEmLuFjUuhfyu', '1', '1233', '2020-12-11 13:37:12', '2020-12-11 13:37:12'),
(31, 36, 'yWHc77vxZQ0r93zPnZEwHx0wAhKenGRK', '1', '1233', '2020-12-11 13:37:46', '2020-12-11 13:37:46'),
(32, 37, 'Kd31liUyfcJC4Fm3mTupIiKx7RsLnBvL', '1', '1233', '2020-12-11 13:37:49', '2020-12-11 13:37:49'),
(33, 38, 'hWXQesOZ7TRQrB06MDWzSunjKxxS8mRp', '1', '1233', '2020-12-11 13:37:56', '2020-12-11 13:37:56'),
(34, 39, 'qDetoeoUGlq2fDYdrGa0d7s0nwwgZWN2', '1', '1233', '2020-12-11 13:38:14', '2020-12-11 13:38:14'),
(35, 40, '9UqvFP1wwFawTD8Jk2MIjavkprXG9UQ3', '1', '1233', '2020-12-11 13:38:21', '2020-12-11 13:38:21'),
(36, 41, 'w0Cecvzm2kMQD3phGZylu5QjJac7FR1D', '1', '1233', '2020-12-11 13:39:45', '2020-12-11 13:39:45'),
(37, 42, 'cAVmOc50AulsVORf7bTKz4OgqLLp7A6n', '1', 'rtyrt', '2020-12-11 13:48:08', '2020-12-24 10:19:46'),
(38, 44, 'coG0rfRODwvrJOOzEnhI0wQEU27hA5py', '1', '1233', '2020-12-11 13:48:20', '2020-12-11 13:48:20'),
(39, 45, 'j2IXSRnjClxNMpHOq0fAtHXtHOHBRGnl', '1', '1233', '2020-12-11 13:48:24', '2020-12-11 13:48:24'),
(40, 46, 'DHOAB3klyldKfbo5ESe6qvITFCotuZe2', '1', '1233', '2020-12-11 13:49:18', '2020-12-11 13:49:18'),
(41, 47, '4099LuqSluBAqxqPNkDpwGCB7p2sauE1', '1', '1233', '2020-12-11 13:53:48', '2020-12-11 13:53:48'),
(42, 48, 'Opb9scwaoI3fwDpcHm0IVm42CWioibSc', '1', '1233', '2020-12-11 13:53:49', '2020-12-11 13:53:49'),
(43, 50, 'TYSBKKdy45FgG0McLnls02Y6r331xIjq', '1', '1233', '2020-12-11 13:54:06', '2020-12-11 13:54:06'),
(44, 52, 'ow91HEsGTlbshTAin67eblP8bADpKT0F', '1', '1233', '2020-12-11 13:54:40', '2020-12-11 13:54:40'),
(45, 53, 'NWKq1iWKASXBqAh7fR6eLa4zsOksWEsn', '1', 'sfsdfsdfdfdsf', '2020-12-22 14:07:47', '2020-12-22 14:07:47'),
(46, 55, 'LIQ4AC2N8lQJ10KRme3QusELjRE0SnR2', '1', 'sfsdfsdfdfdsf', '2020-12-23 09:55:22', '2020-12-23 09:55:22'),
(47, 56, 'ha3qlgObnMpWztzrgvbBWeMW0A0b3pWa', '1', '1233', '2020-12-23 10:35:14', '2020-12-23 10:35:14'),
(48, 61, 'GjacctfzV5e5LvrsRd65OjDYqc69W7W8', '1', '1233', '2020-12-23 10:40:15', '2020-12-23 10:40:15'),
(49, 63, 'nSlP3S8ig02Q4F4NfqRQY7fL7CRlFToZ', '1', '1233', '2020-12-23 10:57:12', '2020-12-23 10:57:12'),
(51, 67, 'kiFayGSsoBjVCGQJ99sWjLRDBMYOCc4K', '1', 'eer19vx9S_qb9FNONVXfqZ:APA91bHhY-HeUayBEx0zgA6VbUXno9KGU30Yr3nydEF7k4hTRSkcmEazJvQeRGabKbUhEfh889qKWG93Bqd_sCOexM_oI0vEhC_NVPgSA18jcSRKWYSVfTS8_8Hs5UqKX5-bFBZFAUxk', '2020-12-24 07:26:05', '2021-04-16 06:30:50'),
(52, 68, 'Al0BBkGFfqBhTUsqyQ7VJBPGtGq3FOza', '1', 'sfsdfsdfdfdsf', '2020-12-24 14:04:03', '2020-12-24 14:04:03'),
(53, 69, 'Rzs5kBvP4h9VQoNo0fRRVbOk4m2BmiFe', '1', 'sfsdfsdfdfdsf', '2020-12-24 14:34:21', '2020-12-24 14:34:21'),
(54, 70, 'DODZnspTOmQsF7oYIPJdVCYbUFKr7BpJ', '1', 'sfsdfsdfdfdsf', '2020-12-28 05:07:27', '2020-12-28 05:07:27'),
(55, 71, '39p0DhIdXRCwxHZ1qpuQjDZL2rUadJQj', '1', '1233', '2020-12-28 05:24:38', '2020-12-28 05:24:38'),
(56, 73, 'K5fSwWQwWwbMWjMMH53sROoXTNhKvQEL', '1', '1233', '2020-12-28 05:25:19', '2020-12-28 05:25:19'),
(57, 66, '2sy93o5rGhEtXEWFw2pb1ExuyX1MF2fA', '1', 'cDr5U5t7hmU:APA91bFjsqFioUJuXG4uuT7FF2JnFo8Nb-BFEsrFSThd2oN8YD-GF57jz44PaUSjhGC0sQhHVZen1Owi-zSe3Wb4n0QxoxsykVjgzTnlx3IvSYLW_UnDbQ9ptcty_gbCW2WBxJu2gy2o', '2020-12-28 07:04:17', '2021-02-26 12:10:00'),
(58, 75, 'tnPcbVItZtGKatz76lPgtZbTSZTafpDl', '1', '1233', '2020-12-31 12:22:54', '2020-12-31 12:22:54'),
(59, 77, '8GFlBpsDLUHAcEfYPWu1egAJRV27GthK', '1', '1233', '2020-12-31 12:25:49', '2020-12-31 12:25:49'),
(60, 79, 'NR2L7GWrz40Fw90FGQjAccmoXDJTmDVf', '1', '1233', '2020-12-31 12:30:45', '2020-12-31 12:30:45'),
(61, 80, 'vgxavYzwkIvtn5rhfygpFg2jH46AVwES', '1', 'sfsdfsdfdfdsf', '2021-01-01 11:07:50', '2021-01-01 11:07:50'),
(62, 81, 'gkCsAejTOmKbxp9ywsLiq1Q1TbmUAMgW', '1', 'sfsdfsdfdfdsf', '2021-01-01 11:28:13', '2021-01-01 11:28:13'),
(63, 82, 'KGzXMrwFUyjQmj11n7EiUdVCeZQ3B5kn', '1', '1233', '2021-01-01 11:29:28', '2021-01-01 11:29:28'),
(64, 83, 'qpLPNhcPMbUmcKlXoizOUfqPHfmH1nDq', '1', '1233', '2021-01-01 11:31:14', '2021-01-01 11:31:14'),
(65, 84, 'mqoUt0zKxf1l9TWbczImrOMzlsJTD5eo', '1', 'sfsdfsdfdfdsf', '2021-01-01 11:49:15', '2021-01-01 11:49:15'),
(66, 85, '3NE7gum89yiiYQCMB3v7s787BKpW27Ue', '1', 'sfsdfsdfdfdsf', '2021-01-01 11:53:07', '2021-01-01 11:53:07'),
(67, 86, 'U7dksINccwlcy4KdE30tYEujosVmxnr8', '1', '1233', '2021-01-01 12:17:42', '2021-01-01 12:17:42'),
(68, 87, 'YdKKQXtpE1y7r0Ort3HPhz6EWREIsFZy', '1', 'sfsdfsdfdfdsf', '2021-01-02 09:59:00', '2021-01-02 09:59:00'),
(69, 89, 'OAN9AnyJj1K9Y9PdtPEbutD6Y4uFsUjs', '1', '1233', '2021-01-02 10:05:16', '2021-01-02 10:05:16'),
(70, 92, 'raiED0tjCPk0Mu5uDH77aigNiwFbMFDw', '1', '1233', '2021-01-02 10:09:14', '2021-01-02 10:09:14'),
(71, 93, 'YDpEhkDTX8bNrbWZFfwtUMjg0xZhnEjX', '1', '1233', '2021-01-02 10:11:42', '2021-01-02 10:11:42'),
(72, 94, 'KW1tCVy83zq579KfOXhizjztmMC0oOJO', '1', '1233', '2021-01-02 10:11:55', '2021-01-02 10:11:55'),
(73, 95, '8SSXjubVSFho4uan83NqzPWdTllBFALy', '1', 'sfsdfsdfdfdsf', '2021-01-02 10:15:53', '2021-02-23 05:19:26'),
(74, 96, 'OmTXVOaJMKJNoFDOltb3wnbokpFhEyu7', '1', 'sfsdfsdfdfdsf', '2021-01-02 10:26:46', '2021-01-02 10:26:46'),
(75, 97, 'xRKatcWwnnHYxulzvPVxr2fWMX1ZMqnY', '1', '1233', '2021-01-02 10:27:49', '2021-01-02 10:27:49'),
(76, 98, 'fKzZy8zvGSnEnPyMwANbwef3wc4AaLsq', '1', 'sfsdfsdfdfdsf', '2021-01-02 10:39:58', '2021-01-02 10:39:58'),
(77, 99, 'qU0vLK1ZLIGhNZ7y4msDyOgc3eqrJHnb', '1', 'sfsdfsdfdfdsf', '2021-01-02 10:45:34', '2021-01-02 10:45:34'),
(78, 100, 'vgMqmVe3ZFPL1dZHfx6ssdrZsm3Gs0wk', '1', 'sfsdfsdfdfdsf', '2021-01-02 11:20:32', '2021-01-02 11:52:43'),
(79, 103, 'xAtSbV7P9O7pel2yeVbeGdxRoKAmxtqa', '1', '1111111', '2021-01-02 12:22:36', '2021-01-02 12:22:36'),
(80, 104, 'BhWaAxLKutzqS4QJp3gZ7Z4CTeAv7qbl', '1', '1111111', '2021-01-02 12:24:45', '2021-01-02 12:24:45'),
(81, 105, 'Gy3u9mK0Ss4BCQZFAOe0tDAdEu9GyRxF', '1', '1111111', '2021-01-02 12:35:40', '2021-01-02 12:35:40'),
(82, 106, '1noaN6021P2ydnB5rkBtgtxMpOEToURv', '1', '1233', '2021-01-02 12:37:05', '2021-01-02 12:37:05'),
(83, 107, 'fB9kFLbyYuuq2Gq731eLkvsNDnowEjlK', '1', '1111111', '2021-01-02 12:49:31', '2021-01-02 12:49:31'),
(84, 108, 'aGqm9PjABoJxAP3ESaZXDdD4OG9yHMRW', '1', '1111111', '2021-01-02 12:49:37', '2021-01-02 12:49:37'),
(85, 109, 'Q5q7EhxAybOdRwl5cwvSTjJcKiVBKDMT', '1', '1111111', '2021-01-02 12:49:58', '2021-01-02 13:42:37'),
(86, 110, 'eJp8iwmjAm14WcVTL5FtexxZqTBLeU4e', '1', '1111111', '2021-01-02 12:51:18', '2021-01-02 12:51:18'),
(87, 111, 'os6p1wmyhWlCm8huypUrH8EhlsWPr7SM', '1', '1111111', '2021-01-02 12:52:53', '2021-01-02 12:52:53'),
(88, 112, 'AgnTdveQbseBmBEMhsYNfAvIcKHixsdp', '1', '1111111', '2021-01-02 12:57:24', '2021-01-02 12:57:24'),
(89, 113, '65N7ZLB98c8zwAeC3D2VbLRXXeFukM9x', '1', '1111111', '2021-01-02 13:01:12', '2021-01-02 13:01:12'),
(90, 114, 'vE2xjRNwd8WDtH52sdiR4AjJIn9tOQRG', '1', '1111111', '2021-01-02 13:01:29', '2021-01-02 13:01:29'),
(91, 115, 'VerLcDlanMJ3RHAS9Qrufbj0Zs8Vrfvm', '1', '1111111', '2021-01-02 13:09:50', '2021-01-02 13:09:50'),
(92, 116, 'aocgIbYnG02JCaoUHlxf8x0Kibk5Efcl', '1', '1111111', '2021-01-02 13:11:13', '2021-01-02 13:11:13'),
(93, 117, '4xb2GmeRuYhvP7qcdLf79sboitsugAW1', '1', '1111111', '2021-01-02 13:14:17', '2021-01-02 13:14:17'),
(94, 118, 'AlTsw4WbEKSsTcDJ0zRD68wLBbSPa7ja', '1', '1111111', '2021-01-02 13:17:28', '2021-01-02 13:17:28'),
(95, 119, 'DFcyL74CEbwmOnuA7mTEhqDbeqlQzjcp', '1', '1111111', '2021-01-02 13:18:19', '2021-01-02 13:18:19'),
(96, 120, 'rOS82nwMjwtDqqpt4XJTPKFQUQjJ2ICD', '1', '1111111', '2021-01-02 13:32:33', '2021-01-02 13:32:33'),
(97, 121, 'VrHHNQF2YO21HI1iGerBGvj4iAXeHYKe', '1', 'sfsdfsdfdfdsf', '2021-01-02 13:44:36', '2021-01-02 13:44:36'),
(98, 122, 'bZlwYEeOMqG4PB3qf5tFFJcm9NzQnZRv', '1', 'sfsdfsdfdfdsf', '2021-01-02 14:03:14', '2021-01-02 14:03:14'),
(99, 123, '1LMcujmNgSmJrAlkLWiGpEldLyrdbr0u', '1', 'sfsdfsdfdfdsf', '2021-01-02 14:10:20', '2021-01-02 14:10:20'),
(100, 124, 'nOLzAAaLcL2lrumCzjRWBjHBdtiZ0RUe', '1', '1111111', '2021-01-02 14:10:32', '2021-01-02 14:10:32'),
(101, 125, 'Z3TgHRCkBRYhNaivryOBrrKnsZCGeJzr', '1', '1111111', '2021-01-02 14:10:39', '2021-01-02 14:11:43'),
(102, 126, 'q7cJp5bu48fQzlHLPBvni692DJo1aMoh', '1', 'sfsdfsdfdfdsf', '2021-01-02 14:12:42', '2021-01-02 14:13:41'),
(103, 127, 'U8qW0hOP18qS61Kxmv0vswfSioEr2pPo', '1', '1111111', '2021-01-02 14:15:47', '2021-02-02 11:22:49'),
(104, 128, 'mr1AUWCrOuWp5f8mLX2qsRm1s3be73Ue', '1', '1111111', '2021-01-02 14:21:32', '2021-01-02 14:22:29'),
(105, 129, 'fvWOQ0TGTMzZN4bDma7XvwkJmy6eavug', '1', '1111111', '2021-01-02 14:22:50', '2021-01-02 14:23:44'),
(106, 130, 'h7slhItw69z8jZAm8NMlD7PmW3JjiLdB', '1', '1111111', '2021-01-02 14:24:00', '2021-01-02 14:24:07'),
(107, 131, '2qwnFcQdPTjwsFVD6pWzDxOVB0ncnx5u', '1', '1111111', '2021-01-02 14:24:13', '2021-01-15 13:07:43'),
(108, 132, 'm4HbaanBwj19NfygIygr9zS23Gog2U3O', '1', 'ffWt7QCQUCQ:APA91bGwMFuWnNVRmB2beucxlYOW6brvN2Xy-rqbfcxCawnov2TNEp6Mw5Yel3EI0vLNoGd2BbgMzH34KfEuidDdWUTUxjDXzxfjF2lbfgfKxreHWEa_v79bUjRZ9ssKSFrt4Sx804JY', '2021-01-04 11:51:23', '2021-01-04 11:52:36'),
(109, 133, 'BoKivD0OTGiiuO9VYDHXfJhEnMwh7c3X', '1', '1111111', '2021-01-04 13:05:11', '2021-01-07 05:51:17'),
(110, 134, 'rkQTyNH9UggNQSVVvz6EeJ7BMvwlJXVo', '1', '234234', '2021-01-04 13:05:36', '2021-01-04 13:05:36'),
(111, 135, 'yXrUEEg3hvgUBrg5P9SbqGk6n4ZqQqR2', '1', 'dexjFzXLRpKDiJlXcSg6If:APA91bEmzTgyHI-wBia-Ei6TA8zeRTYGbG9L-0TiTzsZOyu9GuPoVRU-MffGtwFuqYY2yM6IlvK4iJp-Pqr_yR8JtAbastVa5pqKS0-F-_C75jB7mDg201nUaUZ_RruN-TQ1XYrIAYSG', '2021-01-07 06:09:38', '2021-05-07 05:45:02'),
(112, 136, '7RRnbSD8euGccibYcI81RUOczoa4KrTd', '1', 'sfsdfsdfdfdsf', '2021-01-07 10:33:22', '2021-01-07 10:33:22'),
(113, 137, 'Ff27tkGIicUnACUtCpbCDCkRJZVBwU3f', '1', 'cQ8DctsACoc:APA91bHRRnd3JTzjUVlq4M6YySh_T-y9r1AEX5o9ckmPxzLWi9QDLpjKkQIkE50uVfIb5o_TGsjvSOI6cn3NstZqIwaYOceTDtYmVSNCrvjUZdXKvoRhdcLimWnvHo8sMbXEwkl9mluX', '2021-01-08 11:36:14', '2021-07-09 04:09:01'),
(114, 138, 'Qrq9IPvDBVN50KBaopuQJPNsA8i4DZ7h', '1', 'f5K57iQMw1w:APA91bF0sUZXgh-YZBVoTZkGi3RvYOuzcJTeFgxmEClPHIyO3Bnjx62vPXOtO0WbT-nDCTv6dJsApG7dSHLar0Z1v4RdgX_yztbk_yx6Xqdc8QcWHSZPsC9J1ZhtoQNzMhxXI6OipxUC', '2021-01-08 13:38:35', '2021-02-01 06:10:35'),
(115, 139, 'mqntrWeW9IKyYgGILlaW0xA3N7xD3c96', '1', 'f-BBJwsQ6Vk:APA91bEEfrevG8GVFquexCk90dmAaPz64BuByg72Y83NBHkIDfgGZSyXJPkM04vrDIS7ALlJDnNmM3lR-bWvK3rTUOZcjF1YGcizv7kVB7LRY4lxv_AUOCy3s5otWZNcuBnERrZxDOOn', '2021-01-08 15:17:38', '2021-02-19 15:05:34'),
(116, 140, 'tiSQj1rvr2WBh8rbfYWLZqHdJFCFD77z', '1', 'sfsdfsdfdfdsf', '2021-01-08 18:11:20', '2021-01-08 18:11:20'),
(117, 141, 'svNSXpSKOFKZPjJ4NDd4FfjdCpItiWf9', '1', 'sfsdfsdfdfdsf', '2021-01-08 19:39:56', '2021-01-08 19:39:56'),
(118, 142, 'VVrMgAJ1tDlxnZCDmF2224V0d6zYhqa5', '1', 'sfsdfsdfdfdsf', '2021-01-08 19:50:48', '2021-01-08 19:50:48'),
(119, 143, 'DYbUJdfnp9SVYmAo3o5dz2S4vFxxckjV', '1', 'sfsdfsdfdfdsf', '2021-01-08 21:33:57', '2021-01-08 21:33:57'),
(120, 144, 'nvsBE9RametkwKeyRCQTsZFIDCdQPRWN', '1', 'sfsdfsdfdfdsf', '2021-01-08 21:39:22', '2021-01-08 21:39:22'),
(121, 145, '9Hf9AlZPuE2gnQpnvxzHutPWkqbNbUyG', '1', 'sfsdfsdfdfdsf', '2021-01-11 06:15:55', '2021-01-11 06:15:55'),
(122, 146, 'PnVve6Mt6OqLQCrxDBP65rWE0bEiUDtM', '1', 'fTMo5dcRT7O2s8-c7K_ARu:APA91bEgWjFG1DCDX9HkM76yr333EqDdtzjkPeRlIh3xVi2QUoTafdQhHT0-_qcO9elHAY4yFyUnzcxShGndtGc9mXfPE0p8MxFzKXiXnzr7VRVxto9TuFTzaU9QlVTvgrlsO-ID5XON', '2021-01-11 11:40:17', '2021-04-05 11:45:51'),
(123, 147, 'mFwvLMMp82SjH9uFEfcueySyNfxPrSBa', '1', 'erQ7WWPVdLU:APA91bGrodgqz75mHyyRuY8ZrsK2AUw6k1ZPMNuJlzH8YvhFIE2bOp82OMxQUwj21OL-O5E-i_4jDcwPczY-vK3zzXp1Nn6U2qlsCyAmoWnKRUqyvh91R7ZUEGoKGvd671lwf-E3vg3q', '2021-01-15 12:50:23', '2021-07-09 07:18:32'),
(124, 148, 'ySMp8sEG6qVUYjSQycLj2t3GZQhflPms', '1', 'sfsdfsdfdfdsf', '2021-01-16 01:34:47', '2021-01-16 01:34:47'),
(125, 149, 'YijmAyleOlfpK9gnqP1jyP5HEFRdogg2', '1', 'sfsdfsdfdfdsf', '2021-01-16 06:44:33', '2021-01-16 06:44:33'),
(126, 150, '5RAeX8Hom7MANhqJMYinDSWCJZBuXVVF', '1', 'cosXTRzo98M:APA91bGJ_v_NBB_70L3wXKtUPSO67cZ3HO2vc0RxDzUEuKu_XsRXxB1-rgyd9JZ9DEI-sNhY0d502YvEMETLNNVdoLYQ3BHxLIM8U0kB7EOq127Nh_W6nRu0aiOAvDCg8hS_E74AQVCm', '2021-01-22 15:09:50', '2021-01-22 15:09:50'),
(127, 151, 'l06x16LnzviNK4s8fsQdQYly98psshxS', '1', 'sfsdfsdfdfdsf', '2021-01-25 06:30:58', '2021-01-25 06:30:58'),
(128, 152, 'r0HMW168NwLlLOp3yq46ykcipwLW7WKb', '1', 'dpclGp3DPYM:APA91bErH5W8ILBF9iBAfpeWGr7iMDRP1mJCb-P2JYdAUZUT4RK5tpZ3N3OcH0QaH5LtW1jxm_11WKp4f1OOK1-EoKm9GGIG9wsTo2JcvuKz6JiYfcVRLfT4RF6fyX9bx6XyomLKV8CK', '2021-01-30 04:40:38', '2021-03-01 04:01:10'),
(129, 153, 'enfaWDZ9NpNWkIonAoLvwypv6dm2h0ME', '1', 'sfsdfsdfdfdsf', '2021-02-03 12:31:20', '2021-02-03 12:31:20'),
(130, 154, 'Eau6ZS3xPiNEyQL9JM7rnEC2C3ZohWYp', '1', 'edYR7v_URDE:APA91bHBGVC9Ynst1NF5Cg4ndTCSxtcaeN0DgfcjQdZNIGgmN-BtPVj9hLVxjll855HyX0Krto7DYCTj2xqGjhk0dgQp_YgIQI2bPG2aNAiW241G35CWQeByqYYz6CyOtG3PSMYBFg02', '2021-02-03 21:36:11', '2021-07-09 18:04:18'),
(131, 155, 'B8C9sLO8G0wdgipFZaKtPO4WhTPKYq5e', '1', 'sfsdfsdfdfdsf', '2021-02-05 06:09:52', '2021-02-05 06:09:52'),
(132, 156, 'RDXwN8W8SyegsPqgd09stAkHEs8XMrFt', '1', 'rtyrt', '2021-02-05 13:38:14', '2021-02-05 14:02:23'),
(133, 157, 'fne28d87VvyJljtvCUi2R2WJahwxiY04', '1', 'sfsdfsdfdfdsf', '2021-02-05 20:13:26', '2021-02-05 20:13:26'),
(134, 158, 'cez7XhBkMFV8tX0IwWLj16WyyKJHSDpj', '1', 'sfsdfsdfdfdsf', '2021-02-06 06:33:21', '2021-02-06 06:33:21'),
(135, 159, 'NIqw0YHs7iimSgR7Dud9DTerYd3tczH0', '1', 'cgA25veEz7A:APA91bHanTJs90TJ5RW8ySml4-5886XOLHKc5M5QCv14FSnZ_DN8cD3ZqcI1fiz1xCTXKDOhn-nxBGsprAPWmXBUblCoFVTlHpy3FSEp5PHh0uja4RV9fFiVaHVvjVSfsfbw_tqVIcvj', '2021-02-06 07:30:39', '2021-02-11 10:45:16'),
(136, 160, 'DO3zJvIjNavZsAEE7DheECsT5ZnKtjIu', '1', 'chK4cBruDc4:APA91bFfHSBhhax02pm6ATmuTbSmjGyLhT_tzVVU47oSC-u7k1t0HeN_NfkcVrGgm1EhC3brCqNsAXU808_wXBaA6Haunv9Fh7JBm7bwwH5Slc0he7aM9_AiRsw1-Ez19GW7CdbV8KyL', '2021-02-10 09:43:46', '2021-03-15 13:27:19'),
(137, 161, 'APeue7hcfL9Ry4I5o58KRVsF6TxfrZVP', '1', 'sfsdfsdfdfdsf', '2021-02-11 10:50:51', '2021-02-11 10:50:51'),
(138, 162, 'PYDMCUd8zq1020bEkhttPDRgPnhddAc3', '1', 'f-B5St9dFnk:APA91bG4uQiqkvhDw_ven0urYJnFHYlUMnPU7Iaeyo0NBkqfr_YxrpiTFeYqoNLYAR1-yG_9EuVy-BomtjmNXfN_Zz744JATB4v5TfbLUVgBU-ciuyBQgoNFZfD_1peYsubxk0l-kJT3', '2021-02-11 15:14:17', '2021-02-12 15:38:56'),
(139, 163, 'rCRgl008Ltu9gUMgpSN1wXcWEpi839qS', '1', '1233', '2021-02-23 09:43:08', '2021-02-23 09:43:08'),
(140, 164, 'OxbNNkFP5az6vruubY79xWjXBishUqPY', '1', 'fPG1ixk1RICumxynbH94qg:APA91bFfmO14svmQ-oo5I93XElZV9QULpQtO1SXXT1fLcSQaAWBPBblbyU7P3IXlNV5AYk2R5vxNswA11O_ZfPAly_9jXsWFXGWkwn22qfNIuZnTw3lfh1iPL0mKrzTKFPweRZMjcMic', '2021-03-17 05:54:51', '2021-03-25 08:31:10'),
(141, 165, 'hYy0gLUZex6jb53gOOyYKCNcc7FwvrNd', '1', 'cqdvF0i94qg:APA91bHd9FSKynFwt6-aEwBmQYaS5X6By59Qig4KWvHeS49N0NCGONJAzGXOGK_HZ9_TXX-CzeFOgfCB8a7Yb3COx-dZftqDXokrCG1avPZ_vIpzLksEAPunRAKlzJEh_eQuaV75dKQb', '2021-03-18 11:08:21', '2021-03-18 11:08:21'),
(142, 166, 'YDJnFH0X9jZux91GCa8wgnJ9gly3C5Bt', '1', 'fAk_r87zTmOA9AJjxypg7w:APA91bEdgVT4TqPDunfYsE2DcT0w47epFAsSqVuPfo5uKvEfcromvcuUOtrLATgzbaltxR4bm9XZjtZWLe0VTil2T6SAP4SGGv9jjPuoALdz-Ba17ABXL9_tTdHjcSf1HGSGpYUCV7Gh', '2021-03-19 09:31:17', '2021-03-19 09:31:17'),
(143, 167, 'ROGO7iOKm4BhHZE4p8BiFD2r4YZ0TDmH', '1', 'sfsdfsdfdfdsf', '2021-03-19 10:30:02', '2021-03-19 10:30:02'),
(144, 168, 'LDkQYeKEmlbMToAndCFBvsPQudewWRPt', '1', 'sfsdfsdfdfdsf', '2021-03-25 09:47:43', '2021-03-25 09:47:43'),
(145, 169, 'l2oTZGKTVuYyOpf51Chlhvnr4pQIAWuh', '1', 'sfsdfsdfdfdsf', '2021-03-25 12:26:53', '2021-03-25 12:26:53'),
(146, 170, '4fWd9d7GL8gs8NGEJXtM4CQT5Rw1WLET', '2', 'cQj7xOfLxEcWjY7XkUvpIg:APA91bG497TKACXsWui_yZvV8cJ02xtfzBxgeQpJ20AglKN7AJvDxFotqeZiRggPwr8AaKpNuvoQY7QUrAJKkAsG_z2cqh1nG6nYg2MXlqcOEghqZjGg0gqLZwutBSR8vy0yNcHstNX7', '2021-03-25 12:54:46', '2021-03-30 07:13:57'),
(147, 171, 'XFyjJXf4qoouYV3rj7kyrg9AjmeW7z8N', '2', 'fMYqxgBbXEiMlh1ODW5NEq:APA91bEqcb9-Hmz2eO_9PvhokVHC-tVFgYxp5_TprLHif_T9lgtlRuFPUbuktZmb-BZG84wRz5eYVSNqlaZZubGVAJkDQjE_cqS7DzGWCVSlUV6FVg7RXSAAFhBJdgIG7fZer5qIfvO0', '2021-03-30 09:36:57', '2021-04-05 06:52:02'),
(148, 172, 'o2TaMEJ9dROZ7WW6WXbHhJfZNDMjrUA7', '1', 'sfsdfsdfdfdsf', '2021-03-30 09:46:12', '2021-03-30 09:46:12'),
(149, 173, '9IqVCYxFFbemPWFq3dztIn5Imhwu4hhU', '2', '1111111', '2021-03-31 05:43:34', '2021-03-31 05:43:34'),
(150, 174, 'KNLum9gc4CwRseHHfYpTsJus15qWBcvf', '2', 'e51RuGHedU7Xq1RKpGA4li:APA91bEhWYQKsN6q-Iku76BwDlvXonA2jOIxkBabTT5ANPqxVsOws3wmBMZ1Titw_NstF5j-2-jOhR0uVj04Q6geY-2pbDUaDZPl5dL-84P22uea_Belp2DwFeB8W6rxCbdzrLcMoP1a', '2021-03-31 06:37:22', '2021-04-16 07:20:15'),
(151, 175, 'sYlvBXPUllvgqHFeKzng0VvcWxjjDp9T', '1', '1233', '2021-03-31 06:41:05', '2021-03-31 06:41:05'),
(152, 176, 'dCkReJkeHxvxVlGPX5hXGyAiD9na6qXZ', '1', '1233', '2021-03-31 07:49:10', '2021-03-31 07:49:10'),
(153, 177, 'tLngZDKOCRvceHjdDNhGkltte1MXS5x3', '1', 'sfsdfsdfdfdsf', '2021-03-31 10:11:32', '2021-03-31 10:11:32'),
(154, 178, 'NT6oZRl47HLja9umsjqHwkZFcYguZUHN', '1', 'sfsdfsdfdfdsf', '2021-03-31 10:50:57', '2021-03-31 10:50:57'),
(155, 179, 'bvABNTXqGU6HAtbyruShr6lUSA1xnfQd', '1', 'dhIYTrhkTS2JuPRTxCa6oY:APA91bF15fxBvK4VzfauAz7jTnoDIVGcWFt92CSoel7DDRiPw_xKv-tr8l0QGxufv9xkpQ_zieA_-eHrSpSyTOGpqdVVinNXkAxdsoJ4bHrQJphwkMqAwE7ar7LQY7uvFod4rodkaAUs', '2021-04-05 06:57:47', '2021-04-16 06:21:47'),
(156, 180, 'lPELvUZYdV4cAQY52wWPFgxROLmK8LJe', '1', 'fqLg_M4dSCyAApsJT5G71V:APA91bEg37S1yvTRlA_b8zd6O_XQIVtA-6Vzx5gmzTMaqAIlIGo6w4ZyDoPLWD-GrFWs6pfkdeYzXjkfDqNkjxlr3hdb0Nlx8kvXVbaClFQ0kdhK0_JM3-EnI2v0xzTtiiILcy5TYEsW', '2021-04-06 06:21:33', '2021-04-06 08:12:58'),
(157, 181, 'qcFRPs2QREv83rU3gV1KWEZyRuV4FsIr', '2', 'fidiuyKtWEjDoU0b702ir2:APA91bHORIhg0Yh1YUrkuuvyEc9YF8RAdHaEhyHUan84Sbk8n9_x_odvXb9OgjVNOUhZat3lrfy_6cCfoT-KgQJW6b6exA7rNEABFwxzPBC5-XfZAW6d8ZmA1MuW_ThaBIw0V-w615vT', '2021-04-07 03:55:25', '2021-04-07 11:03:28'),
(158, 182, 'yFL8bq2ItJcLraF5xKhjWKZyba6qgnJW', '2', 'd28uLBtqg0n-mmgKZh49-U:APA91bGPkrggc7gjlxBZJm7hNCkjShr_rQVJVHlKX04DdFvVNZSCxQ-Utpsk--0HpMLx183SMtvW32QGcLUKjMB_aZKEG8weO7KyGoaD7x95B0pWkLhrONQCz2f2r9aJIeGZc5HYvx9f', '2021-04-07 04:51:33', '2021-04-07 04:52:01'),
(159, 183, 'wIFn47DpjFfM2bWQ0vN4ub72IcmpkyEd', '1', 'sfsdfsdfdfdsf', '2021-04-07 10:06:52', '2021-04-07 10:06:52'),
(160, 184, '1RFiF4WinXS8JQlgZgXyUTCkiIGQFKWm', '1', 'sfsdfsdfdfdsf', '2021-04-07 11:26:23', '2021-04-07 11:26:23'),
(161, 185, 'J4CpmX53yCSDlLeQb8rSpizjNFJbkzjv', '1', 'e4ExAieuSdSwA5YoFBDX8x:APA91bF1Vgwy8Dpr8GoSD9kB7bw73nHHWW567-r5iII-CtPXpsfF_bwjrn1B3QfFR5zu5m6yICJLZjLgLslZ7PW73UEL7vm0zuwyfZvwn3bKTyVtnsKwD3xy-DyEBZRfNeWtPjo6qsVI', '2021-04-08 04:07:32', '2021-04-08 04:35:32'),
(162, 186, 'Y35BsIQESL4jM0f2u2rMTGuXc2wtXMJh', '1', 'dcfBigdbTTiP8i4GgDvsFJ:APA91bFe1RDYBXaDnpAAIM0FvKSomMjQMCOFwIYHXKRfSEBu_mlt_E8JEBTKvIFbEb8g3ba0rBR9jHXZHkPBxV095fNlaBaD6hoMUIESdz0Y1dFNVRSIfvhJkcUl649ZrFf-WnlhGl7i', '2021-04-08 10:26:12', '2021-04-09 05:31:23'),
(163, 187, 'c15IB2JmXcpxGbeHhlKDKsWFr51h8OOa', '1', 'foAN9f_zRoK0xgOf2QM6Gn:APA91bGMilBFbrV0JUTzZDZorAEcC9ISk4S-NeL5RmKfbzQ4wPeHR7wI1XjdTTuvaL1b53I7IpnNLbRWa4BQzx5qvmSnjP8UmBvpO6RPZbXqSE9st7B6LQdaDGYGH6VXl8mry6Nd3w6e', '2021-04-15 04:49:10', '2021-04-15 05:48:08'),
(164, 188, 'c999hJnXywx1XBRPBbFLqEu94IpsHUaq', '1', 'sfsdfsdfdfdsf', '2021-04-15 10:24:47', '2021-04-15 10:24:47'),
(165, 189, 'am0KNK8IYY2lqEFkH7s57WDu4VmRudya', '1', 'sfsdfsdfdfdsf', '2021-04-15 10:58:17', '2021-04-15 10:58:17'),
(166, 190, 'Oqqdfnmou0R18TTmnSiKW92fNDuZTBjv', '1', 'dtdvrQJsQYWIdvH7shInzr:APA91bFDJeaRegsUA5zaWVDcjg1mhlFx-cB7MUXKMUhZ_yo9AxfT_Sgb84_NU1Vj1EnrIb9gbtEdvqM_gGS-BylHjIjBquBwifW6Atdk-k2F9FRg77mIoJpgbQavNpEQJ0wKVS_RuopC', '2021-04-15 11:02:28', '2021-04-15 11:24:21'),
(167, 191, '61xeICzbmkyxHFbDq41kqx8rDNDnY839', '1', 'eXjjV5ZKTkyVWzgYzAXItl:APA91bFZkW_A0Z1_Nknc3ychK3dJ2MpQDVvkNpvsJlEE0W9ESlBCznqbKhiTTNo_JdkjuShfkb_hVXvScChIAsZr3ApN6zrIevr3Ofq-F4eOdocKnqqV99vQCrDwHw_uN2-kDSv4VbOF', '2021-04-16 07:06:11', '2021-04-16 07:06:11'),
(168, 192, 'MSbGAoZ7GmlrkbJHbBoNriTc3e8lXBLo', '1', 'sfsdfsdfdfdsf', '2021-04-16 09:28:32', '2021-04-16 09:28:32'),
(169, 193, 'Rmm9D9CETEA9HtbXqn222bDC7kPDIbr4', '1', 'sfsdfsdfdfdsf', '2021-04-16 10:00:55', '2021-04-16 10:00:55'),
(170, 194, 'Hkqa3ISvFv5O8ZuMNTs8qaNPA045kimT', '1', 'cHo6HC7QR7eWtdiW4fpKzi:APA91bHs6V1mWh6-vm_KLOVtRdvdvfyQ4G_eAL2VZSdq6XIcvdlG9R9s3qP1YbTms2MiQ2ZVAXciEDfJeldl8MXCwTaMJjVKl4Vp6PsJK47WAyxfq495IBZPaeWwTf1JVB09TnwiGbuv', '2021-05-14 10:11:48', '2021-05-15 11:54:01'),
(171, 195, 'ylwq7TymQgidtRQdF45B4L9nHKGYrdUY', '1', 'sfsdfsdfdfdsf', '2021-05-15 10:06:10', '2021-05-15 10:06:10'),
(172, 196, 'leN5ltwFgLGiFC7et6qdX32ghAzeTphq', '1', 'dy6m2TOQS_CORqU2WyFYxs:APA91bGpDHSk7vnGhG6-O_2bGAlnukW6i3yo1HYxcLUQLtKh-OAQRtzg1Cw-Wkq1aO6oE5D5Ht2T_GAM7UClglFDHfOwYnOvTXRmgJLVP5sFO_4so3oyfK-8S1KrYT0GElt1tzvTp0UU', '2021-05-18 03:37:23', '2021-05-21 08:47:55'),
(173, 197, 'Jp85N516tvFLqRR46DMRgxoRZkUkf8dx', '1', 'sfsdfsdfdfdsf', '2021-05-18 10:55:51', '2021-05-18 10:55:51'),
(174, 198, 'J79U6C8qJpw9J9sPHg8tQOXW1tzY5vHk', '1', 'd0NFsVNF0Ek:APA91bFXOms-AZHYiAwOqs2I-qg7Z4C-miZ3sHtiTEF-VaJQswTcQxRowNgGsOqzTfexjP0tN6H6AIC_uwN_gJZTb8GUo04TJ6q52DV7b3nI0qxiMCwNSWnYgWukXC0fbRrFT_LwNubv', '2021-05-24 09:41:38', '2021-07-23 12:34:49'),
(175, 199, 'ehwv5oBs60mMKBqxYlXClc7MxNNe5dcE', '1', 'etgyhforQUWvGhLQClbrAJ:APA91bG4znZTv1f7QgTXHwCXXDIL8vZ1Ua8euq71pBp7_M8VxiSDcgafCAtNMtLu8D-10cEgzhnjPkQmk5BnZ7Nb0gY7JclMJueQXBjsxPAYjg1uJxLjg0W5dwAp2iqPWq5FyES7gbDC', '2021-05-25 04:03:13', '2021-05-25 05:27:56'),
(176, 200, 'FIaJZRCOhiokQg1DUTEQoC6hmM10detD', '1', 'fNNuNl6jQZCjU9iZX8RMMP:APA91bGtbXNkw8r0kO9jDW7IaZlDwzwUnTzYTbUhf-XRg_RhSG904RJ2gDw_E79q0PiM-EaJT2TdYKmIYp9RrvFzHAHFAVJz1Egn2JohY4jFYlfsh57EMZAtZxk2mVXQ7YtPQLa5mLKU', '2021-05-26 03:41:58', '2021-06-04 09:20:49'),
(177, 201, 'XteMqUv763CNxL1GZ0cUUpZLwf9XM8MS', '1', 'eVFMxSbAVXM:APA91bG2m0_5AszdS0b0zAdopPa1Ga_jE8GekBwx0j1bMFQ3UvjMZ9BoWV5uZiooaRBMH_rQLH7HrESY6aqZDsnPeTm4i-i9INDcqrwOH9IH7aI1fGzMf1-3XCUNaFQLigP5zYj9vizw', '2021-05-26 06:44:30', '2021-06-18 12:00:03'),
(178, 202, 'RW7AEaC40ZqDTZshC6fuPHvJaJmBozmK', '1', 'sfsdfsdfdfdsf', '2021-06-18 04:24:07', '2021-06-18 04:24:07'),
(179, 203, 'rSXosMVJjY4nYBxKGLtzqNEeWVOWGw7k', '1', 'elY3TJ2bDEw:APA91bElCQkF8m3lpA5WWLICwhhgJYetf6KlE86Z4MQCC3zcEbNjOQ98Lb7SDdeRind8oTyaISDA22CQq6VPbxwa65qic8yhVMcS8OCVSIdYpI1CJ-PXvs7jfHnowdneqjfREJH4Hcw1', '2021-07-27 10:50:00', '2021-07-27 10:50:00'),
(180, 204, '6T1Qsqb6OvoYw8Vyqb4R1pSJLC2Nbrpt', '1', 'cExk3sMIaOA:APA91bFysiDz6qyO2xYpBN9KaSxMoZPMMnooEdG_Mr2r8gcVmqi5n0hiJ-0YNIhTLlr384ptNVLUNbKvIXXiDAFsH6xpFLDUaPqQaZfdnhuCj1wDripXfXcCvwE8cdsowZ792gvcaYF0', '2021-07-28 11:01:50', '2021-07-28 11:01:50'),
(181, 205, 'rAOOsydr4andRyNwIzfVwUDPYhbUYLzF', '1', 'elY3TJ2bDEw:APA91bElCQkF8m3lpA5WWLICwhhgJYetf6KlE86Z4MQCC3zcEbNjOQ98Lb7SDdeRind8oTyaISDA22CQq6VPbxwa65qic8yhVMcS8OCVSIdYpI1CJ-PXvs7jfHnowdneqjfREJH4Hcw1', '2021-07-29 10:07:20', '2021-07-29 10:07:20');

-- --------------------------------------------------------

--
-- Table structure for table `baby_profiles`
--

CREATE TABLE `baby_profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parentID` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'userID as parentID',
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `babyprofilepic` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `baby_profiles`
--

INSERT INTO `baby_profiles` (`id`, `parentID`, `firstname`, `lastname`, `gender`, `dob`, `babyprofilepic`, `created_at`, `updated_at`) VALUES
(11, '15', 'dfadfdfdf', 'sdfsdfsdf', 'FeMale', '2-12-2', '3FrwP1gJ8R_8d0bd279-3c75-436b-8cd3-9f502b9c5fc39145193243119038009.jpg', '2020-12-01 11:11:46', '2020-12-01 11:11:46'),
(12, '15', 'Gagandeep', 'kaur', 'female', '12-9-2018', 'dqKnevxcCR_home-banner.jpg', '2020-12-02 06:39:28', '2020-12-02 06:39:28'),
(13, '15', 'Amandeep', 'singh', 'female', '10-8-1992', 'zwYNGm2qep_dsc_3985.jpg', '2020-12-02 06:53:22', '2020-12-02 12:14:14'),
(14, '15', 'Gagandeep', 'kaur', 'female', '12-9-2018', 'haZkzd8vis_3f3dd9219f7bb1c9617cf4f154b70383.jpg', '2020-12-02 06:56:42', '2020-12-02 06:56:42'),
(16, '21', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-03 13:55:36', '2020-12-03 13:55:36'),
(17, '21', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-03 13:55:38', '2020-12-03 13:55:38'),
(18, '21', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-03 13:55:39', '2020-12-03 13:55:39'),
(19, '21', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-03 13:55:40', '2020-12-03 13:55:40'),
(20, '21', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-03 13:55:41', '2020-12-03 13:55:41'),
(21, '21', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-03 13:55:42', '2020-12-03 13:55:42'),
(22, '19', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-05 09:36:19', '2020-12-05 09:36:19'),
(23, '19', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-05 09:36:20', '2020-12-05 09:36:20'),
(24, '19', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-05 09:36:21', '2020-12-05 09:36:21'),
(25, '19', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-05 09:36:21', '2020-12-05 09:36:21'),
(26, '19', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-05 09:36:23', '2020-12-05 09:36:23'),
(27, '19', 'Gagandeep', 'kaur', 'female', '12-9-2018', '', '2020-12-05 09:36:23', '2020-12-05 09:36:23'),
(28, '23', 'Isha', 'Deo', 'Male', '3-12-1996', '', '2020-12-05 18:14:22', '2020-12-05 18:14:22'),
(29, '23', 'Isha', 'Deo', 'Male', '3-12-1996', '', '2020-12-05 18:14:53', '2020-12-05 18:14:53'),
(30, '23', 'Isha', 'Deo', 'Male', '3-12-1996', '', '2020-12-05 18:14:56', '2020-12-05 18:14:56'),
(31, '23', 'Isha', 'Deo', 'Male', '3-12-1996', '', '2020-12-05 18:15:08', '2020-12-05 18:15:08'),
(32, '23', 'Isha', 'Deo', 'Male', '3-12-1996', '', '2020-12-05 18:15:09', '2020-12-05 18:15:09'),
(33, '23', 'Isha', 'Deo', 'Male', '3-12-1996', '', '2020-12-05 18:15:11', '2020-12-05 18:15:11'),
(34, '23', 'Isha', 'Deo', 'Male', '3-12-1996', '', '2020-12-05 18:15:13', '2020-12-05 18:15:13'),
(35, '24', 'Abhi', 'Bhumihar', 'female', '12-9-2018', '', '2020-12-05 18:27:51', '2020-12-05 18:27:51'),
(36, '24', 'Abhi', 'Bhumihar', 'female', '12-9-2018', '', '2020-12-05 18:27:53', '2020-12-05 18:27:53'),
(37, '24', 'Abhi', 'Bhumihar', 'female', '12-9-2018', '', '2020-12-05 18:27:56', '2020-12-05 18:27:56'),
(38, '15', 'asf', 'dd', 'Female', '2-12-2', 'xc6fGgTSUO_229695f4-c73e-4d12-a1b8-2ba580615fda8382082160272931251.jpg', '2020-12-07 05:45:55', '2020-12-07 05:45:55'),
(39, '15', 'asf', 'dd', 'Female', '2-12-2', 'es3VdAltCs_229695f4-c73e-4d12-a1b8-2ba580615fda8382082160272931251.jpg', '2020-12-07 05:45:55', '2020-12-07 05:45:55'),
(40, '15', 'asf', 'dd', 'Female', '2-12-2', 'To9HeWwC6E_229695f4-c73e-4d12-a1b8-2ba580615fda8382082160272931251.jpg', '2020-12-07 05:45:56', '2020-12-07 05:45:56'),
(41, '15', 'asf', 'dd', 'Female', '2-12-2', 'xaAmQm3Xlk_229695f4-c73e-4d12-a1b8-2ba580615fda8382082160272931251.jpg', '2020-12-07 05:45:56', '2020-12-07 05:45:56'),
(42, '15', 'asf', 'dd', 'Female', '2-12-2', 'eMa2WyuYXG_229695f4-c73e-4d12-a1b8-2ba580615fda8382082160272931251.jpg', '2020-12-07 05:45:56', '2020-12-07 05:45:56'),
(43, '15', 'asf', 'dd', 'Female', '2-12-2', 'rPY5hgRYAm_229695f4-c73e-4d12-a1b8-2ba580615fda8382082160272931251.jpg', '2020-12-07 05:46:00', '2020-12-07 05:46:00'),
(44, '15', 'fghfgh', 'dfgdfg', 'Female', '16-12-16', 'oaAbiiYWH8_e5564f74-123d-4170-bac7-1ed5db9a93867893412368279283829.jpg', '2020-12-07 06:48:32', '2020-12-07 06:48:32'),
(45, '15', 'Abhay', 'Kumar', 'Female', '1992-01-21', '8gfKIfSu6t_image_picker_16FE441E-8B43-4AB2-A87F-47EA947B1366-15130-000008F34FE07B3D.jpg', '2020-12-11 08:09:42', '2020-12-11 08:09:42'),
(46, '15', 'Abhay', 'Kumar', 'Female', '1992-01-21', '8TJ7snuTPC_image_picker_16FE441E-8B43-4AB2-A87F-47EA947B1366-15130-000008F34FE07B3D.jpg', '2020-12-11 08:09:43', '2020-12-11 08:09:43'),
(47, '15', 'Abhay', 'Kumar', 'Female', '1992-01-21', 'hBdBpaZYyv_image_picker_16FE441E-8B43-4AB2-A87F-47EA947B1366-15130-000008F34FE07B3D.jpg', '2020-12-11 08:09:44', '2020-12-11 08:09:44'),
(49, '66', 'BabyFirstName', 'babyLastName', 'Female', '2021-01-04', 'Q2g5xXI3XF_45334f7e-0785-4203-8275-3ab23d2ed0f14928747925005697431.jpg', '2021-01-05 13:27:49', '2021-01-05 13:27:49'),
(50, '66', 'xyz', 'Singh', 'Male', '2021-01-04', 'B3mNoHV6E3_feb44a51-3034-491c-88be-939440115ad12389560982955392829.jpg', '2021-01-08 08:05:00', '2021-01-08 08:05:00'),
(51, '66', 'Gagandeep', 'kaur', 'female', '12-9-2018', '6ZjeuscbXp_microphone.png', '2021-01-08 08:30:42', '2021-01-08 08:30:42'),
(52, '66', 'newbaby', 'lastnm', 'Female', '2020-12-14', 'Uvf9EXaUxa_ef92f386-9ab6-4101-982c-3740d1bd17038315297965621179503.jpg', '2021-01-08 10:51:31', '2021-01-08 10:51:31'),
(53, '66', 'asd', 'jkl', 'Male', '2021-01-03', 'WvIhaFGeGp_scaled_image_picker4931459690649487563.jpg', '2021-01-08 11:12:19', '2021-01-08 11:12:19'),
(54, '137', 'test12', 'testing12', 'Male', '2021-03-16', 'HDgkFQ5IVK_girl-3.png', '2021-01-08 11:37:24', '2021-07-09 05:47:32'),
(55, '138', 'baby', '1', 'Female', '2021-01-01', '4gOix9MCNB_426702cb-28bb-4dbd-bbfb-8689efd82869629649798.jpg', '2021-01-08 13:46:32', '2021-01-08 13:46:32'),
(56, '66', 'aaaa', 'bbb', 'Male', '2021-01-08', '17V2DmEWo7_6d92b093-647b-4bf9-bb1d-16d552b90b863578277962894973447.jpg', '2021-01-08 15:26:40', '2021-01-08 15:26:40'),
(57, '66', 'tstbaby', 'lbaby', 'Female', '2020-12-10', 'fYKKHwQp5V_48977d07-b00d-4ebf-8fbd-4f496cf137d96340936821199049516.jpg', '2021-01-08 15:28:54', '2021-01-08 15:28:54'),
(58, '148', 'maika', 'larocque', 'Female', '2017-10-03', 'hVJD2H6lYC_scaled_image_picker3766661508382675259.jpg', '2021-01-16 01:36:02', '2021-01-16 01:36:02'),
(59, '149', 'key', '1', 'Female', '2020-12-31', 'nY7y21I17f_1b21257c-4d86-46a6-8aa5-763d8c3da5784544616655295707600.jpg', '2021-01-16 07:08:48', '2021-01-16 07:08:48'),
(60, '149', 'asss', 'd', 'Male', '2020-12-09', 'DdwO7JTxAO_scaled_image_picker559218374735853577.jpg', '2021-01-16 07:11:23', '2021-01-16 07:11:23'),
(61, '150', 'fst', 'last', 'Female', '2021-01-16', 'g95umS9mpI_c7b02902-058d-41a5-aa25-ff106c4c04be517613646.jpg', '2021-01-22 15:33:39', '2021-01-22 15:33:39'),
(62, '150', 'vv', 'hg', 'Female', '2021-01-21', '0lmxCG0fKL_scaled_image_picker241189705.jpg', '2021-01-22 15:43:30', '2021-01-22 15:43:30'),
(63, '138', 'rr', 'kk', 'Male', '2019-12-02', 'RWlh4gAtIl_scaled_image_picker902081089.jpg', '2021-02-01 06:32:20', '2021-02-01 06:32:20'),
(64, '138', 'gg', 'ss', 'Male', '2020-02-11', 'lnOBBnh3TS_6acf37e7-e30f-4b4a-b9f1-25c2ec54cf52539139177.jpg', '2021-02-01 06:35:38', '2021-02-01 06:35:38'),
(65, '153', 'iuiuiouiouiou', 'kiiouiouio', 'Male', '2021-02-03', 'oXY3drgHJI_df43dc7b-aca1-4346-8740-317c09a82ef27126799356321248722.jpg', '2021-02-03 14:09:28', '2021-02-03 14:09:28'),
(66, '153', 'oiioiio', 'llkll', 'Female', '2021-01-13', 'nIXGARyGVn_c49122fa-54e9-471c-a0f9-2d1c4da52b19235627965969738372.jpg', '2021-02-03 14:10:08', '2021-02-03 14:10:08'),
(68, '66', 'adfd', 'sdf', 'Female', '2021-02-03', 'PxCtNKRv3D_d1fa2f69-268b-4ece-b445-d116c4c1e629154769444937433098.jpg', '2021-02-05 12:42:29', '2021-02-05 12:42:29'),
(69, '66', 'dfs', 'sdfds', 'Male', '2021-02-01', '8LKXFSmRRI_244c4eee-0811-4ba4-89af-0298e0b27eb31134819519833664889.jpg', '2021-02-05 12:43:00', '2021-02-05 12:43:00'),
(70, '66', 'hh', 'gg', 'Female', '2021-02-06', 'JJblH5jgiK_scaled_image_picker5448449063996408385.jpg', '2021-02-06 06:13:22', '2021-02-06 06:13:22'),
(71, '66', 'nnn', 'bbnj', 'Female', '2021-02-06', 'hpjXryfSe8_4c8a0ec9-5fa5-4d7c-955f-d5014373306a2189196189572189443.jpg', '2021-02-06 06:25:51', '2021-02-06 06:25:51'),
(72, '158', 'ghjk', 'hjk', 'Female', '2021-02-02', 'oi2dIBZj1z_949cb67c-850b-4800-a88a-2350c7a998641338993991182121223.jpg', '2021-02-06 06:45:52', '2021-02-06 06:45:52'),
(73, '159', 'jkg', 'ghjgh', 'Male', '2021-02-02', 'fL26RSzXMB_scaled_image_picker2490422667863355959.jpg', '2021-02-06 10:19:55', '2021-02-06 10:19:55'),
(74, '160', 'fghfgh', 'ghfg', 'Female', '2021-02-02', 'sqWL0kP0OO_a393df19-7ee7-4671-b730-c815e7c46b779153371266997030022.jpg', '2021-02-10 09:57:13', '2021-02-10 09:57:13'),
(75, '161', 'jkljk', 'jkl', 'Male', '2021-02-09', 'Lha68BwbIn_100ec0ae-1e2c-406a-a9bd-203de81548ec2445295148962649406.jpg', '2021-02-11 10:58:42', '2021-02-11 10:58:42'),
(76, '161', 'lk;lkl;', 'kl;', 'Female', '2021-02-09', '7UeEChj7AG_4f572d21-a62c-4bbd-bda7-a7a7108813e7744243126182127794.jpg', '2021-02-11 10:59:10', '2021-02-11 10:59:10'),
(77, '95', 'aa', 'bbb', 'Female', '2020-02-11', 'c9RkVSmGCl_dc79862d-5a3e-4ff2-8a38-04ffbb5b68527882871308539708337.jpg', '2021-02-11 10:59:51', '2021-02-11 10:59:51'),
(78, '162', 'm', 'a', 'Female', '2021-02-08', 'NzZSrdmKRt_scaled_image_picker7833358233275028074.jpg', '2021-02-11 15:17:15', '2021-02-11 15:17:15'),
(79, '95', 'hii', 'gii', 'Female', '2020-02-21', 'R3WSkvhqVi_4c60b272-b0fd-45aa-a1a4-b86a68c04e2b7535969086732264525.jpg', '2021-02-12 07:33:11', '2021-02-12 07:33:11'),
(80, '67', 'childFirst', 'childLast', 'Male', '2021-02-11', '2fyouNb1US_0ac9ec2a-f03c-4810-a2dc-2a2747ced2ef6316684907287955442.jpg', '2021-02-18 12:13:22', '2021-02-18 12:13:22'),
(81, '66', 'sirat', 'singh', 'Male', '2021-02-20', 'Bxp3K63q06_e03e8a59-8d8e-487f-a628-df6560ae74693476403762103030692.jpg', '2021-02-20 05:53:05', '2021-02-20 05:53:05'),
(82, '66', 'kirat', 'Singh', 'Male', '2019-02-21', 'YHqCubulZI_8c9522d6-40c8-4426-bc2f-b6ed6b6d26b81450635662051976654.jpg', '2021-02-20 06:05:44', '2021-02-20 06:05:44'),
(83, '66', 'aaaa', 'bb', 'Male', '2020-02-05', 'l7a6DuCVxi_scaled_image_picker6326518959688493955.jpg', '2021-02-20 11:09:14', '2021-02-20 11:09:14'),
(84, '66', 'gggg', 'jsksd', 'Female', '2020-02-04', 'DgI5dS9hmp_f4aa79fe-5ebd-433f-a4dc-cc03eb4993c13601818426984270777.jpg', '2021-02-20 11:14:41', '2021-02-20 11:14:41'),
(85, '66', 'aa', 'ttt', 'Female', '2019-02-05', 'K1BmAdRCh8_48c41ff8-3ea1-486f-bbb4-7f20d6a7cf034110022064034671589.jpg', '2021-02-20 11:24:18', '2021-02-20 11:24:18'),
(86, '154', 'dd', 'kk', 'Male', '2017-10-03', 'lI0nhTRDd7_scaled_image_picker3503566641500074314.jpg', '2021-02-26 17:08:56', '2021-07-09 18:05:02'),
(87, '152', 'aaaa', 'bbb', 'Male', '2020-03-01', 'QKeK40wOYY_scaled_image_picker7617129853448130176.jpg', '2021-03-01 06:27:44', '2021-03-01 06:27:44'),
(88, '152', 'ddf', 'cgg', 'Female', '2019-03-05', 'QKrTcCv0up_934b91fe-1a38-4b03-bde0-391bbd2c86009051884659752348400.jpg', '2021-03-01 06:28:58', '2021-03-01 06:28:58'),
(89, '152', 'third b', 'baby', 'Male', '2020-03-11', 'G5Ke1LTYyI_502f58d8-143b-4204-a21a-95b2f7c4b13f6987249920959925658.jpg', '2021-03-01 06:30:03', '2021-03-01 06:30:03'),
(90, '152', 'aaaa', 'bbbbbb', 'Female', '2021-03-08', '6eDLstkPY9_d5623ed5-7def-4465-af5a-cb269d4420d77880617741390898035.jpg', '2021-03-08 09:43:20', '2021-03-08 09:43:20'),
(91, '152', 'a', 'aa', 'Female', '2021-03-08', 'E7QHmxDay2_a4b37207-733d-419d-b0a4-f671562f8ad9270497253087292120.jpg', '2021-03-08 09:53:21', '2021-03-08 09:53:21'),
(92, '135', 'test12', 'testing12', 'Male', '2021-03-16', 'SYgJ5mTbNL_Screenshot 2021-07-08 122319.png', '2021-03-16 11:08:38', '2021-07-08 13:25:25'),
(93, '135', 'baby2', 'lstName', 'Male', '2021-03-08', '2HakS2k9PQ_83db1d42-d628-4f7d-80b5-07719f72f7752041808761.jpg', '2021-03-16 11:57:52', '2021-03-16 11:57:52'),
(94, '137', '2nd baby', 'lstName', 'Female', '2021-03-08', 'JHZvmjzW6B_scaled_image_picker4072007225660229386.jpg', '2021-03-17 07:36:54', '2021-07-09 04:31:23'),
(95, '164', 'abc', 'abc', 'Male', '2020-03-03', 'xY1jwLVMZy_f640c9ee-35e0-4fc9-a768-6eca595d99a6348409199592349218.jpg', '2021-03-18 10:30:43', '2021-03-18 10:30:43'),
(96, '164', 'pqr', 'pqr', 'Female', '2021-01-05', 'eq4AMmtT49_92b934e7-b1cc-4a05-abd8-40a83cd1ef326995044161506098536.jpg', '2021-03-18 10:31:23', '2021-03-18 10:31:23'),
(97, '167', 'abhi', 'abhi', 'Male', '2021-01-04', 'as7QnIVqCM_785de3b6-f43f-48e2-b754-2e1eea1681698194169208547783148.jpg', '2021-03-19 10:31:30', '2021-03-19 10:31:30'),
(98, '168', 'abc', 'de', 'Female', '2019-03-29', '3DH2L2peVg_0a200956-0017-4437-8d48-1c59678524c11148041428852072754.jpg', '2021-03-25 09:58:39', '2021-03-25 09:58:39'),
(99, '168', 'fs', 'vshej', 'Male', '2020-02-12', 'jsRFNykP2r_ae8e9483-e04e-441f-b9d6-53ac771a40034142795135009324037.jpg', '2021-03-25 09:59:48', '2021-03-25 09:59:48'),
(100, '169', 'arpit', 'shrma', 'Male', '2020-12-02', 'wz9vg1B6bS_scaled_image_picker4924174282898575187.jpg', '2021-03-25 12:30:18', '2021-03-25 12:30:18'),
(101, '170', 'any', 'sharma', 'Female', '2020-11-04', 'q806VOhx2y_scaled_image_picker8484492766812951175.jpg', '2021-03-25 12:56:24', '2021-03-25 12:56:24'),
(102, '170', 'hello', 'hello', 'Female', '2020-11-12', 'VNhrpx0kNI_scaled_image_picker8813564862444705327.jpg', '2021-03-25 13:17:10', '2021-03-25 13:17:10'),
(103, '171', 'aahil', 'shrma', 'Male', '2020-09-02', 'CoiSUcTOy4_image_picker_7C918E8A-AA44-4986-85EA-E37508C38185-31901-00000914F04B8DC5.jpg', '2021-03-30 10:20:44', '2021-03-30 10:20:44'),
(104, '178', 'ahhsha', 'hdhdhd', 'Male', '2021-03-01', '01D8z4iPTW_scaled_image_picker4775287696530143288.jpg', '2021-03-31 10:52:31', '2021-03-31 10:52:31'),
(105, '178', 'shhsjs', 'jdjdid', 'Female', '2021-03-03', 'uKpu3MlqBt_scaled_image_picker3246853907980022616.jpg', '2021-03-31 10:55:20', '2021-03-31 10:55:20'),
(107, '179', 'dgfd', 'hdh', 'Female', '2021-03-16', '6nngxtAhjq_image_picker_9EFE1FBE-EF71-4CA1-8C67-FA851E9DE51E-622-000000898D13CED5.jpg', '2021-04-05 12:02:59', '2021-04-05 12:02:59'),
(108, '174', 'tstFst', 'tstLst', 'Female', '2021-04-05', '9WWQhsPTEl_image_picker_5A3A6732-EF57-4F22-B966-03658599EFA3-467-000000136229E7E8.jpg', '2021-04-06 06:05:50', '2021-04-06 06:05:50'),
(109, '180', 'sby', 'vm', 'Male', '2020-12-30', 'lAZFh3cpNx_scaled_image_picker3111612525982941141.jpg', '2021-04-06 07:01:34', '2021-04-06 07:01:34'),
(110, '180', 'nmo', 'bh', 'Female', '2020-12-24', 'ewkEkEsWCf_scaled_image_picker4083617031897622221.jpg', '2021-04-06 07:02:30', '2021-04-06 07:02:30'),
(111, '180', 'virbl', 'bn', 'Male', '2021-04-03', '28DifNvn1S_scaled_image_picker5819933615293949184.jpg', '2021-04-06 09:37:02', '2021-04-06 09:37:02'),
(112, '179', 'tdhddh', 'tdydy', 'Female', '2021-03-10', 'IeEfNsG7EG_image_picker_81F4EE49-B522-464B-8F41-FDB519B56C7E-863-0000006D6BC5D656.jpg', '2021-04-06 12:52:13', '2021-04-06 12:52:13'),
(113, '181', 'yuml', 'gg', 'Female', '2021-04-07', 'vxouur8aiI_image_picker_E71E6827-924D-48E6-8D24-5ACB21266D99-921-0000007C359F6895.jpg', '2021-04-07 04:09:46', '2021-04-07 04:09:46'),
(114, '181', 'maji', 'ja', 'Male', '2019-11-29', 'pFZjSpK8g2_image_picker_FF325F4C-147C-49A6-81F4-703C7D6CFC4C-921-0000007D7E57EEF9.jpg', '2021-04-07 04:10:43', '2021-04-07 04:10:43'),
(115, '179', 'hxxhhfu', 'dydhdd', 'Choose gender', '2021-03-02', 'XqcggXSzWp_scaled_image_picker6784581651625032913.jpg', '2021-04-07 04:32:15', '2021-04-07 04:32:15'),
(116, '179', 'fyyffy', 'hcgguuug', 'Male', '2021-03-08', 'nRVC4QKePb_scaled_image_picker4005840835203143656.jpg', '2021-04-07 04:38:34', '2021-04-07 04:38:34'),
(117, '180', 'cttc', 'gyvyyvyg', 'Female', '2020-12-08', 'QmtUGTpGuU_scaled_image_picker3086618608349824051.jpg', '2021-04-07 05:23:12', '2021-04-07 05:23:12'),
(118, '180', 'gygygyyg', 'yvvyvu', 'Male', '2021-03-09', '2R5MRFwwsG_scaled_image_picker1884180665637685995.jpg', '2021-04-07 05:23:45', '2021-04-07 05:23:45'),
(119, '181', 'guri', 'chch', 'Male', '2021-03-10', 'nzio73pysi_image_picker_59796902-9421-4890-8B10-CEFBCACF266A-1027-000000A9DBA2917F.jpg', '2021-04-07 06:22:55', '2021-04-07 06:22:55'),
(120, '179', 'cgchg', 'gccgcg', 'Female', '2020-12-15', 'qHY3zNWRt3_image_picker_C6BF5A85-A052-42D7-83EC-27F5536DBF26-1065-000000C0BD58A9EE.jpg', '2021-04-07 07:31:04', '2021-04-07 07:31:04'),
(121, '183', 'hhf', 'fhhf', 'Female', '2021-03-09', 'vcsmHKMquS_scaled_image_picker2071394984.jpg', '2021-04-07 10:10:41', '2021-04-07 10:10:41'),
(122, '183', 'fhhfhf', 'bfhhgh', 'Male', '2020-12-31', 'bThsfj9Zia_5b27aabc-61ce-4435-b637-bd57881395fe734733910.jpg', '2021-04-07 10:34:36', '2021-04-07 10:34:36'),
(123, '183', 'fhhfhfhf', 'cdgdg', 'Female', '2021-02-08', '6QIekiisEG_scaled_image_picker834337014.jpg', '2021-04-07 10:36:04', '2021-04-07 10:36:04'),
(124, '184', 'haha', 'nsnsn', 'Male', '2021-04-04', 'fjBfedyyE0_image_picker_0FC5422D-74F4-481C-BE8E-B2FDE12BCA09-1233-00000101104F69B8.jpg', '2021-04-07 11:28:44', '2021-04-07 11:28:44'),
(125, '184', 'aaa', 'vvv', 'Female', '2019-04-02', 'A605Cnu5p8_image_picker_EE648993-661E-4B09-969A-21DAFF873A27-1233-000001027E00B9D7.jpg', '2021-04-07 11:32:38', '2021-04-07 11:32:38'),
(126, '185', 'gg', 'hhh', 'Female', '2020-04-01', 'EuOsq1Bod1_a41ca439-a362-474c-a06e-e5a156106570195961446176744182.jpg', '2021-04-08 04:10:02', '2021-04-08 04:10:02'),
(127, '185', 'mmm', 'bbb', 'Male', '2021-02-03', 'QnUi6Qy6aR_scaled_image_picker4180612835763560720.jpg', '2021-04-08 04:10:53', '2021-04-08 04:10:53'),
(128, '187', 'kiera', 'tt', 'Female', '2020-04-29', 'u2qqCPDBJ1_scaled_image_picker3838297053724112751.jpg', '2021-04-15 04:52:32', '2021-04-15 04:52:32'),
(129, '187', 'pout', 'vg', 'Female', '2021-04-01', 'kZdqlEiHd0_scaled_image_picker300280346138314635.jpg', '2021-04-15 04:53:42', '2021-04-15 04:53:42'),
(130, '188', 'ag', 'h', 'Male', '2021-04-01', 'XdZY8g6HDN_image_picker_63CBA8B8-0D3E-4BBC-91D4-3E72B6217FC7-2729-0000028E43224639.jpg', '2021-04-15 10:26:05', '2021-04-15 10:26:05'),
(131, '188', 'vxmx', 'gkdy', 'Female', '2021-04-06', 'RSYZkSwQVl_image_picker_C5F71267-203C-4BAE-96F4-22882D65E31F-2729-0000028FCFE3ED8B.jpg', '2021-04-15 10:31:08', '2021-04-15 10:31:08'),
(132, '67', 'jiva', 'jiva', 'Female', '2021-04-06', 'lrStwkiA0s_scaled_image_picker728413879.jpg', '2021-04-16 06:54:38', '2021-04-16 06:54:38'),
(133, '67', 'dffg', 'gggh', 'Male', '2021-04-01', 'f63YJL8oIo_6e001118-b6a9-4ccf-bf88-9cc16fa2bb92251589333.jpg', '2021-04-16 06:55:30', '2021-04-16 06:55:30'),
(134, '174', 'ydyfhf', 'hvjcc', 'Female', '2021-03-09', 'NCsPVQHtxM_image_picker_12342B1D-B9F5-4C04-9A58-BB01C31A0F31-3001-0000035B73CB030A.jpg', '2021-04-16 07:21:59', '2021-04-16 07:21:59'),
(135, '192', 'nsn', 'sbb', 'Female', '2021-04-05', '7suLOhGZWn_image_picker_2F00A373-46F3-4386-A848-22557EC41B6C-3129-0000038A586D23D1.jpg', '2021-04-16 09:42:05', '2021-04-16 09:42:05'),
(136, '192', 'dd', 'dd', 'Male', '2019-04-24', 'DOew4AoKU9_image_picker_538D713B-D62D-4444-A9D4-7F389774A4ED-3129-0000038B1D57C53C.jpg', '2021-04-16 09:44:19', '2021-04-16 09:44:19'),
(137, '193', 'sj', 'js', 'Male', '2020-04-23', 'xACzzlti4G_d32a1daf-79db-47a6-8fc5-ec346f2545e25878170112616905047.jpg', '2021-04-16 10:03:18', '2021-04-16 10:03:18'),
(138, '193', 'bs', 'jwj', 'Female', '2019-04-26', 'kkrx899U1S_5d3366cd-6584-4237-b961-a3e4568a45742149366018921564583.jpg', '2021-04-16 10:04:01', '2021-04-16 10:04:01'),
(139, '194', 'annny', 'kaur', 'Female', '2021-03-09', 'fo69R5HKIg_scaled_image_picker1980240977315182600.jpg', '2021-05-14 10:12:27', '2021-05-14 10:12:27'),
(140, '195', 'hhh', 'bbbbbb', 'Female', '2019-05-31', 'wAjELp9v8F_scaled_image_picker605530268673560648.jpg', '2021-05-15 10:35:01', '2021-05-15 10:35:01'),
(141, '196', 'anny', 'anny', 'Female', '2021-04-06', 'j8f6Hu2jdC_scaled_image_picker1437097649447085621.jpg', '2021-05-18 03:42:36', '2021-05-18 03:42:36'),
(142, '197', 'baip', 'nm', 'Male', '2021-05-01', 'N9gFcwqLPO_scaled_image_picker8544105761917492458.jpg', '2021-05-18 11:16:02', '2021-05-18 11:16:02'),
(143, '198', 'jyoti', 'kaur', 'Female', '21-05-2021', 'pqnrQBnh1R_scaled_image_picker7108995961645696478.jpg', '2021-05-24 11:17:43', '2021-07-09 05:58:50'),
(145, '199', 'onki', 'onki', 'Male', '2021-05-07', '8X7E19kSKw_scaled_image_picker340777563853575103.jpg', '2021-05-25 09:55:29', '2021-05-25 10:10:16'),
(146, '199', 'ony', 'ony', 'Female', '2021-03-15', 'Ooo2egvbpU_scaled_image_picker8912230399397198976.jpg', '2021-05-25 10:11:15', '2021-05-25 10:16:18'),
(147, '200', 'Aishlin', 'kaur', 'Female', '2020-05-04', 'vj7jrPOymx_scaled_image_picker6567098236816218194.jpg', '2021-05-26 03:53:57', '2021-05-26 04:23:43'),
(150, '200', 'hkxky', 'tjtisjtst', 'Female', '2021-05-04', 'XraCjs6F61_scaled_image_picker4726306204374399130.jpg', '2021-05-26 10:47:11', '2021-05-26 10:47:11'),
(151, '200', 'ssj', 'bee', 'Female', '2020-05-12', 'cAdWOBhKvZ_scaled_image_picker5634427631583203139.jpg', '2021-05-26 10:48:54', '2021-05-26 10:48:54'),
(152, '200', 'ags', 'gz', 'Female', '2009-06-03', 'PqQvQ2HSeQ_scaled_image_picker1322384892852077183.jpg', '2021-06-04 09:58:50', '2021-06-04 09:58:50'),
(153, '200', 'navy', 'kaa', 'Female', '2020-06-30', 'HrHYbD3PTW_5f1c3727-f418-4859-abf9-6209ab469cc3146011973724625837.jpg', '2021-06-04 10:07:24', '2021-06-04 10:07:24'),
(154, '200', 'yxyf', 'asdf', 'Female', '2021-02-03', 'NfV3HAxAq9_307f8b51-d078-4759-8147-d65f19df4952372974375107784013.jpg', '2021-06-04 10:11:03', '2021-06-04 10:11:03'),
(156, '202', 'aroby', 'roby', 'Male', '2021-01-01', '38CAq3auPt_scaled_image_picker7245982189179949529.jpg', '2021-06-18 04:28:12', '2021-06-18 04:28:12'),
(157, '201', 'robix', 'robix', 'Male', '2021-04-05', 'kTs3dvLcWS_scaled_image_picker1528255914908072995.jpg', '2021-06-18 12:01:54', '2021-06-18 12:01:54'),
(158, '201', 'gift', 'gift', 'Male', '2021-04-06', 'm7IgJLxkZt_scaled_image_picker2240138406507959609.jpg', '2021-06-18 12:37:35', '2021-06-18 12:37:35'),
(159, '147', 'first baby', 'last baby', 'Male', '2021-02-10', 'IFZ7Htqwyf_scaled_image_picker544824083683594907.jpg', '2021-07-08 11:12:32', '2021-07-08 12:56:29'),
(160, '203', 'test', 'baby', 'Female', '2020-07-01', 'Onk9ETb8dx_bf01683d-9017-4e3d-97ba-1b1987c9b3d06897063891152568066.jpg', '2021-07-27 11:03:39', '2021-07-27 11:10:42'),
(161, '205', 'Test', 'Baby', 'Male', '2020-07-18', 'mVbNbG0DHS_31cc46cd-4083-41c3-a08b-ad9eec3dedb01531056046262165156.jpg', '2021-07-29 10:11:19', '2021-07-29 10:11:19'),
(162, '205', 'test', 'baby', 'Female', '2021-07-29', 'grIxK3APe1_ec64b618-e6b9-4aa4-9e48-e620cd09f8078526827437661235580.jpg', '2021-07-29 10:14:27', '2021-07-29 10:14:27'),
(163, '204', 'Nivvan', 'Kumar', 'Male', '2021-03-18', 'EuXj8KxBeA_bb078e67-8539-46db-b67d-41451f0585e27598641998479832790.jpg', '2021-07-30 14:30:35', '2021-07-30 14:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `blocked_users`
--

CREATE TABLE `blocked_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `blocked_user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_qty` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_amt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `user_id`, `product_id`, `product_price`, `product_qty`, `total_amt`, `created_at`, `updated_at`) VALUES
(10, '29', '5', '500', '4', '2000', '2021-05-24 11:43:45', '2021-05-24 11:43:45'),
(11, '19', '5', '500', '2', '1000', '2021-05-24 11:57:13', '2021-05-24 11:57:13'),
(28, '199', '26', '20', '4', '80', '2021-05-25 08:57:26', '2021-05-25 09:36:57'),
(29, '199', '12', '256', '1', '256', '2021-05-25 09:14:58', '2021-05-25 09:39:28'),
(51, '154', '28', '20', '3', '60', '2021-06-17 01:57:39', '2021-06-17 01:58:42'),
(52, '154', '27', '20', '1', '20', '2021-06-17 01:58:33', '2021-06-17 01:58:33'),
(53, '203', '9', '3000', '2', '6000', '2021-07-27 10:54:58', '2021-07-28 12:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `important_tips` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `important_tips`, `created_at`, `updated_at`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat', '2021-07-27 11:03:57', '2021-07-27 11:03:57'),
(2, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat', '2021-07-27 11:03:57', '2021-07-27 11:03:57'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat', '2021-07-27 11:03:57', '2021-07-27 11:03:57'),
(4, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat', '2021-07-27 11:03:57', '2021-07-27 11:03:57'),
(5, 'test tip', '2021-07-27 11:03:57', '2021-07-27 11:03:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2014_10_12_000000_create_users_table', 1),
(9, '2014_10_12_100000_create_password_resets_table', 1),
(10, '2019_08_19_000000_create_failed_jobs_table', 1),
(11, '2020_10_26_131207_create_user_otps_table', 1),
(12, '2020_10_26_133923_create_posts_table', 2),
(13, '2020_10_26_134503_create_post_likes_table', 3),
(14, '2020_10_26_134823_create_post_comments_table', 3),
(15, '2020_10_26_135033_create_post_sharedby_users_table', 4),
(17, '2020_10_26_140518_create_baby_profiles_table', 5),
(18, '2020_10_26_141234_create_product_collections_table', 6),
(19, '2020_10_26_142724_create_admins_table', 7),
(20, '2016_06_01_000001_create_oauth_auth_codes_table', 8),
(21, '2016_06_01_000002_create_oauth_access_tokens_table', 8),
(22, '2016_06_01_000003_create_oauth_refresh_tokens_table', 8),
(23, '2016_06_01_000004_create_oauth_clients_table', 8),
(24, '2016_06_01_000005_create_oauth_personal_access_clients_table', 8),
(25, '2020_11_16_052306_create_baby_profile_table', 8),
(26, '2020_11_16_054100_create_baby_profiles_table', 9),
(27, '2020_11_16_131120_create_shop_collection_table', 10),
(28, '2020_11_17_092001_add_description_to_product_collections_table', 11),
(29, '2020_11_17_093122_add_description_to_product_collections_table', 12),
(30, '2020_11_26_055750_create_tracking_table', 13),
(31, '2020_12_01_131531_share', 14),
(32, '2020_12_07_064606_create_addcards_table', 15),
(33, '2021_01_06_071739_create_cart_table', 16),
(34, '2021_01_06_072220_create_orders_table', 17),
(35, '2021_01_07_062407_create_product_image_table', 18),
(36, '2021_01_07_063554_create_product_images_table', 19),
(37, '2021_01_07_063657_create_reviewt_rating_table', 19),
(38, '2021_04_01_125020_info', 20),
(40, '2021_05_24_074802_notifications', 21);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_user` bigint(20) UNSIGNED NOT NULL,
  `created_by_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `description`, `to_user`, `created_by_id`, `created_at`, `updated_at`) VALUES
(2, 'Notification', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut', 19, 31, '2021-05-24 08:03:02', '2021-05-24 08:03:02'),
(3, 'Notification', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco ', 199, 31, '2021-05-25 08:22:58', '2021-05-25 08:22:58'),
(4, 'Notification', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco ', 199, 31, '2021-05-25 08:22:58', '2021-05-25 08:22:58');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` int(11) DEFAULT NULL,
  `product_quantity` int(11) DEFAULT NULL,
  `token` longtext COLLATE utf8mb4_unicode_ci,
  `card_details` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `product_id`, `product_price`, `product_quantity`, `token`, `card_details`, `bill`, `created_at`, `updated_at`) VALUES
(4, '135', '48', NULL, NULL, NULL, '6011118585757548', '20', '2021-01-27 13:11:49', '2021-01-27 13:11:49'),
(5, '135', '48e', NULL, NULL, NULL, '6011118585757548', '20', '2021-01-27 13:11:58', '2021-01-27 13:11:58'),
(6, '15', '14', 256, 2, 'test', '1234567891012345', '512', '2021-05-25 05:25:43', '2021-05-25 05:25:43'),
(8, '15', '14', 256, 4, 'test', '1234567891012345', '1024', '2021-05-25 05:27:22', '2021-05-25 05:27:22'),
(9, '15', '11', 200, 2, 'test', '1234567891012345', '400', '2021-05-25 05:27:22', '2021-05-25 05:27:22'),
(10, '15', '11', 200, 2, 'test', '1234567891012345', '400', '2021-05-25 05:46:31', '2021-05-25 05:46:31'),
(11, '15', '11', 200, 1, 'test', '1234567891012345', '200', '2021-05-25 05:47:01', '2021-05-25 05:47:01'),
(12, '199', '9', 3000, 1, 'test', '1234567891223212', '3000', '2021-05-25 06:34:16', '2021-05-25 06:34:16'),
(13, '200', '9', 3000, 2, 'test', '1234567891223212', '6000', '2021-05-26 04:33:15', '2021-05-26 04:33:15'),
(14, '200', '14', 256, 1, 'test', '1234567891223212', '256', '2021-05-26 04:38:42', '2021-05-26 04:38:42'),
(15, '200', '19', 256, 4, 'test', '1234567891223212', '1024', '2021-05-26 04:38:42', '2021-05-26 04:38:42'),
(16, '198', '9', 3000, 1, 'test', '1234567891223212', '3000', '2021-05-26 10:16:06', '2021-05-26 10:16:06'),
(17, '200', '9', 3000, 1, 'test', '1234567891223212', '3000', '2021-05-26 10:42:25', '2021-05-26 10:42:25'),
(18, '200', '11', 200, 1, 'test', '1234567891223212', '200', '2021-05-26 10:43:23', '2021-05-26 10:43:23'),
(19, '154', '9', 3000, 1, 'test', '1234567891223212', '3000', '2021-06-03 19:09:27', '2021-06-03 19:09:27'),
(20, '198', '11', 200, 2, 'test', '1234567891223212', '400', '2021-06-04 05:49:42', '2021-06-04 05:49:42'),
(21, '198', '12', 256, 1, 'test', '1234567891223212', '256', '2021-06-04 05:49:42', '2021-06-04 05:49:42'),
(22, '198', '14', 256, 1, 'test', '1234567891223212', '256', '2021-06-04 05:52:06', '2021-06-04 05:52:06'),
(23, '198', '11', 200, 1, 'tok_1IyXTBAKGUvzJoV9NhViqaE5', '4242424242424242', '200', '2021-06-04 07:29:30', '2021-06-04 07:29:30'),
(24, '200', '9', 3000, 4, 'tok_1IyZL6AKGUvzJoV9QiUmo1Yz', '4242424242424242', '12000', '2021-06-04 09:29:17', '2021-06-04 09:29:17'),
(25, '200', '12', 256, 3, 'tok_1IyZL6AKGUvzJoV9QiUmo1Yz', '4242424242424242', '768', '2021-06-04 09:29:17', '2021-06-04 09:29:17'),
(26, '200', '38', 20, 1, 'tok_1IyZWXAKGUvzJoV9I5mfNeOU', '4242424242424242', '20', '2021-06-04 09:41:06', '2021-06-04 09:41:06'),
(27, '200', '37', 20, 1, 'tok_1IyZWXAKGUvzJoV9I5mfNeOU', '4242424242424242', '20', '2021-06-04 09:41:06', '2021-06-04 09:41:06'),
(28, '200', '12', 256, 1, 'tok_1IyZZlAKGUvzJoV97QFuvRqK', '4242424242424242', '256', '2021-06-04 09:44:25', '2021-06-04 09:44:25'),
(29, '154', '9', 3000, 1, 'tok_1Iz7O4AKGUvzJoV9KE5ESgxA', '4540330601422025', '3000', '2021-06-05 21:50:36', '2021-06-05 21:50:36'),
(30, '198', '9', 3000, 2, 'tok_1IzaPxAKGUvzJoV9AaBP8L1i', '4242424242424242', '6000', '2021-06-07 04:50:29', '2021-06-07 04:50:29'),
(31, '198', '9', 3000, 1, 'tok_1IzadqAKGUvzJoV9xvLGohVF', '4242424242424242', '3000', '2021-06-07 05:04:50', '2021-06-07 05:04:50'),
(32, '154', '9', 3000, 3, 'tok_1J2Nf9AKGUvzJoV9O63MEeKz', '4540330601422025', '9000', '2021-06-14 21:49:43', '2021-06-14 21:49:43'),
(33, '198', '11', 200, 2, 'tok_1JAtg4AKGUvzJoV9xKcbKv9H', '4242424242424242', '400', '2021-07-08 09:37:53', '2021-07-08 09:37:53'),
(34, '204', '9', 3000, 1, 'tok_1JJ18dAKGUvzJoV9FxJrTKVq', '4242424242424242', '3000', '2021-07-30 19:12:56', '2021-07-30 19:12:56'),
(35, '204', '12', 256, 1, 'tok_1JK07hAKGUvzJoV92HC7P0Gx', '4242424242424242', '256', '2021-08-02 12:20:02', '2021-08-02 12:20:02'),
(36, '205', '9', 3000, 1, 'tok_1JK0GPAKGUvzJoV9kD6OYsel', '4242424343546465', '3000', '2021-08-02 12:29:02', '2021-08-02 12:29:02'),
(37, '205', '28', 20, 3, 'tok_1JK0GPAKGUvzJoV9kD6OYsel', '4242424343546465', '60', '2021-08-02 12:29:02', '2021-08-02 12:29:02');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `pages` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `pages`, `content`, `created_at`, `updated_at`) VALUES
(1, 'About-App', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2020-10-28 10:22:23', '2021-07-29 10:18:59'),
(2, 'Privacy Policy', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2020-10-28 10:23:00', '2021-01-11 07:58:27'),
(3, 'help-and-faqs', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>\r\n\r\n<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', '2021-01-11 07:08:12', '2021-01-11 07:58:39');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `postPicture` text COLLATE utf8mb4_unicode_ci,
  `postVideo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likesCount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentsCount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postShareCount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `description`, `postPicture`, `postVideo`, `likesCount`, `commentsCount`, `postShareCount`, `created_at`, `updated_at`) VALUES
(14, '15', 'post ', 'testing', 'CZSTkA7Ot7_download.jpg', '', NULL, '13', '13', '2020-12-02 12:06:17', '2021-02-19 09:52:40'),
(18, '28', 'post ', 'testing', 'inUGGfZpKN_abstract-blur-green-color-backgroundblurred-600w-749692990.jpg', '', NULL, NULL, NULL, '2020-12-11 10:43:56', '2020-12-11 10:43:56'),
(19, '15', 'post', 'testing', 'AWZerOBC5n_arrow_left.png', '', NULL, NULL, NULL, '2020-12-11 10:45:36', '2020-12-11 10:45:36'),
(20, '28', 'post ', 'testing', 'isxd1VMWXu_3f3dd9219f7bb1c9617cf4f154b70383.jpg', '', NULL, NULL, NULL, '2020-12-12 06:59:05', '2020-12-12 06:59:05'),
(21, '28', 'post ', 'testing', 'hGRehH0iyS_3f3dd9219f7bb1c9617cf4f154b70383.jpg', '', NULL, NULL, NULL, '2020-12-12 07:33:56', '2020-12-12 07:33:56'),
(23, '15', 'Testing post today', 'Testing description today', 'mraS5RwDsx_ca749ec1-3451-4587-806f-7a0c8e8679731914161998387807734.jpg', '', '8', '8', '8', '2020-12-14 11:06:37', '2020-12-28 10:07:10'),
(24, '15', 'Isha Deo Testing', 'Testing', 'fjJCZNY55m_about_image.png', '', '40', '20', '50', '2020-12-14 11:07:48', '2021-07-30 09:31:37'),
(25, '15', 'Isha Deo Testing Post', 'Testing description', 'uJvOAFPJ2H_about_image.png', '', '2', '2', '5', '2020-12-15 07:25:06', '2021-01-11 05:51:07'),
(26, '15', 'Isha Deo Testing Post', 'Testing description', 'lcYyTyzdRE_ea5d6685-d8b8-49a6-8d72-25b5bd6b63945623657374143057736.jpg', '', NULL, NULL, NULL, '2020-12-15 07:33:32', '2020-12-15 07:33:32'),
(27, '15', 'post titlexcxc', 'testing', 'X9vOsKRrDR_customize-icon.jpg', '', NULL, NULL, NULL, '2020-12-23 05:38:38', '2020-12-23 05:38:38'),
(28, '15', 'post title', 'testing', 'SZWaSLz754_customize-icon.jpg', '', NULL, '3', '5', '2020-12-23 05:40:53', '2020-12-28 10:03:42'),
(29, '15', 'post titlexcxc', 'testing', '7bRbukjgie_customize-icon.jpg', '', NULL, NULL, NULL, '2020-12-30 07:57:27', '2020-12-30 07:57:27'),
(30, '15', 'post titlexcxc', 'testing', 'femNm21BSL_customize-icon.jpg', '', NULL, NULL, NULL, '2020-12-30 08:03:47', '2020-12-30 08:03:47'),
(31, '15', 'post titlexcxc', 'testing', '', '', NULL, NULL, NULL, '2020-12-30 08:04:18', '2020-12-30 08:04:18'),
(32, '66', 'tst', 'my testing post', 'C176AdD2hr_1c52b996-96d9-448e-acb0-55c9641046535339827540416753304.jpg', '', NULL, NULL, NULL, '2021-01-06 09:56:30', '2021-01-06 09:56:30'),
(33, '15', 'post titlexcxc', 'testing', 'fBC2vpZYsa_customize-icon.jpg', '', NULL, NULL, NULL, '2021-01-06 10:02:40', '2021-01-06 10:02:40'),
(34, '66', 'post titlexcxc', 'testing', '8rUo1k5Jbn_arrow_right.png', '', NULL, NULL, NULL, '2021-01-06 10:04:08', '2021-01-06 10:04:08'),
(35, '15', 'post titlexcxc', 'testing', 'QIdgkviYyu_customize-icon.jpg', '', NULL, NULL, NULL, '2021-01-06 10:11:16', '2021-01-06 10:11:16'),
(36, '15', 'pkPost', 'testing', 'Mk7Lz7k6xp_customize-icon.jpg', '', NULL, NULL, NULL, '2021-01-06 10:12:36', '2021-01-06 10:12:36'),
(37, '15', 'pkPost', 'testing', 'vrWSOstUag_avatar.png', '', NULL, NULL, NULL, '2021-01-06 10:21:00', '2021-01-06 10:21:00'),
(38, '15', 'pkPost', 'testing', NULL, '', NULL, NULL, NULL, '2021-01-06 10:23:13', '2021-01-06 10:23:13'),
(39, '15', NULL, 'testing', NULL, '', NULL, NULL, NULL, '2021-01-06 10:26:26', '2021-01-06 10:26:26'),
(40, '15', NULL, 'testing', NULL, '', NULL, NULL, NULL, '2021-01-06 10:26:33', '2021-01-06 10:26:33'),
(41, '66', 'tst', 'my testing post', '6KlFRKKw1A_1c52b996-96d9-448e-acb0-55c9641046535339827540416753304.jpg', '', NULL, NULL, NULL, '2021-01-06 10:27:30', '2021-01-06 10:27:30'),
(42, '15', NULL, 'testing', NULL, '', NULL, NULL, NULL, '2021-01-06 10:29:02', '2021-01-06 10:29:02'),
(43, '66', 'tst', 'test1 test1 test1 post', NULL, '', NULL, NULL, NULL, '2021-01-06 10:30:13', '2021-01-06 10:30:13'),
(44, '66', NULL, 'testing', NULL, '', NULL, NULL, NULL, '2021-01-06 10:37:47', '2021-01-06 10:37:47'),
(45, '66', 'post titlexcxc', 'testing', NULL, '', NULL, NULL, NULL, '2021-01-06 10:37:57', '2021-01-06 10:37:57'),
(46, '66', 'post titlexcxc', 'testing', 'kpiyEFupqM_dislike_icon.png', '', NULL, NULL, NULL, '2021-01-06 10:38:07', '2021-01-06 10:38:07'),
(47, '66', 'tst', 'test1 test1 test1 post', NULL, '', NULL, NULL, NULL, '2021-01-06 10:43:36', '2021-01-06 10:43:36'),
(48, '15', NULL, 'testing', NULL, '', NULL, NULL, NULL, '2021-01-06 11:01:16', '2021-01-06 11:01:16'),
(49, '15', NULL, 'testing', NULL, '', NULL, NULL, NULL, '2021-01-06 11:01:34', '2021-01-06 11:01:34'),
(50, '66', 'tst', 'test hello ppost', NULL, '', NULL, NULL, NULL, '2021-01-06 11:29:22', '2021-01-06 11:29:22'),
(51, '66', 'tst', 'test hello ppost tsting', 'UC4bq1nb7c_scaled_image_picker3306762626812420894.jpg', '', NULL, NULL, NULL, '2021-01-06 11:30:03', '2021-01-06 11:30:03'),
(52, '66', 'tst', 'hi test this is my testing post', 'Q3mgBHLrzs_162613d2-58ec-41b0-b23c-3a472465c5cf1067555421615932324.jpg', '', NULL, NULL, NULL, '2021-01-08 11:34:41', '2021-01-08 11:34:41'),
(53, '137', 'tst', 'AAA pppp BBB fff', '1A0iyNmOCc_scaled_image_picker905839827065468630.jpg', '', NULL, NULL, NULL, '2021-01-08 11:39:19', '2021-01-08 11:39:19'),
(54, '15', 'test', 'test', 'bCDEqcpROl_opportunities-banner.jpg', '', NULL, NULL, NULL, '2021-01-08 12:44:58', '2021-01-08 12:44:58'),
(55, '15', 'test', 'test', NULL, '', NULL, NULL, NULL, '2021-01-08 12:45:09', '2021-01-08 12:45:09'),
(56, '15', 'test', 'test', NULL, NULL, NULL, NULL, NULL, '2021-01-08 13:02:26', '2021-01-08 13:02:26'),
(58, '15', 'test', 'test', NULL, 'n1WiosmqLo_SampleVideo_1280x720_1mb.mp4', NULL, NULL, NULL, '2021-01-08 13:09:21', '2021-01-08 13:09:21'),
(59, '15', 'test', 'test', NULL, 'KPrkNHSV7w_SampleVideo_1280x720_1mb.mp4', NULL, NULL, NULL, '2021-01-08 13:10:11', '2021-01-08 13:10:11'),
(60, '66', 'tst', 'my test post', 'DCSxkNGdks_9bb99c95-2707-45a9-b80c-f8937f79c2be6537064632475428728.jpg', NULL, NULL, NULL, '1', '2021-01-08 15:23:59', '2021-01-30 06:19:11'),
(61, '135', 'tst', 'Hi dear, i am creating a new post for testing purpose', NULL, NULL, NULL, NULL, NULL, '2021-01-15 15:26:25', '2021-01-15 15:26:25'),
(62, '148', 'tst', 'hey!', NULL, NULL, '111', '11', '11', '2021-01-16 01:36:45', '2021-01-16 01:36:45'),
(63, '66', 'tst', 'hsjs\nhsjs', '2a7PmOpQs8_scaled_image_picker7466206108477292167.jpg', NULL, '50', '56', '55', '2021-01-22 12:10:10', '2021-01-22 12:10:10'),
(64, '150', 'tst', 'good app', NULL, NULL, NULL, NULL, NULL, '2021-01-22 15:41:26', '2021-01-22 15:41:26'),
(65, '150', 'tst', 'aaass', 'ILIJ4ngtjx_08d277c4-5b7d-45cb-a540-f3e30bc9d817447806576.jpg', NULL, NULL, '2', NULL, '2021-01-22 16:10:18', '2021-02-01 07:03:16'),
(66, '16', 'post title test', 'testing test', NULL, NULL, NULL, '1', NULL, '2021-01-29 07:45:57', '2021-02-01 06:47:24'),
(67, '138', 'tst', 'hello\njxjxk\nnxjkd\njdkdod\nkkogiehhshs\njzjsj', '6q44AszBLE_5f6f5392-0f23-4f2f-bcbd-3af67f89bde0792822662.jpg', NULL, NULL, NULL, '1', '2021-02-01 06:53:17', '2021-02-01 07:22:39'),
(68, '138', 'tst', 'tyyyuuuyyy\ngyuu\nkj7i', 'xnqf1FslmX_scaled_image_picker508914773.jpg', NULL, NULL, NULL, NULL, '2021-02-01 07:01:30', '2021-02-01 07:01:30'),
(69, '153', 'tst', 'khkhjkhjhjhjkhjkjj', NULL, NULL, NULL, '1', NULL, '2021-02-03 14:05:34', '2021-02-03 14:06:38'),
(70, '153', 'tst', 'oiopiopiopiopiopkjjljkljklk\njkjhjhjhjjkjhjhjgghfghfgfgfgh\n\njghjghjghjghjghjghghjghjghjghggjgjh', NULL, NULL, NULL, '3', NULL, '2021-02-03 14:05:56', '2021-02-03 14:07:59'),
(71, '153', 'tst', 'oiiopiopiopiopiopiopiopoiopiopop', NULL, NULL, NULL, '5', '5', '2021-02-03 14:08:14', '2021-02-03 14:51:18'),
(72, '155', 'tst', 'sddgfgdffdgdfg', NULL, NULL, NULL, NULL, NULL, '2021-02-05 06:17:52', '2021-02-05 06:17:52'),
(73, '155', 'tst', 'asfsfdfdsdsf', 'PW6UgYvHot_7b93101a-8408-45f9-a0dd-d58c523200e8298478307370679147.jpg', NULL, NULL, NULL, NULL, '2021-02-05 06:21:00', '2021-02-05 06:21:00'),
(74, '155', 'tst', 'dgdfgdfgdfgdfgdfgfsdfdsfdsf', 'YiD9duldTQ_d97030bb-3f90-459e-85b2-7782816828623247568669817141074.jpg', NULL, NULL, '1', '1', '2021-02-05 06:21:22', '2021-02-05 14:10:10'),
(75, '66', 'tst', 'fghfghfghfghfg', NULL, NULL, NULL, NULL, NULL, '2021-02-05 14:40:42', '2021-02-05 14:40:42'),
(76, '66', 'tst', 'nfgnfghfghfgh', NULL, NULL, NULL, NULL, NULL, '2021-02-05 15:00:49', '2021-02-05 15:00:49'),
(77, '66', 'tst', 'vbnbvnbnnbn', NULL, NULL, NULL, NULL, NULL, '2021-02-05 15:03:10', '2021-02-05 15:03:10'),
(78, '66', 'tst', 'dfds', NULL, NULL, NULL, NULL, NULL, '2021-02-05 15:03:53', '2021-02-05 15:03:53'),
(79, '66', 'tst', 'hiii hii hii', '3LNT7Kc2vD_scaled_image_picker48614036958590896.jpg', NULL, NULL, '1', '1', '2021-02-06 05:55:50', '2021-02-06 05:59:04'),
(80, '66', 'tst', 'hhhhh', '5SKUm6f8Wk_scaled_image_picker7945532665217884379.jpg', NULL, NULL, NULL, NULL, '2021-02-06 06:11:28', '2021-02-06 06:11:28'),
(81, '66', 'tst', 'fghfg', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:16:52', '2021-02-06 06:16:52'),
(82, '66', 'tst', 'fghfghgfhgf', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:17:49', '2021-02-06 06:17:49'),
(83, '66', 'tst', 'dfgdfg', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:21:15', '2021-02-06 06:21:15'),
(84, '66', 'tst', 'tyutyu', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:22:44', '2021-02-06 06:22:44'),
(85, '158', 'tst', 'fghfgh', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:41:34', '2021-02-06 06:41:34'),
(86, '158', 'tst', 'fghjghj', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:42:22', '2021-02-06 06:42:22'),
(87, '158', 'tst', 'jkhjkj', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:42:52', '2021-02-06 06:42:52'),
(88, '158', 'tst', 'n,m,nm,', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:53:17', '2021-02-06 06:53:17'),
(89, '158', 'tst', 'jkljkljk', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:53:27', '2021-02-06 06:53:27'),
(90, '158', 'tst', 'ghjghjghjghjgh', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:54:10', '2021-02-06 06:54:10'),
(91, '158', 'tst', 'n,m,,nm,mn,mn,', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:55:31', '2021-02-06 06:55:31'),
(92, '158', 'tst', 'gfhfgh', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:58:17', '2021-02-06 06:58:17'),
(93, '158', 'tst', 'fghghfg', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:58:59', '2021-02-06 06:58:59'),
(94, '158', 'tst', 'fghghfg', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:59:03', '2021-02-06 06:59:03'),
(95, '158', 'tst', 'fghghfg', NULL, NULL, NULL, NULL, NULL, '2021-02-06 06:59:13', '2021-02-06 06:59:13'),
(96, '158', 'tst', 'ghjghjghjgh', NULL, NULL, NULL, NULL, NULL, '2021-02-06 07:08:06', '2021-02-06 07:08:06'),
(97, '158', 'tst', 'fghfghfg', NULL, NULL, NULL, NULL, NULL, '2021-02-06 07:08:52', '2021-02-06 07:08:52'),
(98, '158', 'tst', 'hjkhjkhjkhjkhjkjkhj', NULL, NULL, NULL, NULL, NULL, '2021-02-06 07:13:55', '2021-02-06 07:13:55'),
(99, '158', 'tst', 'lkk;kl;', NULL, NULL, NULL, NULL, NULL, '2021-02-06 07:20:36', '2021-02-06 07:20:36'),
(100, '158', 'tst', 'hjkhjkhjkhj', NULL, NULL, NULL, '1', '3', '2021-02-06 07:22:41', '2021-02-18 12:26:04'),
(101, '160', 'tst', 'hjghjghjgh', NULL, NULL, NULL, '1', '10', '2021-02-06 07:22:55', '2021-02-11 06:19:41'),
(102, '160', 'tst', 'fghfghfgh\n\n\nfghfghfgh', NULL, NULL, NULL, NULL, NULL, '2021-02-10 10:03:29', '2021-02-10 10:03:29'),
(103, '160', 'tst', 'https://youtu.be/FkNmTDGsh2s', NULL, NULL, '1', '3', NULL, '2021-02-10 10:08:34', '2021-02-10 13:39:48'),
(104, '160', 'tst', 'http://youtube', NULL, NULL, NULL, NULL, '1', '2021-02-10 10:18:17', '2021-02-10 12:24:05'),
(105, '160', 'tst', 'fghfghfghfghfg', 'GvlDJwS68M_3c4da255-3ec9-443d-8208-3e65fe4580bf279131644764931906.jpg', NULL, '1', '9', '3', '2021-02-10 10:20:34', '2021-02-12 15:40:34'),
(106, '95', 'tst', 'hhhhh', NULL, NULL, NULL, NULL, NULL, '2021-02-11 10:57:08', '2021-02-11 10:57:08'),
(107, '95', 'tst', 'hhiiii', 'MFu4NeAswz_scaled_image_picker3780221781494279299.jpg', NULL, NULL, '1', '1', '2021-02-11 11:03:28', '2021-02-12 07:24:05'),
(108, '95', 'tst', 'fhchcjrjrhby\nghhj', 'D6O891dXtA_1aad6cee-a6d2-4c69-8d46-fdd24e3b492d1554791204689263201.jpg', NULL, NULL, NULL, NULL, '2021-02-12 07:29:07', '2021-02-12 07:29:07'),
(109, '162', 'tst', '1234\n5678', NULL, NULL, NULL, NULL, NULL, '2021-02-12 15:40:01', '2021-02-12 15:40:01'),
(110, '67', 'tst', 'sfasdf\nasdf\nasdf\nasdf\nasfd\nasfdasdfasdfasdfasdfasdf asdfasdfa sdf as df a sdf as df as df a sdf  asdf a sdf  as df asdf', 'dtFFxWOQxB_72ff8854-9e52-4ab5-8fbe-45a1dab012434659846220385851178.jpg', NULL, NULL, '2', '2', '2021-02-18 12:24:56', '2021-02-18 12:38:45'),
(111, '135', 'tst', 'heart niceeeeeeee!!!', NULL, NULL, NULL, '2', '1', '2021-02-20 04:17:53', '2021-02-20 04:23:08'),
(112, '66', 'tst', 'post post post', 'ThyM0C7XII_5ba13f30-66c1-4505-b54d-9e87ad5ceb54742761201310177771.jpg', NULL, NULL, '1', NULL, '2021-02-20 05:13:47', '2021-02-20 05:17:11'),
(113, '66', 'tst', 'jdjfnfjfjfjfjjf', 'ByBLNw1jDG_8bc7490d-2919-49df-932d-c7a3b14438d86971903445882167142.jpg', NULL, NULL, NULL, NULL, '2021-02-20 10:59:29', '2021-02-20 10:59:29'),
(114, '66', 'tst', 'gdydydydtdydyddyhhhjjjjjk', 'JiXL3UQ0iO_bd6429e0-a19c-4914-bbeb-b7c75c0a25e83615514689417648162.jpg', NULL, NULL, '1', '1', '2021-02-20 11:02:39', '2021-04-06 06:00:51'),
(115, '154', 'tst', 'testtt', NULL, NULL, NULL, '1', NULL, '2021-03-07 15:29:50', '2021-03-07 15:30:05'),
(116, '164', 'tst', 'Gz hdhf ff', '3Ir9qkwL2T_scaled_image_picker1881543126.jpg', NULL, NULL, NULL, NULL, '2021-03-25 08:05:39', '2021-03-25 08:05:39'),
(117, '168', 'tst', 'great app. .', 'K2XGkw9PET_cc511ad1-f482-4607-8207-69b431ae38bc4211837232257156856.jpg', NULL, NULL, '2', '1', '2021-03-25 10:01:29', '2021-03-25 10:05:46'),
(118, '168', 'tst', 'jjlnkno', NULL, NULL, NULL, '2', '1', '2021-03-25 10:25:55', '2021-04-12 09:49:41'),
(119, '171', 'tst', 'hiii how are you', 'szp6iL66Gb_image_picker_5990F1C1-A8AF-4337-82C7-FAE17C4ABF79-32835-000009903DCA2B56.jpg', NULL, NULL, '1', NULL, '2021-03-30 10:29:14', '2021-03-30 10:58:06'),
(120, '171', 'tst', 'helllo all', 'CzwQLZbS8i_c97573a1-736d-4753-ab13-39af362508145133506322863173009.jpg', NULL, NULL, NULL, NULL, '2021-03-30 11:43:58', '2021-03-30 11:43:58'),
(121, '178', 'tst', 'fimmjdjdjdjd', NULL, NULL, NULL, '1', '2', '2021-03-31 10:57:15', '2021-03-31 10:57:57'),
(122, '154', 'tst', 'allo', NULL, NULL, NULL, NULL, NULL, '2021-03-31 19:02:38', '2021-03-31 19:02:38'),
(123, '171', 'tst', 'jssksk', NULL, NULL, NULL, NULL, NULL, '2021-04-05 05:29:03', '2021-04-05 05:29:03'),
(124, '154', 'tst', 'aaa', 'cie2pduYLR_image_picker_B5C411F6-C008-4075-B820-EFB2538950F0-33270-000005A7467B9F62.jpg', NULL, NULL, NULL, NULL, '2021-04-05 11:18:53', '2021-04-05 11:18:53'),
(125, '179', 'tst', 'chcfuuggugg', 'lk3CyWTn44_image_picker_D482DA1E-8FFB-4A36-9187-8B6156731BE0-415-0000000E67DC5A25.jpg', NULL, NULL, NULL, NULL, '2021-04-06 05:50:44', '2021-04-06 05:50:44'),
(126, '180', 'tst', 'newpostnewpost', NULL, NULL, NULL, '1', NULL, '2021-04-06 07:20:51', '2021-04-06 07:21:12'),
(127, '180', 'tst', 'babahzhahzg', '9aAXyM8fnn_scaled_image_picker2375217204538446552.jpg', NULL, NULL, '1', '1', '2021-04-06 07:23:04', '2021-04-06 09:41:28'),
(128, '180', 'tst', 'babiessaas', 'A6AEkv2Kxa_scaled_image_picker2673873582381107110.jpg', NULL, NULL, NULL, NULL, '2021-04-06 09:42:03', '2021-04-06 09:42:03'),
(129, '181', 'tst', 'creating post for babies ... hdhdjdjskdkjfhcufufufjjfjfjfjcjcjjcjcjcjcjjsjshwhwhwhshxjcjfjjfjrjdjxj', 'gNskKUa2Om_image_picker_BD346728-BC65-43FF-BB9C-135AEE01312E-921-000000810AEB9EBF.jpg', NULL, NULL, '1', '1', '2021-04-07 04:20:59', '2021-04-07 04:27:42'),
(130, '181', 'tst', 'llllo', NULL, NULL, NULL, NULL, NULL, '2021-04-07 04:31:48', '2021-04-07 04:31:48'),
(131, '185', 'tst', 'jvvjv', 'AGWEo5NyxU_5a9262d0-67c4-41fb-8bc2-636250caeabc4367096854603010113.jpg', NULL, NULL, '1', '1', '2021-04-08 04:12:05', '2021-04-08 04:12:37'),
(132, '179', 'imagepost', 'hello', NULL, NULL, NULL, NULL, NULL, '2021-04-08 05:26:02', '2021-04-08 05:26:02'),
(133, '179', 'test post', 'video testing', 'TchJbzi4ni_villa-with-orange-trees-nice.jpg', '9yCQtW7u5u_file_example_MP4_480_1_5MG.mp4', NULL, NULL, NULL, '2021-04-08 06:17:33', '2021-04-08 06:17:33'),
(134, '179', 'tst', 'hccchvvjjvvh', 'TxbjXwylAQ_scaled_image_picker-787463230.jpg', NULL, NULL, '1', NULL, '2021-04-08 08:12:10', '2021-04-12 09:50:11'),
(135, '179', 'testingvideo', 'videoooo', NULL, 'YQO4yDLqVr_file_example_MP4_480_1_5MG.mp4', NULL, NULL, NULL, '2021-04-08 08:20:40', '2021-04-08 08:20:40'),
(136, '179', 'testingvideo', 'jrfjgfigtdl', NULL, 'aGvWKcpAke_file_example_MP4_480_1_5MG.mp4', NULL, NULL, NULL, '2021-04-08 09:21:23', '2021-04-08 09:21:23'),
(137, '179', 'test post', 'video testing', NULL, 'WWY1GByZLC_file_example_MP4_480_1_5MG.mp4', NULL, NULL, NULL, '2021-04-08 10:17:34', '2021-04-08 10:17:34'),
(138, '179', 'test post', 'video testing', NULL, 'v5qrFuYKrt_file_example_MP4_480_1_5MG.mp4', NULL, NULL, NULL, '2021-04-08 10:28:11', '2021-04-08 10:28:11'),
(139, '179', 'test post', 'video testing', NULL, 'lTdgFkV3hK_file_example_MP4_640_3MG.mp4', NULL, NULL, NULL, '2021-04-08 10:33:34', '2021-04-08 10:33:34'),
(140, '179', 'test post', 'video testing', NULL, 'iSkuZnOCIl_file_example_MP4_1280_10MG.mp4', NULL, NULL, NULL, '2021-04-08 10:33:55', '2021-04-08 10:33:55'),
(141, '179', 'tst', 'Hfhhfbcbc', NULL, NULL, NULL, NULL, NULL, '2021-04-08 11:03:21', '2021-04-08 11:03:21'),
(142, '179', 'tst', 'Hello testing video', NULL, NULL, NULL, NULL, NULL, '2021-04-08 11:11:16', '2021-04-08 11:11:16'),
(143, '186', 'tst', 'chcchchchhccjvj', NULL, 'wLL67GizgA_image_picker284664970.jpg', NULL, NULL, NULL, '2021-04-08 11:20:51', '2021-04-08 11:20:51'),
(144, '186', 'tst', 'hxcxgdtyyfy', NULL, 'R98cXluk23_image_picker691809578.jpg', NULL, '2', '2', '2021-04-08 11:47:10', '2021-04-14 05:45:10'),
(145, '179', 'tst', 'testing imagesess', 'TJ9aeU09lJ_scaled_image_picker6879946500724983392.jpg', NULL, NULL, NULL, NULL, '2021-04-14 05:42:53', '2021-04-14 05:42:53'),
(146, '187', 'tst', 'gjgjgjg', 'YaZAYBJ2iv_scaled_image_picker4175118085486192911.jpg', NULL, NULL, '2', NULL, '2021-04-15 05:32:43', '2021-04-15 10:41:09'),
(147, '188', 'tst', 'cvvvvvvvvvv', 'lNwHdsAk7h_image_picker_EE8C010B-DDF8-4217-B96A-07C5B1DA8BB2-2729-0000029085693DDB.jpg', NULL, NULL, NULL, NULL, '2021-04-15 10:32:42', '2021-04-15 10:32:42'),
(148, '188', 'tst', 'igcuxtxtiitxiycccgciydccifiiyifyoycoyoyccocyooyxoccoyyccyyyyyyyyyyyyyuuuuuhhhh iiijbbbbnnn', '8I1h4jAGTu_image_picker_EA06E45F-A008-48D7-A387-8288A6481E21-2729-000002921578EE03.jpg', NULL, NULL, '1', NULL, '2021-04-15 10:37:16', '2021-04-15 10:54:45'),
(149, '190', 'tst', 'Fccggv', 'ZXKElyqUsG_scaled_image_picker1838484110.jpg', NULL, NULL, NULL, NULL, '2021-04-15 11:04:01', '2021-04-15 11:04:01'),
(150, '188', 'tst', 'ghfyrzyz', 'x5pOnwLwts_image_picker_FDF5DA25-11A9-4FD0-B720-0CC45ABFC02D-2729-0000029BDC75AA66.jpg', NULL, NULL, NULL, NULL, '2021-04-15 11:06:28', '2021-04-15 11:06:28'),
(151, '190', 'tst', 'Chfhggfhffhfh', NULL, NULL, NULL, NULL, NULL, '2021-04-15 11:24:47', '2021-04-15 11:24:47'),
(152, '190', 'tst', 'Gftfgg', NULL, NULL, NULL, NULL, NULL, '2021-04-15 11:28:07', '2021-04-15 11:28:07'),
(153, '190', 'tst', 'Gftdgpvgjhfgddyhff', NULL, NULL, NULL, NULL, NULL, '2021-04-15 11:29:08', '2021-04-15 11:29:08'),
(154, '190', 'tst', 'Vcdjfgjnvvj', NULL, NULL, NULL, NULL, NULL, '2021-04-15 11:32:04', '2021-04-15 11:32:04'),
(155, '190', 'tst', 'Eeeeeeeeeee', NULL, NULL, NULL, NULL, NULL, '2021-04-15 11:32:17', '2021-04-15 11:32:17'),
(156, '179', 'tst', 'hhjfhrsthyh', NULL, NULL, NULL, NULL, NULL, '2021-04-15 12:58:21', '2021-04-15 12:58:21'),
(157, '179', 'tst', 'hhhhhhh', NULL, NULL, NULL, NULL, NULL, '2021-04-15 13:01:36', '2021-04-15 13:01:36'),
(158, '179', 'tst', 'jjjjjjjj', NULL, NULL, NULL, NULL, NULL, '2021-04-15 13:03:21', '2021-04-15 13:03:21'),
(159, '188', 'tst', 'fdtgvbhgg', 'FZQnmy1ZF5_image_picker_A64A184A-E54E-4DCA-A905-04D803DAAABF-2796-00000319B37CEB36.jpg', NULL, NULL, NULL, NULL, '2021-04-16 04:05:56', '2021-04-16 04:05:56'),
(160, '179', 'tst', 'xvhxhdjjg', 'YkBDCxXzl0_image_picker_B3EF246B-9D65-475B-BE99-E0A6A30686D9-2806-0000031FC64367E8.jpg', NULL, NULL, NULL, NULL, '2021-04-16 04:24:02', '2021-04-16 04:24:02'),
(161, '179', 'tst', 'hhfcghh', 'gsQAdYjTZp_image_picker_D34281E1-6392-431E-9CEB-F4D1B78DD491-2806-000003206B3A2B59.jpg', NULL, NULL, NULL, NULL, '2021-04-16 04:26:06', '2021-04-16 04:26:06'),
(162, '179', 'tst', 'cghcfhcch', 'xEGgOPPijp_image_picker_7F26FB85-906F-44B8-9ECC-EE50A6B158CD-2806-00000320CC8275D2.jpg', NULL, NULL, NULL, NULL, '2021-04-16 04:27:09', '2021-04-16 04:27:09'),
(163, '179', 'tst', 'chcfhufuij', 'dGOJClDDoN_image_picker_062EB7C1-BFAA-41EC-8699-B004A5822D74-2853-000003246789C221.jpg', NULL, NULL, NULL, NULL, '2021-04-16 04:37:49', '2021-04-16 04:37:49'),
(164, '179', 'tst', 'cgufdydyyffhhffhfuffhfufuf', 'D3qEhNSq2Y_image_picker_10F51177-AE83-4D67-82C6-C9595ABD56E9-2853-00000325D6D2C2BF.jpg', NULL, NULL, NULL, NULL, '2021-04-16 04:42:05', '2021-04-16 04:42:05'),
(165, '179', 'tst', 'hello all', 'rcPJrjBlmX_image_picker_C9F3D39F-1352-4520-BD20-27F279CAFCCD-2853-00000326617E9A0A.jpg', NULL, NULL, NULL, '2', '2021-04-16 04:43:45', '2021-07-28 11:02:29'),
(166, '179', 'tst', 'detfhjhhbhg', 'sPPB8XGR2w_image_picker_281ECB1F-650E-42C8-908F-91B79D12C9C7-2853-0000032740FE901B.jpg', NULL, NULL, NULL, NULL, '2021-04-16 04:46:19', '2021-04-16 04:46:19'),
(167, '179', 'tst', 'hello', NULL, NULL, NULL, NULL, NULL, '2021-04-16 04:50:36', '2021-04-16 04:50:36'),
(168, '179', 'tst', 'hello', NULL, NULL, NULL, NULL, NULL, '2021-04-16 05:06:46', '2021-04-16 05:06:46'),
(169, '179', 'tst', 'Hello', NULL, NULL, NULL, NULL, NULL, '2021-04-16 05:47:50', '2021-04-16 05:47:50'),
(170, '179', 'tst', 'Vzhzjxnc', NULL, NULL, NULL, NULL, NULL, '2021-04-16 05:57:59', '2021-04-16 05:57:59'),
(171, '179', 'tst', 'Test post', NULL, NULL, NULL, NULL, NULL, '2021-04-16 06:04:51', '2021-04-16 06:04:51'),
(172, '67', 'tst', 'Tst2', NULL, NULL, NULL, NULL, NULL, '2021-04-16 06:33:38', '2021-04-16 06:33:38'),
(173, '67', 'tst', 'Tst3', NULL, NULL, NULL, NULL, NULL, '2021-04-16 06:34:54', '2021-04-16 06:34:54'),
(174, '67', 'tst', 'Tst4', NULL, NULL, NULL, NULL, NULL, '2021-04-16 06:38:30', '2021-04-16 06:38:30'),
(175, '191', 'tst', 'tesying post refres', '3ZetjjbNCv_scaled_image_picker1269707851.jpg', NULL, NULL, '1', NULL, '2021-04-16 07:10:37', '2021-07-27 11:07:05'),
(176, '174', 'tst', 'testing', 'IdPF98dGK2_image_picker_B2712475-11E6-4F50-A8CF-BEBC8D6E38C6-3001-0000035B43DF6BC2.jpg', NULL, NULL, NULL, NULL, '2021-04-16 07:21:29', '2021-04-16 07:21:29'),
(177, '192', 'tst', 'cghjjhj', NULL, NULL, NULL, NULL, NULL, '2021-04-16 09:32:08', '2021-04-16 09:32:08'),
(178, '192', 'tst', 'bc ddbdhd', 'EC2IRf7Tw0_image_picker_3C07E66D-1027-4E9F-9567-1D6958C8DEF1-3129-00000387411E9A56.jpg', NULL, NULL, '4', '6', '2021-04-16 09:32:45', '2021-08-02 12:11:16');

-- --------------------------------------------------------

--
-- Table structure for table `post_comments`
--

CREATE TABLE `post_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_comments`
--

INSERT INTO `post_comments` (`id`, `user_id`, `post_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, '160', '103', 'testing Today vee', '2021-02-10 13:38:16', '2021-02-10 13:38:16'),
(2, '160', '103', 'testing Today vee', '2021-02-10 13:39:00', '2021-02-10 13:39:00'),
(3, '160', '103', 'testing Today vee', '2021-02-10 13:39:48', '2021-02-10 13:39:48'),
(4, '160', '105', 'dfgdfgdfgdfgdfg', '2021-02-10 13:41:01', '2021-02-10 13:41:01'),
(5, '160', '105', 'dfgdfgdfgdfgdfgdfgdfgdfgdf', '2021-02-10 13:41:11', '2021-02-10 13:41:11'),
(6, '160', '105', 'dfgdfsgdfsgdfg', '2021-02-10 13:41:18', '2021-02-10 13:41:18'),
(7, '160', '105', 'dsfgdfgdfgfg', '2021-02-10 13:41:25', '2021-02-10 13:41:25'),
(8, '160', '105', 'fhfghghfg', '2021-02-10 13:56:27', '2021-02-10 13:56:27'),
(9, '160', '105', 'ghfghfgh', '2021-02-10 14:26:47', '2021-02-10 14:26:47'),
(10, '160', '101', 'jkhgjkhjkhgjk', '2021-02-11 06:19:41', '2021-02-11 06:19:41'),
(11, '95', '105', 'ggggg', '2021-02-11 10:57:44', '2021-02-11 10:57:44'),
(12, '95', '107', 'hiii', '2021-02-12 07:24:05', '2021-02-12 07:24:05'),
(13, '162', '105', 'test2', '2021-02-12 15:40:25', '2021-02-12 15:40:25'),
(14, '67', '110', 'this is comment in reply section \nthis is second line of reply', '2021-02-18 12:25:38', '2021-02-18 12:25:38'),
(15, '67', '100', 'reply posted', '2021-02-18 12:26:04', '2021-02-18 12:26:04'),
(16, '67', '110', 'second reply', '2021-02-18 12:32:34', '2021-02-18 12:32:34'),
(17, '135', '111', 'braveeee', '2021-02-20 04:19:23', '2021-02-20 04:19:23'),
(18, '135', '111', 'good', '2021-02-20 04:23:08', '2021-02-20 04:23:08'),
(19, '66', '112', 'baby', '2021-02-20 05:17:11', '2021-02-20 05:17:11'),
(20, '154', '115', '1234', '2021-03-07 15:30:05', '2021-03-07 15:30:05'),
(21, '168', '117', 'good', '2021-03-25 10:03:48', '2021-03-25 10:03:48'),
(22, '168', '117', 'bravo', '2021-03-25 10:04:08', '2021-03-25 10:04:08'),
(23, '168', '118', 'aaaaaaa', '2021-03-25 10:28:34', '2021-03-25 10:28:34'),
(24, '171', '119', 'tthhh', '2021-03-30 10:58:06', '2021-03-30 10:58:06'),
(25, '178', '121', 'good', '2021-03-31 10:57:57', '2021-03-31 10:57:57'),
(26, '174', '114', 'NCOs\njxjx\ndndkn', '2021-04-06 06:00:51', '2021-04-06 06:00:51'),
(27, '180', '126', 'good', '2021-04-06 07:21:12', '2021-04-06 07:21:12'),
(28, '180', '127', 'fine', '2021-04-06 09:41:28', '2021-04-06 09:41:28'),
(29, '181', '129', 'fine fine', '2021-04-07 04:27:42', '2021-04-07 04:27:42'),
(30, '185', '131', 'hjjjj', '2021-04-08 04:12:37', '2021-04-08 04:12:37'),
(31, '154', '118', 'a', '2021-04-12 09:49:41', '2021-04-12 09:49:41'),
(32, '154', '134', 'fff', '2021-04-12 09:50:11', '2021-04-12 09:50:11'),
(33, '154', '144', 'uu', '2021-04-12 17:41:50', '2021-04-12 17:41:50'),
(34, '179', '144', 'fufufhffhhf\nhh', '2021-04-13 12:39:30', '2021-04-13 12:39:30'),
(35, '187', '146', '5ydyff', '2021-04-15 05:38:24', '2021-04-15 05:38:24'),
(36, '188', '146', 'cccccxxf', '2021-04-15 10:41:09', '2021-04-15 10:41:09'),
(37, '188', '148', 'bbbbhb', '2021-04-15 10:54:45', '2021-04-15 10:54:45'),
(38, '192', '178', 'dhdh', '2021-04-16 09:33:06', '2021-04-16 09:33:06'),
(39, '192', '178', 'chhchcjjchc', '2021-04-16 09:34:17', '2021-04-16 09:34:17'),
(40, '203', '175', 'Test', '2021-07-27 11:07:05', '2021-07-27 11:07:05'),
(41, '205', '178', 'Eheheh', '2021-07-30 05:01:59', '2021-07-30 05:01:59'),
(42, '204', '178', 'het', '2021-08-02 12:11:16', '2021-08-02 12:11:16');

-- --------------------------------------------------------

--
-- Table structure for table `post_likes`
--

CREATE TABLE `post_likes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `likeStatus` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_likes`
--

INSERT INTO `post_likes` (`id`, `user_id`, `post_id`, `likeStatus`, `created_at`, `updated_at`) VALUES
(1, '160', '105', '1', '2021-02-10 12:37:31', '2021-02-11 06:19:21'),
(2, '123', '105', '1', '2021-02-10 12:45:33', '2021-02-10 12:53:01'),
(3, '139', '105', '0', '2021-02-10 12:53:55', '2021-02-10 12:56:44'),
(4, '140', '105', '1', '2021-02-10 12:56:58', '2021-02-10 12:57:06'),
(5, '141', '105', '1', '2021-02-10 12:57:19', '2021-02-10 12:57:19'),
(6, '160', '104', '1', '2021-02-10 12:58:11', '2021-02-10 12:58:11'),
(7, '160', '103', '1', '2021-02-10 12:58:49', '2021-02-10 12:58:49'),
(8, '160', '101', '1', '2021-02-11 06:19:28', '2021-02-11 06:19:28'),
(9, '95', '107', '1', '2021-02-12 07:23:51', '2021-02-12 07:23:51'),
(10, '162', '108', '1', '2021-02-12 15:40:07', '2021-02-12 15:40:07'),
(11, '67', '110', '0', '2021-02-18 12:32:39', '2021-02-18 12:38:03'),
(12, '135', '111', '1', '2021-02-20 04:18:00', '2021-02-20 04:18:00'),
(13, '66', '112', '1', '2021-02-20 05:14:06', '2021-02-20 05:14:06'),
(14, '28', '14', '0', '2021-02-22 11:03:29', '2021-02-22 11:05:21'),
(15, '28', '15', '0', '2021-02-22 11:05:28', '2021-02-22 11:09:40'),
(16, '28', '12', '1', '2021-02-22 11:17:17', '2021-02-22 11:17:17'),
(17, '147', '113', '1', '2021-03-18 11:49:55', '2021-03-18 11:49:55'),
(18, '147', '112', '1', '2021-03-18 11:50:08', '2021-03-18 11:50:08'),
(19, '168', '117', '1', '2021-03-25 10:03:27', '2021-03-25 10:03:27'),
(20, '178', '121', '1', '2021-03-31 10:57:21', '2021-03-31 10:57:21'),
(21, '180', '126', '1', '2021-04-06 07:21:00', '2021-04-06 07:21:00'),
(22, '180', '128', '1', '2021-04-06 09:42:09', '2021-04-06 09:42:09'),
(23, '181', '129', '1', '2021-04-07 04:24:53', '2021-04-07 04:24:53'),
(24, '185', '131', '1', '2021-04-08 04:12:21', '2021-04-08 04:12:21'),
(25, '154', '144', '1', '2021-04-12 17:42:06', '2021-04-12 17:42:06'),
(26, '179', '145', '1', '2021-04-14 05:44:31', '2021-04-14 05:44:31'),
(27, '187', '146', '1', '2021-04-15 05:38:09', '2021-04-15 05:38:09'),
(28, '192', '178', '1', '2021-04-16 09:32:52', '2021-04-16 09:32:52'),
(29, '203', '175', '1', '2021-07-27 11:06:52', '2021-07-27 11:06:52'),
(30, '203', '178', '0', '2021-07-28 10:57:21', '2021-07-28 10:57:24'),
(31, '204', '165', '0', '2021-07-28 11:02:18', '2021-07-28 11:02:24'),
(32, '205', '178', '0', '2021-07-30 05:02:35', '2021-08-02 13:11:10'),
(33, '204', '178', '1', '2021-08-02 12:11:22', '2021-08-02 12:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `post_share`
--

CREATE TABLE `post_share` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `share` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_share`
--

INSERT INTO `post_share` (`id`, `user_id`, `post_id`, `share`, `created_at`, `updated_at`) VALUES
(1, '16', '14', '1', '2020-12-01 13:48:00', '2020-12-01 13:48:00'),
(2, '16', '14', '0', '2020-12-01 13:48:21', '2020-12-01 13:48:21'),
(3, '16', '14', '1', '2020-12-01 13:49:10', '2020-12-01 13:49:10'),
(4, '16', '14', '1', '2020-12-02 05:43:01', '2020-12-02 05:43:01'),
(5, '16', '14', '1', '2020-12-30 08:29:23', '2020-12-30 08:29:23'),
(6, '16', '14', '1', '2020-12-31 11:31:49', '2020-12-31 11:31:49'),
(7, '16', '14', '1', '2020-12-31 11:39:30', '2020-12-31 11:39:30'),
(8, '16', '14', '1', '2020-12-31 11:40:31', '2020-12-31 11:40:31'),
(9, '16', '14', '1', '2020-12-31 11:40:35', '2020-12-31 11:40:35'),
(10, '16', '14', '1', '2020-12-31 11:40:37', '2020-12-31 11:40:37'),
(11, '16', '14', '1', '2020-12-31 11:41:26', '2020-12-31 11:41:26'),
(12, '16', '14', '1', '2020-12-31 11:41:32', '2020-12-31 11:41:32'),
(13, '16', '14', '1', '2020-12-31 11:41:58', '2020-12-31 11:41:58'),
(14, '16', '14', '1', '2020-12-31 11:42:02', '2020-12-31 11:42:02'),
(15, '16', '14', '1', '2020-12-31 11:46:30', '2020-12-31 11:46:30'),
(16, '16', '14', '0', '2020-12-31 11:46:33', '2020-12-31 11:46:33'),
(17, '16', '14', '1', '2020-12-31 11:57:32', '2020-12-31 11:57:32'),
(18, '15', '15', '1', '2021-01-28 07:13:25', '2021-01-28 07:13:25'),
(19, '15', '15', '1', '2021-01-28 07:14:03', '2021-01-28 07:14:03'),
(20, '16', '14', '1', '2021-01-29 06:58:42', '2021-01-29 06:58:42'),
(21, '66', '64', '1', '2021-01-29 07:42:51', '2021-01-29 07:42:51'),
(22, '135', '65', '1', '2021-01-29 09:48:22', '2021-01-29 09:48:22'),
(23, '16', '14', '1', '2021-01-29 10:25:34', '2021-01-29 10:25:34'),
(24, '16', '14', '1', '2021-01-29 10:37:48', '2021-01-29 10:37:48'),
(25, '16', '14', '1', '2021-01-29 10:38:20', '2021-01-29 10:38:20'),
(26, '16', '14', '1', '2021-01-29 10:39:04', '2021-01-29 10:39:04'),
(27, '16', '14', '1', '2021-01-29 10:39:18', '2021-01-29 10:39:18'),
(28, '16', '14', '1', '2021-01-29 11:18:10', '2021-01-29 11:18:10'),
(29, '16', '14', '1', '2021-01-29 11:20:16', '2021-01-29 11:20:16'),
(30, '16', '14', '1', '2021-01-29 11:20:29', '2021-01-29 11:20:29'),
(31, '16', '14', '1', '2021-01-29 11:23:59', '2021-01-29 11:23:59'),
(32, '16', '14', '1', '2021-01-29 11:24:03', '2021-01-29 11:24:03'),
(33, '16', '14', '1', '2021-01-29 11:27:14', '2021-01-29 11:27:14'),
(34, '16', '14', '1', '2021-01-29 11:27:28', '2021-01-29 11:27:28'),
(35, '135', '60', '1', '2021-01-30 06:19:11', '2021-01-30 06:19:11'),
(36, '138', '67', '1', '2021-02-01 07:22:39', '2021-02-01 07:22:39'),
(37, '153', '71', '1', '2021-02-03 14:47:20', '2021-02-03 14:47:20'),
(38, '153', '71', '1', '2021-02-03 14:47:24', '2021-02-03 14:47:24'),
(39, '153', '71', '1', '2021-02-03 14:48:32', '2021-02-03 14:48:32'),
(40, '153', '71', '1', '2021-02-03 14:51:00', '2021-02-03 14:51:00'),
(41, '153', '71', '1', '2021-02-03 14:51:18', '2021-02-03 14:51:18'),
(42, '66', '74', '1', '2021-02-05 14:10:10', '2021-02-05 14:10:10'),
(43, '66', '79', '1', '2021-02-06 05:59:04', '2021-02-06 05:59:04'),
(44, '16', '14', '1', '2021-02-06 06:02:48', '2021-02-06 06:02:48'),
(45, '16', '14', '1', '2021-02-06 06:02:56', '2021-02-06 06:02:56'),
(46, '159', '101', '1', '2021-02-06 07:30:47', '2021-02-06 07:30:47'),
(47, '159', '101', '1', '2021-02-06 10:21:07', '2021-02-06 10:21:07'),
(48, '159', '101', '1', '2021-02-06 10:21:17', '2021-02-06 10:21:17'),
(49, '159', '101', '1', '2021-02-06 10:21:21', '2021-02-06 10:21:21'),
(50, '159', '101', '1', '2021-02-06 10:21:24', '2021-02-06 10:21:24'),
(51, '159', '101', '1', '2021-02-06 10:21:26', '2021-02-06 10:21:26'),
(52, '159', '100', '1', '2021-02-06 10:21:29', '2021-02-06 10:21:29'),
(53, '159', '100', '1', '2021-02-06 10:21:33', '2021-02-06 10:21:33'),
(54, '159', '100', '1', '2021-02-06 10:21:37', '2021-02-06 10:21:37'),
(55, '160', '101', '1', '2021-02-10 09:47:20', '2021-02-10 09:47:20'),
(56, '160', '101', '1', '2021-02-10 09:47:23', '2021-02-10 09:47:23'),
(57, '160', '101', '1', '2021-02-10 09:47:25', '2021-02-10 09:47:25'),
(58, '160', '101', '1', '2021-02-10 09:50:32', '2021-02-10 09:50:32'),
(59, '160', '104', '1', '2021-02-10 12:24:05', '2021-02-10 12:24:05'),
(60, '160', '105', '1', '2021-02-10 12:58:24', '2021-02-10 12:58:24'),
(61, '160', '105', '1', '2021-02-10 12:58:30', '2021-02-10 12:58:30'),
(62, '95', '107', '1', '2021-02-11 11:03:38', '2021-02-11 11:03:38'),
(63, '162', '105', '1', '2021-02-12 15:40:34', '2021-02-12 15:40:34'),
(64, '16', '14', '1', '2021-02-16 11:02:37', '2021-02-16 11:02:37'),
(65, '67', '110', '1', '2021-02-18 12:38:34', '2021-02-18 12:38:34'),
(66, '67', '110', '1', '2021-02-18 12:38:45', '2021-02-18 12:38:45'),
(67, '16', '14', '1', '2021-02-19 09:52:40', '2021-02-19 09:52:40'),
(68, '135', '111', '1', '2021-02-20 04:22:03', '2021-02-20 04:22:03'),
(69, '66', '114', '1', '2021-02-20 11:03:49', '2021-02-20 11:03:49'),
(70, '168', '117', '1', '2021-03-25 10:05:46', '2021-03-25 10:05:46'),
(71, '178', '121', '1', '2021-03-31 10:57:30', '2021-03-31 10:57:30'),
(72, '178', '121', '1', '2021-03-31 10:57:44', '2021-03-31 10:57:44'),
(73, '180', '127', '1', '2021-04-06 08:11:39', '2021-04-06 08:11:39'),
(74, '181', '129', '1', '2021-04-07 04:25:03', '2021-04-07 04:25:03'),
(75, '185', '131', '1', '2021-04-08 04:12:27', '2021-04-08 04:12:27'),
(76, '154', '118', '1', '2021-04-12 09:49:32', '2021-04-12 09:49:32'),
(77, '154', '144', '1', '2021-04-12 17:42:00', '2021-04-12 17:42:00'),
(78, '179', '144', '1', '2021-04-14 05:45:10', '2021-04-14 05:45:10'),
(79, '154', '178', '1', '2021-04-19 17:37:16', '2021-04-19 17:37:16'),
(80, '154', '178', '1', '2021-04-19 17:43:13', '2021-04-19 17:43:13'),
(81, '204', '165', '1', '2021-07-28 11:02:28', '2021-07-28 11:02:28'),
(82, '204', '165', '1', '2021-07-28 11:02:29', '2021-07-28 11:02:29'),
(83, '205', '178', '1', '2021-07-30 04:53:42', '2021-07-30 04:53:42'),
(84, '205', '178', '1', '2021-07-30 04:53:43', '2021-07-30 04:53:43'),
(85, '205', '178', '1', '2021-07-30 04:53:43', '2021-07-30 04:53:43'),
(86, '205', '178', '1', '2021-07-30 04:54:55', '2021-07-30 04:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `post_sharedby_users`
--

CREATE TABLE `post_sharedby_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_collections`
--

CREATE TABLE `product_collections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci,
  `shop_collection` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discounted_price` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_collections`
--

INSERT INTO `product_collections` (`id`, `title`, `picture`, `shop_collection`, `price`, `discounted_price`, `created_at`, `updated_at`, `description`) VALUES
(5, 'toy', '1605598816.jpeg', '2', '500', '0', '2020-11-16 12:19:09', '2020-12-22 09:40:07', 'This is test'),
(9, 'testing', '1605598816.jpeg', '3', '3000', '0', '2020-11-17 10:21:51', '2020-12-01 10:42:08', 'description'),
(11, 'Shop Fit Match', '1606819278.webp', '3', '200', '0', '2020-11-17 11:48:46', '2020-11-17 11:48:46', 'tt'),
(12, 'Test', '1605598816.jpeg', '3', '256', '0', '2020-12-22 06:08:51', '2020-12-22 06:08:51', 'Testing'),
(13, 'Test', '1605598816.jpeg', '2', '159', '0', '2020-12-23 08:11:47', '2020-12-23 08:11:47', 'Testing'),
(14, 'Test', '1605598816.jpeg', '3', '256', '0', '2020-12-23 12:25:09', '2020-12-23 12:25:09', 'abc'),
(15, 'Test', 'Z7YdY7oCUe_about_us.png', '2', '20000', '10000', '2021-01-06 13:13:41', '2021-01-07 05:05:17', 'Testing'),
(16, 'Test', 'AYbsOMNUtp_about_image.png', '2', '20', '100', '2021-01-06 14:04:16', '2021-01-07 05:05:47', 'Testing'),
(17, 'pk testing', 'wVMLCRSYqW_about_image.png', '2', '20', '14', '2021-01-06 14:12:38', '2021-01-06 14:12:38', 'This is test'),
(18, 'Test', '/tmp/phpKOJrZG', '2', '20', '1012', '2021-01-07 05:48:23', '2021-01-08 10:00:59', 'This is test'),
(19, 'Test', '/tmp/phpsSh0bv', '3', '256', '14', '2021-01-07 05:51:52', '2021-01-07 05:51:52', 'Testing'),
(20, 'Test', '/tmp/phpwDVwBz|/tmp/phpRgSOaI|/tmp/phpcCEHtR|/tmp/php1InJM0', '5', '20', '14', '2021-01-07 05:58:02', '2021-01-07 05:58:02', 'This is test'),
(21, 'Test', '/tmp/phprOG1hM', '2', '20', '10000', '2021-01-07 06:02:16', '2021-01-07 06:02:16', 'This is test'),
(22, 'Grade 7', '/tmp/phpLIWzLf|/tmp/php9SoFYa', '2', '20', '14', '2021-01-07 06:04:57', '2021-01-07 06:04:57', '1'),
(23, 'Test', '', '2', '20', '14', '2021-01-07 06:44:33', '2021-01-07 06:44:33', 'Testing'),
(24, 'Test', '', '2', '20', '14', '2021-01-07 06:48:20', '2021-01-07 06:48:20', 'Testing'),
(25, 'Test', '', '2', '20', '14', '2021-01-07 06:48:34', '2021-01-07 06:48:34', 'This is test'),
(26, 'Test', '', '3', '20', '14', '2021-01-07 07:19:59', '2021-01-07 07:19:59', '1'),
(27, 'Test', '', '3', '20', '14', '2021-01-07 07:22:29', '2021-01-07 07:22:29', '1'),
(28, 'Test', '', '3', '20', '14', '2021-01-07 07:23:12', '2021-01-07 07:23:12', '1'),
(29, 'Test', '', '3', '20', '14', '2021-01-07 07:24:08', '2021-01-07 07:24:08', '1'),
(30, 'Test', '', '3', '20', '14', '2021-01-07 07:26:50', '2021-01-07 07:26:50', '1'),
(31, 'Test', '', '3', '20', '14', '2021-01-07 07:31:23', '2021-01-07 07:31:23', '1'),
(32, 'Test', '', '3', '20', '14', '2021-01-07 07:31:50', '2021-01-07 07:31:50', 'Testing'),
(33, 'Test', '', '3', '20', '14', '2021-01-07 07:34:18', '2021-01-07 07:34:18', 'Testing'),
(34, 'Test', '', '3', '20', '14', '2021-01-07 07:37:22', '2021-01-07 07:37:22', 'Testing'),
(35, 'Test', '', '3', '20', '14', '2021-01-07 07:38:15', '2021-01-07 07:38:15', 'Testing'),
(36, 'Test', '', '3', '20', '14', '2021-01-07 07:39:42', '2021-01-07 07:39:42', 'Testing'),
(37, 'Test', '', '3', '20', '14', '2021-01-07 07:40:10', '2021-01-07 07:40:10', 'Testing'),
(38, 'Test', '', '3', '20', '14', '2021-01-07 07:41:26', '2021-01-07 07:41:26', 'Testing'),
(39, 'Test', '', '3', '20', '14', '2021-01-07 07:42:45', '2021-01-07 07:42:45', 'Testing'),
(40, 'Test', '', '3', '20', '14', '2021-01-07 07:43:07', '2021-01-07 07:43:07', 'Testing'),
(41, 'Test', '', '3', '20', '14', '2021-01-07 07:43:34', '2021-01-07 07:43:34', 'Testing'),
(42, 'Test', '', '3', '20', '14', '2021-01-07 07:44:09', '2021-01-07 07:44:09', 'Testing'),
(43, 'Test', '', '3', '20', '14', '2021-01-07 07:49:52', '2021-01-07 07:49:52', 'Testing'),
(44, 'Test', '', '3', '20', '14', '2021-01-07 07:55:34', '2021-01-07 07:55:34', 'Testing'),
(45, 'Test', '', '3', '20', '14', '2021-01-07 07:56:37', '2021-01-07 07:56:37', 'Testing'),
(46, 'Test', '', '3', '20', '14', '2021-01-07 07:58:05', '2021-01-07 07:58:05', 'Testing'),
(47, 'Test', NULL, '3', '20', '14', '2021-01-07 08:01:37', '2021-01-07 08:01:37', 'Testing'),
(48, 'Test', NULL, '3', '20', '14', '2021-01-07 08:05:36', '2021-01-07 08:05:36', 'Testing'),
(49, 'Test', NULL, '3', '20', '14', '2021-01-07 08:05:59', '2021-01-07 08:05:59', 'Testing'),
(50, 'Test', NULL, '2', '20', '14', '2021-01-07 08:07:56', '2021-01-07 08:07:56', 'Testing'),
(51, 'Test', NULL, '2', '20', '14', '2021-01-07 08:17:50', '2021-01-07 10:05:50', 'Testing'),
(52, 'Testpkkk', NULL, '2', '20', '14', '2021-01-07 13:01:08', '2021-01-07 13:30:58', 'Testing'),
(53, 'sam', NULL, '5', '20', '10000', '2021-01-08 09:39:45', '2021-01-08 09:39:45', '1'),
(54, 'Test2pm', NULL, '2', '20', '10', '2021-01-08 10:01:32', '2021-01-08 10:01:32', 'This is test'),
(55, 'Test', NULL, '5', '20', '100', '2021-01-11 04:57:48', '2021-01-11 04:57:48', '1'),
(56, 'Test', NULL, '3', '20', '10', '2021-01-11 09:59:35', '2021-01-11 09:59:35', 'Testing'),
(57, 'Test', NULL, '3', '20', '101', '2021-01-11 10:31:00', '2021-01-11 10:31:00', 'Testing'),
(58, 'Test', NULL, '3', '20', '101', '2021-01-11 10:33:30', '2021-01-11 10:33:30', 'Testing'),
(59, 'Test', NULL, '3', '20', '101', '2021-01-11 10:38:38', '2021-01-11 10:38:38', 'Testing'),
(60, 'Test', NULL, '3', '20', '101', '2021-01-11 10:46:48', '2021-01-11 10:46:48', 'Testing'),
(61, 'Test', NULL, '3', '20', '101', '2021-01-11 10:48:25', '2021-01-11 10:48:25', 'Testing'),
(62, 'Test', NULL, '5', '20', '10000', '2021-01-11 11:14:05', '2021-01-11 11:14:05', 'Testing'),
(63, 'Baby product', NULL, '3', '20', '10', '2021-01-11 11:19:58', '2021-01-11 11:19:58', 'Baby Testing'),
(64, 'Baby product', NULL, '3', '20', '10', '2021-01-11 11:21:42', '2021-01-11 11:21:42', 'Baby Testing'),
(65, 'Baby product', NULL, '3', '20', '10', '2021-01-11 11:22:03', '2021-01-11 11:22:03', 'Baby Testing'),
(66, 'Baby product', NULL, '3', '20', '10', '2021-01-11 11:23:59', '2021-01-11 11:23:59', 'Baby Testing'),
(67, 'Baby', NULL, '3', '20', '10', '2021-01-11 11:24:27', '2021-01-11 11:24:27', 'Baby Testing'),
(68, 'Baby', NULL, '3', '20', '10', '2021-01-11 11:25:11', '2021-01-11 11:25:11', 'Baby Testing'),
(69, 'Test Product 5pm', NULL, '3', '2000', '100', '2021-01-11 11:31:45', '2021-01-11 11:31:45', 'Testing'),
(70, 'Test Product 5:5pm', NULL, '3', '20', '14', '2021-01-11 11:36:07', '2021-01-11 11:36:07', 'Testing'),
(71, 'Test Product  5:15', NULL, '3', '200', '50', '2021-01-11 11:46:45', '2021-01-11 11:46:45', 'Baby Testing'),
(72, 'Test', NULL, '3', '20', '10', '2021-01-11 12:12:29', '2021-01-11 12:12:29', 'Testing'),
(73, 'Test', NULL, '3', '20', '10', '2021-01-11 12:34:25', '2021-01-11 12:34:25', 'Testing'),
(74, 'Test', NULL, '3', '20', '10', '2021-01-11 12:35:13', '2021-01-11 12:35:13', 'Testing'),
(75, 'Test', NULL, '3', '20', '10', '2021-01-11 12:35:55', '2021-01-11 12:35:55', 'Testing'),
(76, 'Test Product', NULL, '3', '1000', '100', '2021-07-30 09:24:17', '2021-07-30 09:24:17', 'Test product\'s description'),
(77, 'Scientific Courses', NULL, '3', '12', '32', '2021-07-30 19:29:46', '2021-07-30 19:29:46', 'Test Description');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `created_at`, `updated_at`) VALUES
(1, '5', '4qdgvLlaWh_about_image.png', '2021-01-07 07:56:37', '2021-01-07 07:56:37'),
(2, '46', 'JDpY8lf9Bs_about_image.png', '2021-01-07 07:58:05', '2021-01-07 07:58:05'),
(3, '5', 'XErvucXYaP_about_image.png', '2021-01-07 08:05:59', '2021-01-07 08:05:59'),
(4, '49', 'vwh6JKuw46_about_us.png', '2021-01-07 08:05:59', '2021-01-07 08:05:59'),
(5, '49', 'ruzZiafTWc_legal.png', '2021-01-07 08:05:59', '2021-01-07 08:05:59'),
(6, '5', 'SFhwd3s9QK_about_image.png', '2021-01-07 08:07:56', '2021-01-07 08:07:56'),
(7, '50', 'msRUt1erTL_about_us.png', '2021-01-07 08:07:56', '2021-01-07 08:07:56'),
(8, '5', 'zjEt74KzI8_chinese-in-america1.png', '2021-01-07 08:07:56', '2021-01-07 08:07:56'),
(9, '50', '5L9cTAAuKx_legal.png', '2021-01-07 08:07:56', '2021-01-07 08:07:56'),
(10, '51', 'gjEaj1UU2W_about_image.png', '2021-01-07 08:17:50', '2021-01-07 08:17:50'),
(11, '52', 'sIG5c07QPT_about_image.png', '2021-01-07 13:01:08', '2021-01-07 13:01:08'),
(12, '52', 'QkshwnYkbJ_legal.png', '2021-01-07 13:01:08', '2021-01-08 07:10:22'),
(14, '53', 'JYyI7SPHoW_about_image.png', '2021-01-08 09:39:45', '2021-01-08 09:39:45'),
(15, '54', 'rlk6z8PwuH_about_image.png', '2021-01-08 10:01:33', '2021-01-08 10:01:33'),
(16, '54', 'aiHEK8gmeY_about_us.png', '2021-01-08 10:01:33', '2021-01-08 10:01:33'),
(17, '54', 'WV3MngmBWF_legal.png', '2021-01-08 10:01:33', '2021-01-08 10:01:33'),
(18, '56', 'WcA49vKbxY_about_us.png', '2021-01-11 09:59:35', '2021-01-11 09:59:35'),
(19, '62', 'xUHgWQ3fcS_about_image.png', '2021-01-11 11:14:14', '2021-01-11 11:14:14'),
(20, '68', 'PYXbd5SPvM_about_image.png', '2021-01-11 11:25:19', '2021-01-11 11:25:19'),
(21, '69', 'bcXqOObIwH_about_image.png', '2021-01-11 11:31:53', '2021-01-11 11:31:53'),
(22, '70', 'I38ilVKbDS_about_image.png', '2021-01-11 11:36:16', '2021-01-11 11:36:16'),
(23, '71', 'I3TMgM6jes_about_image.png', '2021-01-11 11:46:53', '2021-01-11 11:46:53'),
(24, '75', 'xyf6T8Ilrh_about_image.png', '2021-01-11 12:36:01', '2021-01-11 12:36:01');

-- --------------------------------------------------------

--
-- Table structure for table `review_rating`
--

CREATE TABLE `review_rating` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review_message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `review_rating`
--

INSERT INTO `review_rating` (`id`, `user_id`, `product_id`, `review_message`, `rating`, `created_at`, `updated_at`) VALUES
(1, '15', '5', 'Thsis is testing message', '3', '2021-01-08 13:53:58', '2021-01-08 13:53:58'),
(2, '15', '5', 'Thsis is testing message', '3', '2021-01-08 13:56:34', '2021-01-08 13:56:34'),
(3, '15', '5', 'Thsis is testing message', '3', '2021-01-11 05:10:14', '2021-01-11 05:10:14'),
(4, '15', '5', 'Thsis is testing message', '3', '2021-01-11 05:55:38', '2021-01-11 05:55:38');

-- --------------------------------------------------------

--
-- Table structure for table `shop_collection`
--

CREATE TABLE `shop_collection` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shop_collection`
--

INSERT INTO `shop_collection` (`id`, `title`, `picture`, `created_at`, `updated_at`) VALUES
(3, 'Toys', '1605598816.jpeg', '2020-11-17 07:40:16', '2020-12-01 10:40:39'),
(5, 'Traditional', '1606819278.webp', '2020-12-01 10:41:18', '2020-12-01 10:41:18'),
(6, 'footwear', '77LhpZhUuK_about_image.png', '2021-01-08 06:26:00', '2021-01-08 10:34:39'),
(8, 'Apple', '34f705WcQG_legal.png', '2021-01-08 10:20:35', '2021-01-08 12:23:37'),
(9, 'Notification', 'UBlcET8dz4_about_us.png', '2021-01-11 11:00:21', '2021-01-11 11:00:21'),
(10, 'Notification', 'mb2SJy6zc9_about_us.png', '2021-01-11 11:02:03', '2021-01-11 11:02:03'),
(13, 'Notification', 'FFtd6LB4Nm_about_image.png', '2021-01-11 11:10:42', '2021-01-11 11:10:42'),
(16, 'Notification', 'OsfKcNXMwk_about_image.png', '2021-01-11 11:13:33', '2021-01-11 11:13:33'),
(17, 'Test 6:15', 'YWzz7GUtYg_about_image.png', '2021-01-11 12:46:04', '2021-01-11 12:46:04'),
(18, 'Test 6:15', 'UBBv8DnLNW_about_image.png', '2021-01-11 12:47:59', '2021-01-11 12:47:59'),
(19, 'Test 6:15', 'mJRaj8hgJ6_about_image.png', '2021-01-11 12:53:18', '2021-01-11 12:53:18'),
(21, 'Test 7pm', '99F9jQE2Gp_about_image.png', '2021-01-11 13:14:22', '2021-01-11 13:14:22'),
(22, 'Test 7pm', 'jCvLxrASqb_about_image.png', '2021-01-11 13:15:02', '2021-01-11 13:15:02'),
(23, 'Test', '3X4Dth9pGA_about_image.png', '2021-01-11 14:21:51', '2021-01-11 14:21:51'),
(24, 'Test', 'u1HZ15iJWX_about_image.png', '2021-01-11 14:27:06', '2021-01-11 14:27:06'),
(25, 'Test', 'Gqmo36re7K_about_image.png', '2021-01-11 14:27:33', '2021-01-11 14:27:33'),
(26, 'Liptim', '6PbtTloG84_dove-2516641__340.webp', '2021-04-01 11:25:11', '2021-04-01 11:25:11'),
(27, 'Test Collection', '8fowiOFrZP_USSD App.png', '2021-07-30 09:18:49', '2021-07-30 09:18:49');

-- --------------------------------------------------------

--
-- Table structure for table `tracking`
--

CREATE TABLE `tracking` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `baby_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datetime` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tracking`
--

INSERT INTO `tracking` (`id`, `user_id`, `baby_id`, `datetime`, `action`, `comment`, `created_at`, `updated_at`) VALUES
(4, '21', '16', '2020-12-26 11:00:00', 'meal', 'testing', '2020-12-26 05:00:00', '2020-12-01 08:00:49'),
(5, '21', '16', '2021-01-13 12:38:42', 'Meal', 'Test', '2020-12-26 06:00:00', '2021-01-13 12:38:42'),
(6, '21', '16', '2021-01-13 07:51:28', 'Meal', 'Pawan Testing', '2020-12-26 11:00:00', '2021-01-13 07:51:28'),
(13, '21', '16', '2020-12-26 03:00:00', 'meel', 'testing', '2021-01-11 13:46:13', '2021-01-11 13:46:13'),
(14, '22', '24', '2021-01-11 03:00:00', 'meel', 'testing', '2021-01-11 13:46:13', '2021-01-11 13:46:13'),
(15, '22', '24', '2021-01-11 05:00:00', 'meel', 'testing', '2021-01-11 13:46:13', '2021-01-11 13:46:13'),
(16, '15', '6', '2021-01-13 06:09:45', 'meal', 'Pawan Testing', '2021-01-13 06:09:45', '2021-01-13 06:09:45'),
(17, '15', '6', '2021-01-13 06:58:17', 'Meal', 'Pawan Testing', '2021-01-13 06:58:17', '2021-01-13 06:58:17'),
(18, '15', '6', '2021-02-04 05:29:01', 'Meal', 'gbffg', '2021-02-04 05:29:01', '2021-02-04 05:29:01'),
(19, '15', '66', '2021-02-04 05:45:38', 'Meal', 'gbffg', '2021-02-04 05:45:38', '2021-02-04 05:45:38'),
(20, '153', '66', '2021-02-05 10:39:26', 'Meal', 'hello', '2021-02-05 10:39:26', '2021-02-05 10:39:26'),
(21, '153', '65', '2021-02-05 10:57:44', 'Poop', 'xyz', '2021-02-05 10:57:44', '2021-02-05 10:57:44'),
(22, '153', '65', '2021-02-05 10:57:47', 'Poop', 'xyz', '2021-02-05 10:57:47', '2021-02-05 10:57:47'),
(23, '153', '65', '2021-02-05 10:57:49', 'Poop', 'xyz', '2021-02-05 10:57:49', '2021-02-05 10:57:49'),
(24, '153', '65', '2021-02-05 10:57:51', 'Poop', 'xyz', '2021-02-05 10:57:51', '2021-02-05 10:57:51'),
(25, '153', '65', '2021-02-05 10:58:02', 'Poop', 'xyz', '2021-02-05 10:58:02', '2021-02-05 10:58:02'),
(26, '66', '52', '2021-02-05 11:21:25', 'Poop', 'xyz', '2021-02-05 11:21:25', '2021-02-05 11:21:25'),
(27, '66', '52', '2021-02-05 12:07:15', 'Poop', 'xyz', '2021-02-05 12:07:15', '2021-02-05 12:07:15'),
(28, '161', '75', '2021-02-12 05:59:50', 'Poop', 'xyz', '2021-02-12 05:59:50', '2021-02-12 05:59:50'),
(29, '161', '76', '2021-02-12 06:00:04', 'Poop', 'xyz', '2021-02-12 06:00:04', '2021-02-12 06:00:04'),
(30, '95', '77', '2021-02-12 06:24:08', 'Poop', 'xyz', '2021-02-12 06:24:08', '2021-02-12 06:24:08'),
(31, '95', '77', '2021-02-12 06:24:12', 'Poop', 'xyz', '2021-02-12 06:24:12', '2021-02-12 06:24:12'),
(32, '95', '77', '2021-02-12 06:24:15', 'Poop', 'xyz', '2021-02-12 06:24:15', '2021-02-12 06:24:15'),
(33, '95', '77', '2021-02-12 06:32:44', 'Poop', 'xyz', '2021-02-12 06:32:44', '2021-02-12 06:32:44'),
(34, '95', '77', '2021-02-12 07:23:15', 'Poop', 'xyz', '2021-02-12 07:23:15', '2021-02-12 07:23:15'),
(35, '95', '77', '2021-02-12 07:23:30', 'Poop', 'xyz', '2021-02-12 07:23:30', '2021-02-12 07:23:30'),
(36, '162', '78', '2021-02-12 15:44:13', 'Poop', 'xyz', '2021-02-12 15:44:13', '2021-02-12 15:44:13'),
(37, '162', '78', '2021-02-12 15:44:18', 'Peepee', 'xyz', '2021-02-12 15:44:18', '2021-02-12 15:44:18'),
(38, '162', '78', '2021-02-12 15:44:38', 'Poop', 'xyz', '2021-02-12 15:44:38', '2021-02-12 15:44:38'),
(39, '162', '78', '2021-02-12 15:49:19', 'Poop', 'xyz', '2021-02-12 15:49:19', '2021-02-12 15:49:19'),
(40, '162', '78', '2021-02-12 15:49:21', 'Poop', 'xyz', '2021-02-12 15:49:21', '2021-02-12 15:49:21'),
(41, '162', '78', '2021-02-12 15:49:23', 'Poop', 'xyz', '2021-02-12 15:49:23', '2021-02-12 15:49:23'),
(42, '66', '49', '2021-02-19 13:16:10', 'Peepee', 'xyz', '2021-02-19 13:16:10', '2021-02-19 13:16:10'),
(43, '139', 'null', '2021-02-20 05:03:21', 'Poop', 'xyz', '2021-02-20 05:03:21', '2021-02-20 05:03:21'),
(44, '66', '49', '2021-02-20 05:22:05', 'Poop', 'xyz', '2021-02-20 05:22:05', '2021-02-20 05:22:05'),
(45, '66', '49', '2021-02-20 05:22:14', 'Poop', 'xyz', '2021-02-20 05:22:14', '2021-02-20 05:22:14'),
(46, '66', '49', '2021-02-20 05:22:18', 'Poop', 'xyz', '2021-02-20 05:22:18', '2021-02-20 05:22:18'),
(47, '66', '56', '2021-02-20 05:46:05', 'Poop', 'xyz', '2021-02-20 05:46:05', '2021-02-20 05:46:05'),
(48, '66', '51', '2021-02-20 05:47:22', 'Peepee', 'xyz', '2021-02-20 05:47:22', '2021-02-20 05:47:22'),
(49, '66', '49', '2021-02-20 10:44:07', 'Poop', 'xyz', '2021-02-20 10:44:07', '2021-02-20 10:44:07'),
(50, '66', '49', '2021-02-20 10:44:09', 'Peepee', 'xyz', '2021-02-20 10:44:09', '2021-02-20 10:44:09'),
(51, '66', '49', '2021-02-20 10:44:11', 'Poop', 'xyz', '2021-02-20 10:44:11', '2021-02-20 10:44:11'),
(52, '66', '49', '2021-02-20 10:44:14', 'Poop', 'xyz', '2021-02-20 10:44:14', '2021-02-20 10:44:14'),
(53, '66', '49', '2021-02-20 10:44:21', 'Poop', 'xyz', '2021-02-20 10:44:21', '2021-02-20 10:44:21'),
(54, '66', '49', '2021-02-20 10:44:49', 'Peepee', 'xyz', '2021-02-20 10:44:49', '2021-02-20 10:44:49'),
(55, '66', '49', '2021-02-20 10:44:53', 'Poop', 'xyz', '2021-02-20 10:44:53', '2021-02-20 10:44:53'),
(56, '66', '49', '2021-02-20 10:44:55', 'Poop', 'xyz', '2021-02-20 10:44:55', '2021-02-20 10:44:55'),
(57, '66', '56', '2021-02-20 11:05:30', 'Poop', 'xyz', '2021-02-20 11:05:30', '2021-02-20 11:05:30'),
(58, '66', '56', '2021-02-20 11:05:32', 'Poop', 'xyz', '2021-02-20 11:05:32', '2021-02-20 11:05:32'),
(59, '66', '56', '2021-02-20 11:05:34', 'Peepee', 'xyz', '2021-02-20 11:05:34', '2021-02-20 11:05:34'),
(60, '66', '56', '2021-02-20 11:05:37', 'Poop', 'xyz', '2021-02-20 11:05:37', '2021-02-20 11:05:37'),
(61, '66', '56', '2021-02-20 11:05:40', 'Poop', 'xyz', '2021-02-20 11:05:40', '2021-02-20 11:05:40'),
(62, '66', '56', '2021-02-20 11:05:44', 'Poop', 'xyz', '2021-02-20 11:05:44', '2021-02-20 11:05:44'),
(63, '154', '67', '2021-02-22 17:55:08', 'Poop', 'xyz', '2021-02-22 17:55:08', '2021-02-22 17:55:08'),
(64, '154', '67', '2021-02-22 17:55:10', 'Poop', 'xyz', '2021-02-22 17:55:10', '2021-02-22 17:55:10'),
(65, '154', '67', '2021-02-22 17:55:11', 'Poop', 'xyz', '2021-02-22 17:55:11', '2021-02-22 17:55:11'),
(66, '146', 'null', '2021-02-26 09:54:49', 'Poop', 'xyz', '2021-02-26 09:54:49', '2021-02-26 09:54:49'),
(67, '154', '67', '2021-02-26 17:09:16', 'Poop', 'xyz', '2021-02-26 17:09:16', '2021-02-26 17:09:16'),
(68, '154', '67', '2021-02-26 17:09:17', 'Peepee', 'xyz', '2021-02-26 17:09:17', '2021-02-26 17:09:17'),
(69, '154', '86', '2021-02-26 17:09:22', 'Peepee', 'xyz', '2021-02-26 17:09:22', '2021-02-26 17:09:22'),
(70, '154', '86', '2021-02-26 17:09:24', 'Poop', 'xyz', '2021-02-26 17:09:24', '2021-02-26 17:09:24'),
(71, '154', '67', '2021-03-07 15:31:17', 'Poop', 'xyz', '2021-03-07 15:31:17', '2021-03-07 15:31:17'),
(72, '154', '67', '2021-03-07 15:31:19', 'Poop', 'xyz', '2021-03-07 15:31:19', '2021-03-07 15:31:19'),
(73, '67', '80', '2021-03-16 10:17:48', 'Peepee', 'xyz', '2021-03-16 10:17:48', '2021-03-16 10:17:48'),
(74, '67', '80', '2021-03-16 10:25:18', 'Peepee', 'xyz', '2021-03-16 10:25:18', '2021-03-16 10:25:18'),
(75, '67', '80', '2021-03-16 10:25:29', 'Nap', 'xyz', '2021-03-16 10:25:29', '2021-03-16 10:25:29'),
(76, '135', '92', '2021-03-16 11:09:13', 'Meal', 'xyz', '2021-03-16 11:09:13', '2021-03-16 11:09:13'),
(77, '135', '92', '2021-03-16 11:09:20', 'Both', 'xyz', '2021-03-16 11:09:20', '2021-03-16 11:09:20'),
(78, '135', '92', '2021-03-16 11:09:49', 'Peepee', 'xyz', '2021-03-16 11:09:49', '2021-03-16 11:09:49'),
(79, '135', '92', '2021-03-16 12:31:29', 'Peepee', 'xyz', '2021-03-16 12:31:29', '2021-03-16 12:31:29'),
(80, '135', '93', '2021-03-16 12:31:44', 'Poop', 'xyz', '2021-03-16 12:31:44', '2021-03-16 12:31:44'),
(81, '135', '92', '2021-03-16 12:32:03', 'Meal', 'xyz', '2021-03-16 12:32:03', '2021-03-16 12:32:03'),
(82, '137', '54', '2021-03-17 07:14:51', 'Drink', 'xyz', '2021-03-17 07:14:51', '2021-03-17 07:14:51'),
(83, '137', '54', '2021-03-17 07:14:56', 'Nap', 'xyz', '2021-03-17 07:14:56', '2021-03-17 07:14:56'),
(84, '137', '94', '2021-03-17 07:37:15', 'Poop', 'xyz', '2021-03-17 07:37:15', '2021-03-17 07:37:15'),
(85, '137', '94', '2021-03-17 07:37:21', 'Peepee', 'xyz', '2021-03-17 07:37:21', '2021-03-17 07:37:21'),
(86, '137', '94', '2021-03-17 07:37:25', 'Meal', 'xyz', '2021-03-17 07:37:25', '2021-03-17 07:37:25'),
(87, '137', '54', '2021-03-17 07:39:47', 'Peepee', 'xyz', '2021-03-17 07:39:47', '2021-03-17 07:39:47'),
(88, '137', '54', '2021-03-17 07:39:53', 'Meal', 'xyz', '2021-03-17 07:39:53', '2021-03-17 07:39:53'),
(89, '137', '54', '2021-03-17 07:44:16', 'Drink', 'xyz', '2021-03-17 07:44:16', '2021-03-17 07:44:16'),
(90, '137', '54', '2021-03-17 07:44:24', 'Meal', 'xyz', '2021-03-17 07:44:24', '2021-03-17 07:44:24'),
(91, '164', '95', '2021-03-18 10:34:09', 'Drink', 'xyz', '2021-03-18 10:34:09', '2021-03-18 10:34:09'),
(92, '164', '95', '2021-03-18 10:34:14', 'Meal', 'xyz', '2021-03-18 10:34:14', '2021-03-18 10:34:14'),
(93, '164', '95', '2021-03-18 10:34:19', 'Nap', 'xyz', '2021-03-18 10:34:19', '2021-03-18 10:34:19'),
(94, '164', '96', '2021-03-18 10:36:33', 'Meal', 'xyz', '2021-03-18 10:36:33', '2021-03-18 10:36:33'),
(95, '164', '95', '2021-03-18 10:39:52', 'Drink', 'xyz', '2021-03-18 10:39:52', '2021-03-18 10:39:52'),
(96, '164', '96', '2021-03-18 10:40:07', 'Both', 'xyz', '2021-03-18 10:40:07', '2021-03-18 10:40:07'),
(97, '164', '95', '2021-03-18 10:45:05', 'Drink', 'xyz', '2021-03-18 10:45:05', '2021-03-18 10:45:05'),
(98, '164', '95', '2021-03-18 10:45:16', 'Nap', 'xyz', '2021-03-18 10:45:16', '2021-03-18 10:45:16'),
(99, '164', '96', '2021-03-18 11:12:32', 'Meal', 'xyz', '2021-03-18 11:12:32', '2021-03-18 11:12:32'),
(100, '164', '95', '2021-03-18 11:12:38', 'Meal', 'xyz', '2021-03-18 11:12:38', '2021-03-18 11:12:38'),
(101, '164', '96', '2021-03-18 11:12:42', 'Drink', 'xyz', '2021-03-18 11:12:42', '2021-03-18 11:12:42'),
(102, '164', '96', '2021-03-18 11:12:47', 'Drink', 'xyz', '2021-03-18 11:12:47', '2021-03-18 11:12:47'),
(103, '164', '95', '2021-03-18 11:12:52', 'Both', 'xyz', '2021-03-18 11:12:52', '2021-03-18 11:12:52'),
(104, '164', '95', '2021-03-18 11:45:48', 'Meal', 'xyz', '2021-03-18 11:45:48', '2021-03-18 11:45:48'),
(105, '164', '95', '2021-03-18 12:05:43', 'Peepee', 'xyz', '2021-03-18 12:05:43', '2021-03-18 12:05:43'),
(106, '164', '95', '2021-03-18 12:07:24', 'Nap', 'xyz', '2021-03-18 12:07:24', '2021-03-18 12:07:24'),
(107, '164', '95', '2021-03-18 12:27:03', 'Drink', 'xyz', '2021-03-18 12:27:03', '2021-03-18 12:27:03'),
(108, '164', '95', '2021-03-18 12:27:11', 'Meal', 'xyz', '2021-03-18 12:27:11', '2021-03-18 12:27:11'),
(109, '164', '95', '2021-03-18 12:27:17', 'Drink', 'xyz', '2021-03-18 12:27:17', '2021-03-18 12:27:17'),
(110, '164', '95', '2021-03-18 12:28:19', 'Drink', 'xyz', '2021-03-18 12:28:19', '2021-03-18 12:28:19'),
(111, '164', '95', '2021-03-18 12:29:12', 'Meal', 'xyz', '2021-03-18 12:29:12', '2021-03-18 12:29:12'),
(112, '164', '95', '2021-03-18 12:29:23', 'Nap', 'xyz', '2021-03-18 12:29:23', '2021-03-18 12:29:23'),
(113, '164', '95', '2021-03-18 12:46:19', 'Drink', 'xyz', '2021-03-18 12:46:19', '2021-03-18 12:46:19'),
(114, '164', '95', '2021-03-18 12:46:23', 'Meal', 'xyz', '2021-03-18 12:46:23', '2021-03-18 12:46:23'),
(115, '164', '95', '2021-03-18 12:49:18', 'Nap', 'xyz', '2021-03-18 12:49:18', '2021-03-18 12:49:18'),
(116, '164', '95', '2021-03-18 12:54:20', 'Nap', 'xyz', '2021-03-18 12:54:20', '2021-03-18 12:54:20'),
(117, '164', '95', '2021-03-19 04:26:35', 'Drink', 'xyz', '2021-03-19 04:26:35', '2021-03-19 04:26:35'),
(118, '164', '96', '2021-03-19 04:26:40', 'Meal', 'xyz', '2021-03-19 04:26:40', '2021-03-19 04:26:40'),
(119, '164', '96', '2021-03-19 04:26:43', 'Nap', 'xyz', '2021-03-19 04:26:43', '2021-03-19 04:26:43'),
(120, '164', '95', '2021-03-19 04:26:47', 'Meal', 'xyz', '2021-03-19 04:26:47', '2021-03-19 04:26:47'),
(121, '164', '95', '2021-03-19 04:30:01', 'Drink', 'xyz', '2021-03-19 04:30:01', '2021-03-19 04:30:01'),
(122, '164', '95', '2021-03-19 06:00:53', 'Drink', 'xyz', '2021-03-19 06:00:53', '2021-03-19 06:00:53'),
(123, '164', '95', '2021-03-19 06:01:03', 'Nap', 'xyz', '2021-03-19 06:01:03', '2021-03-19 06:01:03'),
(124, '164', '95', '2021-03-19 06:01:17', 'Drink', 'xyz', '2021-03-19 06:01:17', '2021-03-19 06:01:17'),
(125, '164', '95', '2021-03-19 06:02:22', 'Drink', 'xyz', '2021-03-19 06:02:22', '2021-03-19 06:02:22'),
(126, '164', '95', '2021-03-19 06:20:10', 'Nap', 'xyz', '2021-03-19 06:20:10', '2021-03-19 06:20:10'),
(127, '164', '95', '2021-03-19 06:56:45', 'Both', 'xyz', '2021-03-19 06:56:45', '2021-03-19 06:56:45'),
(128, '164', '95', '2021-03-19 06:57:09', 'Poop', 'xyz', '2021-03-19 06:57:09', '2021-03-19 06:57:09'),
(129, '164', '95', '2021-03-19 06:57:24', 'Peepee', 'xyz', '2021-03-19 06:57:24', '2021-03-19 06:57:24'),
(130, '164', 'null', '2021-03-19 10:23:34', 'Drink', 'xyz', '2021-03-19 10:23:34', '2021-03-19 10:23:34'),
(131, '164', 'null', '2021-03-19 10:23:38', 'Peepee', 'xyz', '2021-03-19 10:23:38', '2021-03-19 10:23:38'),
(132, '167', 'null', '2021-03-19 10:30:10', 'Drink', 'xyz', '2021-03-19 10:30:10', '2021-03-19 10:30:10'),
(133, '167', '97', '2021-03-19 10:32:00', 'Drink', 'xyz', '2021-03-19 10:32:00', '2021-03-19 10:32:00'),
(134, '167', 'null', '2021-03-19 10:42:18', 'Nap', 'xyz', '2021-03-19 10:42:18', '2021-03-19 10:42:18'),
(135, '166', 'null', '2021-03-19 13:06:33', 'Nap', 'xyz', '2021-03-19 13:06:33', '2021-03-19 13:06:33'),
(136, '164', 'null', '2021-03-22 08:14:44', 'Drink', 'xyz', '2021-03-22 08:14:44', '2021-03-22 08:14:44'),
(137, '164', 'null', '2021-03-22 08:14:54', 'Both', 'xyz', '2021-03-22 08:14:54', '2021-03-22 08:14:54'),
(138, '164', '95', '2021-03-22 08:19:56', 'Meal', 'xyz', '2021-03-22 08:19:56', '2021-03-22 08:19:56'),
(139, '147', 'null', '2021-03-22 09:43:40', 'Peepee', 'xyz', '2021-03-22 09:43:40', '2021-03-22 09:43:40'),
(140, '147', 'null', '2021-03-22 10:18:21', 'Peepee', 'xyz', '2021-03-22 10:18:21', '2021-03-22 10:18:21'),
(141, '164', '95', '2021-03-23 06:19:29', 'Meal', 'xyz', '2021-03-23 06:19:29', '2021-03-23 06:19:29'),
(142, '164', '95', '2021-03-23 06:34:22', 'Meal', 'xyz', '2021-03-23 06:34:22', '2021-03-23 06:34:22'),
(143, '164', '96', '2021-03-23 06:34:37', 'Both', 'xyz', '2021-03-23 06:34:37', '2021-03-23 06:34:37'),
(144, '164', '95', '2021-03-23 06:53:01', 'Drink', 'xyz', '2021-03-23 06:53:01', '2021-03-23 06:53:01'),
(145, '164', '95', '2021-03-23 07:23:06', 'Drink', 'xyz', '2021-03-23 07:23:06', '2021-03-23 07:23:06'),
(146, '164', '96', '2021-03-23 07:23:17', 'Both', 'xyz', '2021-03-23 07:23:17', '2021-03-23 07:23:17'),
(147, '164', '95', '2021-03-23 12:03:26', 'Drink', 'xyz', '2021-03-23 12:03:26', '2021-03-23 12:03:26'),
(148, '164', '95', '2021-03-23 12:15:21', 'Drink', 'xyz', '2021-03-23 12:15:21', '2021-03-23 12:15:21'),
(149, '164', '95', '2021-03-23 12:27:26', 'Poop', 'xyz', '2021-03-23 12:27:26', '2021-03-23 12:27:26'),
(150, '164', '95', '2021-03-23 12:27:43', 'Poop', 'xyz', '2021-03-23 12:27:43', '2021-03-23 12:27:43'),
(151, '164', '96', '2021-03-25 04:37:54', 'Meal', 'xyz', '2021-03-25 04:37:54', '2021-03-25 04:37:54'),
(152, '164', '95', '2021-03-25 04:39:42', 'Drink', 'xyz', '2021-03-25 04:39:42', '2021-03-25 04:39:42'),
(153, '164', '95', '2021-03-25 04:41:32', 'Both', 'xyz', '2021-03-25 04:41:32', '2021-03-25 04:41:32'),
(154, '164', '96', '2021-03-25 04:41:40', 'Nap', 'xyz', '2021-03-25 04:41:40', '2021-03-25 04:41:40'),
(155, '164', '95', '2021-03-25 04:41:49', 'Meal', 'xyz', '2021-03-25 04:41:49', '2021-03-25 04:41:49'),
(156, '164', '96', '2021-03-25 04:41:55', 'Poop', 'xyz', '2021-03-25 04:41:55', '2021-03-25 04:41:55'),
(157, '164', '95', '2021-03-25 04:42:01', 'Peepee', 'xyz', '2021-03-25 04:42:01', '2021-03-25 04:42:01'),
(158, '164', '96', '2021-03-25 04:42:07', 'Nap', 'xyz', '2021-03-25 04:42:07', '2021-03-25 04:42:07'),
(159, '164', '96', '2021-03-25 04:42:18', 'Drink', 'xyz', '2021-03-25 04:42:18', '2021-03-25 04:42:18'),
(160, '164', '95', '2021-03-25 04:42:29', 'Peepee', 'xyz', '2021-03-25 04:42:29', '2021-03-25 04:42:29'),
(161, '164', '95', '2021-03-25 04:45:53', 'Drink', 'xyz', '2021-03-25 04:45:53', '2021-03-25 04:45:53'),
(162, '164', '96', '2021-03-25 04:46:06', 'Poop', 'xyz', '2021-03-25 04:46:06', '2021-03-25 04:46:06'),
(163, '168', '98', '2021-03-25 10:09:26', 'Peepee', 'xyz', '2021-03-25 10:09:26', '2021-03-25 10:09:26'),
(164, '168', '98', '2021-03-25 10:09:28', 'Drink', 'xyz', '2021-03-25 10:09:28', '2021-03-25 10:09:28'),
(165, '168', '98', '2021-03-25 10:09:31', 'Meal', 'xyz', '2021-03-25 10:09:31', '2021-03-25 10:09:31'),
(166, '168', '98', '2021-03-25 10:09:33', 'Poop', 'xyz', '2021-03-25 10:09:33', '2021-03-25 10:09:33'),
(167, '168', '98', '2021-03-25 10:09:36', 'Nap', 'xyz', '2021-03-25 10:09:36', '2021-03-25 10:09:36'),
(168, '168', '98', '2021-03-25 10:09:38', 'Both', 'xyz', '2021-03-25 10:09:38', '2021-03-25 10:09:38'),
(169, '168', '98', '2021-03-25 10:11:29', 'Drink', 'xyz', '2021-03-25 10:11:29', '2021-03-25 10:11:29'),
(170, '168', '98', '2021-03-25 10:11:47', 'Meal', 'xyz', '2021-03-25 10:11:47', '2021-03-25 10:11:47'),
(171, '168', '98', '2021-03-25 10:34:08', 'Meal', 'xyz', '2021-03-25 10:34:08', '2021-03-25 10:34:08'),
(172, '168', '98', '2021-03-25 10:34:29', 'Poop', 'xyz', '2021-03-25 10:34:29', '2021-03-25 10:34:29'),
(173, '168', '98', '2021-03-25 10:34:33', 'Peepee', 'xyz', '2021-03-25 10:34:33', '2021-03-25 10:34:33'),
(174, '170', '101', '2021-03-26 06:03:49', 'Drink', 'xyz', '2021-03-26 06:03:49', '2021-03-26 06:03:49'),
(175, '170', '101', '2021-03-26 06:03:52', 'Drink', 'xyz', '2021-03-26 06:03:52', '2021-03-26 06:03:52'),
(176, '170', '101', '2021-03-26 06:03:59', 'Drink', 'xyz', '2021-03-26 06:03:59', '2021-03-26 06:03:59'),
(177, '170', '102', '2021-03-26 06:05:49', 'Meal', 'xyz', '2021-03-26 06:05:49', '2021-03-26 06:05:49'),
(178, '170', '101', '2021-03-26 06:06:00', 'Peepee', 'xyz', '2021-03-26 06:06:00', '2021-03-26 06:06:00'),
(179, '170', '101', '2021-03-26 06:06:03', 'Nap', 'xyz', '2021-03-26 06:06:03', '2021-03-26 06:06:03'),
(180, '170', '101', '2021-03-26 06:06:06', 'Both', 'xyz', '2021-03-26 06:06:06', '2021-03-26 06:06:06'),
(181, '170', '102', '2021-03-26 06:06:13', 'Drink', 'xyz', '2021-03-26 06:06:13', '2021-03-26 06:06:13'),
(182, '171', '103', '2021-03-30 10:21:09', 'Drink', 'xyz', '2021-03-30 10:21:09', '2021-03-30 10:21:09'),
(183, '171', '103', '2021-03-30 10:28:16', 'Meal', 'xyz', '2021-03-30 10:28:16', '2021-03-30 10:28:16'),
(184, '171', '103', '2021-03-30 12:26:00', 'Both', 'xyz', '2021-03-30 12:26:00', '2021-03-30 12:26:00'),
(185, '171', '103', '2021-03-31 04:40:24', 'Meal', 'xyz', '2021-03-31 04:40:24', '2021-03-31 04:40:24'),
(186, '171', '103', '2021-03-31 10:22:12', 'Drink', 'xyz', '2021-03-31 10:22:12', '2021-03-31 10:22:12'),
(187, '178', '104', '2021-03-31 11:02:40', 'Drink', 'xyz', '2021-03-31 11:02:40', '2021-03-31 11:02:40'),
(188, '178', '104', '2021-03-31 11:02:43', 'Meal', 'xyz', '2021-03-31 11:02:43', '2021-03-31 11:02:43'),
(189, '178', '104', '2021-03-31 11:14:50', 'Meal', 'xyz', '2021-03-31 11:14:50', '2021-03-31 11:14:50'),
(190, '154', '67', '2021-03-31 19:03:18', 'Drink', 'xyz', '2021-03-31 19:03:18', '2021-03-31 19:03:18'),
(191, '154', '67', '2021-03-31 19:03:19', 'Meal', 'xyz', '2021-03-31 19:03:19', '2021-03-31 19:03:19'),
(192, '154', '67', '2021-03-31 19:03:22', 'Peepee', 'xyz', '2021-03-31 19:03:22', '2021-03-31 19:03:22'),
(193, '154', '67', '2021-03-31 19:03:23', 'Peepee', 'xyz', '2021-03-31 19:03:23', '2021-03-31 19:03:23'),
(194, '154', '67', '2021-03-31 19:03:24', 'Poop', 'xyz', '2021-03-31 19:03:24', '2021-03-31 19:03:24'),
(195, '154', '67', '2021-03-31 19:03:25', 'Both', 'xyz', '2021-03-31 19:03:25', '2021-03-31 19:03:25'),
(196, '154', '67', '2021-04-05 11:19:07', 'Drink', 'xyz', '2021-04-05 11:19:07', '2021-04-05 11:19:07'),
(197, '154', '67', '2021-04-05 11:19:08', 'Peepee', 'xyz', '2021-04-05 11:19:08', '2021-04-05 11:19:08'),
(198, '154', '67', '2021-04-05 11:19:10', 'Poop', 'xyz', '2021-04-05 11:19:10', '2021-04-05 11:19:10'),
(199, '154', '67', '2021-04-05 11:19:10', 'Both', 'xyz', '2021-04-05 11:19:10', '2021-04-05 11:19:10'),
(200, '154', '67', '2021-04-05 11:19:11', 'Nap', 'xyz', '2021-04-05 11:19:11', '2021-04-05 11:19:11'),
(201, '179', '107', '2021-04-05 12:08:34', 'Peepee', 'xyz', '2021-04-05 12:08:34', '2021-04-05 12:08:34'),
(202, '180', '109', '2021-04-06 07:18:59', 'Peepee', 'xyz', '2021-04-06 07:18:59', '2021-04-06 07:18:59'),
(203, '180', '109', '2021-04-06 07:19:02', 'Meal', 'xyz', '2021-04-06 07:19:02', '2021-04-06 07:19:02'),
(204, '180', '109', '2021-04-06 07:20:11', 'Drink', 'xyz', '2021-04-06 07:20:11', '2021-04-06 07:20:11'),
(205, '180', '109', '2021-04-06 07:20:15', 'Peepee', 'xyz', '2021-04-06 07:20:15', '2021-04-06 07:20:15'),
(206, '180', '109', '2021-04-06 07:20:17', 'Meal', 'xyz', '2021-04-06 07:20:17', '2021-04-06 07:20:17'),
(207, '180', '109', '2021-04-06 07:20:18', 'Poop', 'xyz', '2021-04-06 07:20:18', '2021-04-06 07:20:18'),
(208, '180', '109', '2021-04-06 07:20:23', 'Nap', 'xyz', '2021-04-06 07:20:23', '2021-04-06 07:20:23'),
(209, '180', '109', '2021-04-06 07:20:25', 'Both', 'xyz', '2021-04-06 07:20:25', '2021-04-06 07:20:25'),
(210, '179', '107', '2021-04-06 09:25:53', 'Peepee', 'xyz', '2021-04-06 09:25:53', '2021-04-06 09:25:53'),
(211, '154', '67', '2021-04-06 09:39:14', 'Peepee', 'xyz', '2021-04-06 09:39:14', '2021-04-06 09:39:14'),
(212, '154', '67', '2021-04-06 09:39:16', 'Poop', 'xyz', '2021-04-06 09:39:16', '2021-04-06 09:39:16'),
(213, '154', '67', '2021-04-06 09:39:17', 'Both', 'xyz', '2021-04-06 09:39:17', '2021-04-06 09:39:17'),
(214, '154', '67', '2021-04-06 09:39:18', 'Peepee', 'xyz', '2021-04-06 09:39:18', '2021-04-06 09:39:18'),
(215, '154', '67', '2021-04-06 09:39:19', 'Poop', 'xyz', '2021-04-06 09:39:19', '2021-04-06 09:39:19'),
(216, '154', '67', '2021-04-06 09:39:20', 'Drink', 'xyz', '2021-04-06 09:39:20', '2021-04-06 09:39:20'),
(217, '154', '67', '2021-04-06 09:39:21', 'Meal', 'xyz', '2021-04-06 09:39:21', '2021-04-06 09:39:21'),
(218, '154', '67', '2021-04-06 09:39:31', 'Poop', 'xyz', '2021-04-06 09:39:31', '2021-04-06 09:39:31'),
(219, '154', '67', '2021-04-06 13:05:56', 'Peepee', 'xyz', '2021-04-06 13:05:56', '2021-04-06 13:05:56'),
(220, '154', '67', '2021-04-06 13:05:57', 'Poop', 'xyz', '2021-04-06 13:05:57', '2021-04-06 13:05:57'),
(221, '154', '67', '2021-04-06 13:05:58', 'Both', 'xyz', '2021-04-06 13:05:58', '2021-04-06 13:05:58'),
(222, '181', '113', '2021-04-07 04:44:51', 'Drink', 'xyz', '2021-04-07 04:44:51', '2021-04-07 04:44:51'),
(223, '181', '113', '2021-04-07 04:45:01', 'Peepee', 'xyz', '2021-04-07 04:45:01', '2021-04-07 04:45:01'),
(224, '183', '121', '2021-04-07 10:11:44', 'Drink', 'xyz', '2021-04-07 10:11:44', '2021-04-07 10:11:44'),
(225, '154', '67', '2021-04-08 23:24:11', 'Both', 'xyz', '2021-04-08 23:24:11', '2021-04-08 23:24:11'),
(226, '154', '67', '2021-04-08 23:24:12', 'Both', 'xyz', '2021-04-08 23:24:12', '2021-04-08 23:24:12'),
(227, '154', '67', '2021-04-08 23:24:13', 'Poop', 'xyz', '2021-04-08 23:24:13', '2021-04-08 23:24:13'),
(228, '154', '67', '2021-04-08 23:24:14', 'Poop', 'xyz', '2021-04-08 23:24:14', '2021-04-08 23:24:14'),
(229, '154', '67', '2021-04-12 09:45:59', 'Drink', 'xyz', '2021-04-12 09:45:59', '2021-04-12 09:45:59'),
(230, '154', '67', '2021-04-12 09:46:01', 'Meal', 'xyz', '2021-04-12 09:46:01', '2021-04-12 09:46:01'),
(231, '154', '67', '2021-04-12 09:46:02', 'Drink', 'xyz', '2021-04-12 09:46:02', '2021-04-12 09:46:02'),
(232, '154', '67', '2021-04-12 09:46:03', 'Nap', 'xyz', '2021-04-12 09:46:03', '2021-04-12 09:46:03'),
(233, '154', '67', '2021-04-12 09:46:08', 'Peepee', 'xyz', '2021-04-12 09:46:08', '2021-04-12 09:46:08'),
(234, '187', '128', '2021-04-15 05:50:22', 'Drink', 'xyz', '2021-04-15 05:50:22', '2021-04-15 05:50:22'),
(235, '187', '128', '2021-04-15 05:50:27', 'Poop', 'xyz', '2021-04-15 05:50:27', '2021-04-15 05:50:27'),
(236, '187', '128', '2021-04-15 05:50:30', 'Peepee', 'xyz', '2021-04-15 05:50:30', '2021-04-15 05:50:30'),
(237, '187', '128', '2021-04-15 06:11:59', 'Peepee', 'xyz', '2021-04-15 06:11:59', '2021-04-15 06:11:59'),
(238, '187', '128', '2021-04-15 06:12:04', 'Nap', 'xyz', '2021-04-15 06:12:04', '2021-04-15 06:12:04'),
(239, '187', '128', '2021-04-15 06:12:06', 'Meal', 'xyz', '2021-04-15 06:12:06', '2021-04-15 06:12:06'),
(240, '187', '128', '2021-04-15 06:31:54', 'Peepee', 'xyz', '2021-04-15 06:31:54', '2021-04-15 06:31:54'),
(241, '188', '130', '2021-04-15 11:06:39', 'Drink', 'xyz', '2021-04-15 11:06:39', '2021-04-15 11:06:39'),
(242, '188', '130', '2021-04-15 11:06:42', 'Poop', 'xyz', '2021-04-15 11:06:42', '2021-04-15 11:06:42'),
(243, '192', '135', '2021-04-16 09:44:50', 'Drink', 'xyz', '2021-04-16 09:44:50', '2021-04-16 09:44:50'),
(244, '192', '135', '2021-04-16 09:44:54', 'Poop', 'xyz', '2021-04-16 09:44:54', '2021-04-16 09:44:54'),
(245, '154', '67', '2021-04-19 17:32:21', 'Meal', 'xyz', '2021-04-19 17:32:21', '2021-04-19 17:32:21'),
(246, '154', '67', '2021-05-17 12:19:32', 'Peepee', 'xyz', '2021-05-17 12:19:32', '2021-05-17 12:19:32'),
(247, '154', '67', '2021-05-17 12:19:33', 'Peepee', 'xyz', '2021-05-17 12:19:33', '2021-05-17 12:19:33'),
(248, '154', '67', '2021-05-17 12:19:33', 'Poop', 'xyz', '2021-05-17 12:19:33', '2021-05-17 12:19:33'),
(249, '154', '67', '2021-05-17 12:19:34', 'Poop', 'xyz', '2021-05-17 12:19:34', '2021-05-17 12:19:34'),
(250, '154', '67', '2021-05-17 12:19:35', 'Both', 'xyz', '2021-05-17 12:19:35', '2021-05-17 12:19:35'),
(251, '200', '147', '2021-06-04 09:55:26', 'Drink', 'xyz', '2021-06-04 09:55:26', '2021-06-04 09:55:26'),
(252, '200', '147', '2021-06-04 09:55:29', 'Peepee', 'xyz', '2021-06-04 09:55:29', '2021-06-04 09:55:29'),
(253, '200', '147', '2021-06-04 09:55:50', 'Poop', 'xyz', '2021-06-04 09:55:50', '2021-06-04 09:55:50'),
(254, '154', '155', '2021-06-14 21:52:31', 'Drink', 'xyz', '2021-06-14 21:52:31', '2021-06-14 21:52:31'),
(255, '154', '155', '2021-06-14 21:52:33', 'Drink', 'xyz', '2021-06-14 21:52:33', '2021-06-14 21:52:33'),
(256, '154', '155', '2021-06-14 21:52:33', 'Peepee', 'xyz', '2021-06-14 21:52:33', '2021-06-14 21:52:33'),
(257, '154', '155', '2021-06-14 21:52:37', 'Peepee', 'xyz', '2021-06-14 21:52:37', '2021-06-14 21:52:37'),
(258, '198', '143', '2021-07-14 05:45:37', 'Peepee', 'xyz', '2021-07-14 05:45:37', '2021-07-14 05:45:37'),
(259, '198', '143', '2021-07-14 05:52:28', 'Drink', 'xyz', '2021-07-14 05:52:28', '2021-07-14 05:52:28'),
(260, '198', '143', '2021-07-14 05:52:51', 'Peepee', 'xyz', '2021-07-14 05:52:51', '2021-07-14 05:52:51'),
(261, '198', '143', '2021-07-14 06:16:50', 'Nap', 'xyz', '2021-07-14 06:16:50', '2021-07-14 06:16:50'),
(262, '198', '143', '2021-07-14 06:47:28', 'Peepee', 'xyz', '2021-07-14 06:47:28', '2021-07-14 06:47:28'),
(263, '198', '143', '2021-07-14 06:48:18', 'Poop', 'xyz', '2021-07-14 06:48:18', '2021-07-14 06:48:18'),
(264, '198', '143', '2021-07-14 06:52:36', 'Drink', 'xyz', '2021-07-14 06:52:37', '2021-07-14 06:52:37'),
(265, '198', '143', '2021-07-14 08:21:33', 'Drink', 'xyz', '2021-07-14 08:21:33', '2021-07-14 08:21:33'),
(266, '198', '143', '2021-07-14 09:59:15', 'Nap', 'xyz', '2021-07-14 09:59:15', '2021-07-14 09:59:15'),
(267, '198', '143', '2021-07-14 10:08:30', 'Meal', 'xyz', '2021-07-14 10:08:30', '2021-07-14 10:08:30'),
(268, '198', '143', '2021-07-14 10:10:03', 'Drink', 'xyz', '2021-07-14 10:10:03', '2021-07-14 10:10:03'),
(269, '198', '143', '2021-07-14 10:54:09', 'Drink', 'xyz', '2021-07-14 10:54:09', '2021-07-14 10:54:09'),
(270, '198', '143', '2021-07-14 10:54:26', 'Drink', 'xyz', '2021-07-14 10:54:26', '2021-07-14 10:54:26'),
(271, '198', '143', '2021-07-15 04:36:34', 'Drink', 'xyz', '2021-07-15 04:36:34', '2021-07-15 04:36:34'),
(272, '198', '143', '2021-07-15 04:46:43', 'Poop', 'xyz', '2021-07-15 04:46:43', '2021-07-15 04:46:43'),
(273, '198', '143', '2021-07-15 06:59:47', 'Meal', 'xyz', '2021-07-15 06:59:47', '2021-07-15 06:59:47'),
(274, '198', '143', '2021-07-17 05:35:49', 'Drink', 'xyz', '2021-07-17 05:35:49', '2021-07-17 05:35:49'),
(275, '198', '143', '2021-07-23 12:36:29', 'Drink', 'xyz', '2021-07-23 12:36:29', '2021-07-23 12:36:29'),
(276, '203', '160', '2021-07-27 11:09:18', 'Drink', 'xyz', '2021-07-27 11:09:18', '2021-07-27 11:09:18'),
(277, '203', '160', '2021-07-27 11:09:22', 'Peepee', 'xyz', '2021-07-27 11:09:22', '2021-07-27 11:09:22'),
(278, '203', '160', '2021-07-27 11:09:28', 'Meal', 'xyz', '2021-07-27 11:09:28', '2021-07-27 11:09:28'),
(279, '203', '160', '2021-07-27 11:09:30', 'Poop', 'xyz', '2021-07-27 11:09:30', '2021-07-27 11:09:30'),
(280, '203', '160', '2021-07-27 11:11:43', 'Drink', 'xyz', '2021-07-27 11:11:43', '2021-07-27 11:11:43'),
(281, '203', '160', '2021-07-27 11:11:45', 'Peepee', 'xyz', '2021-07-27 11:11:45', '2021-07-27 11:11:45'),
(282, '203', '160', '2021-07-27 11:11:48', 'Meal', 'xyz', '2021-07-27 11:11:48', '2021-07-27 11:11:48'),
(283, '203', '160', '2021-07-27 11:11:49', 'Nap', 'xyz', '2021-07-27 11:11:49', '2021-07-27 11:11:49'),
(284, '203', '160', '2021-07-27 11:11:51', 'Nap', 'xyz', '2021-07-27 11:11:51', '2021-07-27 11:11:51'),
(285, '203', '160', '2021-07-27 11:11:52', 'Meal', 'xyz', '2021-07-27 11:11:52', '2021-07-27 11:11:52'),
(286, '203', '160', '2021-07-27 11:11:54', 'Drink', 'xyz', '2021-07-27 11:11:54', '2021-07-27 11:11:54'),
(287, '203', '160', '2021-07-28 04:14:23', 'Drink', 'xyz', '2021-07-28 04:14:23', '2021-07-28 04:14:23'),
(288, '203', '160', '2021-07-28 04:14:27', 'Drink', 'xyz', '2021-07-28 04:14:27', '2021-07-28 04:14:27'),
(289, '203', '160', '2021-07-28 12:09:06', 'Peepee', 'xyz', '2021-07-28 12:09:06', '2021-07-28 12:09:06'),
(290, '203', '160', '2021-07-28 12:09:09', 'Peepee', 'xyz', '2021-07-28 12:09:09', '2021-07-28 12:09:09'),
(291, '203', '160', '2021-07-29 06:25:10', 'Drink', 'xyz', '2021-07-29 06:25:10', '2021-07-29 06:25:10'),
(292, '203', '160', '2021-07-29 06:25:12', 'Meal', 'xyz', '2021-07-29 06:25:12', '2021-07-29 06:25:12'),
(293, '203', '160', '2021-07-29 06:25:14', 'Nap', 'xyz', '2021-07-29 06:25:14', '2021-07-29 06:25:14'),
(294, '203', '160', '2021-07-29 06:25:17', 'Peepee', 'xyz', '2021-07-29 06:25:17', '2021-07-29 06:25:17'),
(295, '203', '160', '2021-07-29 06:25:19', 'Poop', 'xyz', '2021-07-29 06:25:19', '2021-07-29 06:25:19'),
(296, '203', '160', '2021-07-29 09:08:14', 'Peepee', 'xyz', '2021-07-29 09:08:14', '2021-07-29 09:08:14'),
(297, '203', '160', '2021-07-29 09:08:15', 'Poop', 'xyz', '2021-07-29 09:08:15', '2021-07-29 09:08:15'),
(298, '203', '160', '2021-07-29 10:04:54', 'Peepee', 'xyz', '2021-07-29 10:04:54', '2021-07-29 10:04:54'),
(299, '203', '160', '2021-07-29 10:04:56', 'Poop', 'xyz', '2021-07-29 10:04:56', '2021-07-29 10:04:56'),
(300, '205', '161', '2021-07-29 10:12:58', 'Drink', 'xyz', '2021-07-29 10:12:58', '2021-07-29 10:12:58'),
(301, '205', '161', '2021-07-29 10:13:03', 'Meal', 'xyz', '2021-07-29 10:13:03', '2021-07-29 10:13:03'),
(302, '205', '161', '2021-07-30 04:48:46', 'Drink', 'xyz', '2021-07-30 04:48:46', '2021-07-30 04:48:46'),
(303, '205', '161', '2021-07-30 04:49:04', 'Meal', 'xyz', '2021-07-30 04:49:04', '2021-07-30 04:49:04'),
(304, '204', '163', '2021-07-30 14:32:32', 'Drink', 'xyz', '2021-07-30 14:32:32', '2021-07-30 14:32:32'),
(305, '204', '163', '2021-07-30 14:35:22', 'Drink', 'xyz', '2021-07-30 14:35:22', '2021-07-30 14:35:22'),
(306, '205', '161', '2021-08-02 05:26:25', 'Drink', 'xyz', '2021-08-02 05:26:25', '2021-08-02 05:26:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `apple_login_id` text COLLATE utf8mb4_unicode_ci,
  `google_login_id` text COLLATE utf8mb4_unicode_ci,
  `facebook_login_id` text COLLATE utf8mb4_unicode_ci,
  `firstName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profilePic` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1= male, 2= female',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userEnableStatus` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `notificationStatus` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `apple_login_id`, `google_login_id`, `facebook_login_id`, `firstName`, `lastName`, `email`, `phone`, `profilePic`, `gender`, `email_verified_at`, `password`, `userEnableStatus`, `notificationStatus`, `remember_token`, `created_at`, `updated_at`) VALUES
(15, NULL, NULL, NULL, 'Pawan ', 'Gupta', 'pawan.k@richestsoft.in', '9839571075', '', 'male', NULL, 'a039597ed5f3cef3a945e33981918b48', '1', '1', NULL, '2020-12-01 07:12:13', '2021-07-30 09:03:35'),
(19, NULL, NULL, NULL, 'test', 'hello', NULL, '+91852145214', 'IlV1WISoWg_Screenshot 2021-05-21 183256.png', 'female', NULL, 'd41d8cd98f00b204e9800998ecf8427e', '1', '1', NULL, '2020-12-02 08:49:59', '2021-05-25 04:19:57'),
(20, NULL, NULL, NULL, 'kaur', 'kaur', 'er41@gmail.com', '3554655555', 'srkxZtKChD_avatar.png', 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-02 13:09:27', '2020-12-02 13:09:27'),
(21, NULL, NULL, NULL, 'kaur', 'kaur', 'er941@gmail.com', '35546555558', 'srkxZtKChD_avatar.png', 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-03 13:54:49', '2020-12-03 13:54:49'),
(23, NULL, NULL, NULL, 'kaur', 'kaur', 'erop@gmail.com', '355465852', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-05 18:11:55', '2020-12-05 18:11:55'),
(24, NULL, NULL, NULL, 'kaur', 'kaur', 'er1hjhub@gmail.com', '3554659876786', 'srkxZtKChD_avatar.png', 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-05 18:26:18', '2020-12-05 18:26:18'),
(25, NULL, NULL, NULL, 'kaur', 'kaur', 'er1hjhukb@gmail.com', '+917888581721', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-06 18:35:52', '2020-12-06 18:35:52'),
(26, NULL, NULL, NULL, 'kaur', 'kaur', 'er11@gmail.com', '355465425', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-07 06:29:29', '2020-12-07 06:29:29'),
(29, NULL, NULL, NULL, 'avneesh', 'mishra', 'avim@gmail.com', '9865321244', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 10:11:35', '2020-12-11 10:11:35'),
(30, NULL, NULL, NULL, 'kaur', 'kaur', 'gggf', '35546545646456', '', 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '1', NULL, '2020-12-11 12:06:39', '2020-12-28 12:37:27'),
(31, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '6532124587', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 12:49:10', '2020-12-11 12:49:10'),
(34, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '7845123265', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:20:01', '2020-12-11 13:20:01'),
(35, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '78487', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:37:12', '2020-12-11 13:37:12'),
(36, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '78487', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:37:46', '2020-12-11 13:37:46'),
(37, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '78487', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:37:49', '2020-12-11 13:37:49'),
(38, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '78487', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:37:56', '2020-12-11 13:37:56'),
(39, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '7848784', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:38:14', '2020-12-11 13:38:14'),
(40, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '213232', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:38:21', '2020-12-11 13:38:21'),
(41, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '213232', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:39:45', '2020-12-11 13:39:45'),
(42, NULL, NULL, NULL, 'avneesh', 'mishra', 'av645564i@gmail.com', NULL, NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:48:08', '2020-12-11 13:48:08'),
(44, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '213232445643', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:48:20', '2020-12-11 13:48:20'),
(45, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '213232445643', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:48:24', '2020-12-11 13:48:24'),
(46, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, NULL, NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:49:18', '2020-12-11 13:49:18'),
(47, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '445643424', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:53:48', '2020-12-11 13:53:48'),
(48, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '445643424', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:53:49', '2020-12-11 13:53:49'),
(50, NULL, NULL, NULL, 'avneesh', 'mishra', 'av4254645564i@gmail.com', NULL, NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:54:06', '2020-12-11 13:54:06'),
(52, NULL, NULL, NULL, 'avneesh', 'mishra', NULL, '445643424', NULL, 'male', NULL, '8feee527f1c53ab814010073c66ce50e', '1', '1', NULL, '2020-12-11 13:54:40', '2020-12-11 13:54:40'),
(53, NULL, NULL, NULL, 'bittu', 'singh', NULL, '12345678', NULL, 'Female', NULL, 'c31b32364ce19ca8fcd150a417ecce58', '1', '1', NULL, '2020-12-22 14:07:47', '2020-12-22 14:07:47'),
(55, NULL, NULL, NULL, 'bittu', 'singh', NULL, '9855748751', NULL, 'Male', NULL, '8a76673ad939945c6a42b30e0ac0b039', '1', '1', NULL, '2020-12-23 09:55:22', '2021-03-15 13:57:10'),
(56, NULL, NULL, NULL, 'kaur', 'kaur', 'er1@gmail.com', '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-23 10:35:14', '2020-12-23 10:35:14'),
(61, NULL, NULL, NULL, 'kaur', 'kaur', 'er1a@gmail.com', '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-23 10:40:15', '2020-12-23 10:40:15'),
(63, NULL, NULL, NULL, 'kaur', 'kaur', 'erl1a@gmail.com', '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-23 10:57:12', '2020-12-23 10:57:12'),
(66, NULL, NULL, NULL, 'bittu', 'singh', 'bittu@gmail.com', NULL, NULL, 'female', NULL, 'cd791744b71abd8167fbf9c282d7c13f', '1', '1', NULL, '2020-12-24 07:05:34', '2021-07-08 10:58:49'),
(67, NULL, NULL, NULL, 'bittu', 'singh', NULL, '9855748751', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-24 07:26:05', '2020-12-24 07:26:05'),
(68, NULL, NULL, NULL, 'bittu', 'singh', NULL, '9855748751', NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-24 14:04:03', '2020-12-24 14:04:03'),
(69, NULL, NULL, NULL, 'bittu', 'singh', NULL, '9855748751', NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-24 14:34:21', '2020-12-24 14:34:21'),
(70, NULL, NULL, NULL, 'bitttu', 'singh', NULL, '9855748751', NULL, 'Male', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2020-12-28 05:07:27', '2020-12-28 05:07:27'),
(71, NULL, NULL, NULL, 'bittu', 'singh', NULL, '9855748751', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-28 05:24:38', '2020-12-28 05:24:38'),
(73, NULL, NULL, NULL, 'bittu', 'singh', NULL, '9855748751', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '0', '1', NULL, '2020-12-28 05:25:19', '2020-12-28 12:37:47'),
(75, NULL, NULL, NULL, 'kaur1', 'kaur', 'er111@gmail.com', '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-31 12:22:54', '2020-12-31 12:22:54'),
(77, NULL, NULL, NULL, 'kaur1', 'kaur', 'er1110@gmail.com', '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-31 12:25:49', '2020-12-31 12:25:49'),
(79, NULL, NULL, NULL, 'kaur1', 'kaur', NULL, '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2020-12-31 12:30:45', '2020-12-31 12:30:45'),
(80, NULL, NULL, NULL, 'pawan', 'kumar', NULL, 'gupta@gmail.com', NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-01 11:07:50', '2021-01-01 11:07:50'),
(81, NULL, NULL, NULL, 'tst', 'lTst', NULL, 'bittu@gmail.com', NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-01 11:28:13', '2021-01-01 11:28:13'),
(82, NULL, NULL, NULL, 'kaur', 'kaur', 'abc@gmail.com', NULL, NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-01 11:29:28', '2021-01-01 11:29:28'),
(83, NULL, NULL, NULL, 'kaur', 'kaur', NULL, '35546545646456', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-01 11:31:14', '2021-01-01 11:31:14'),
(84, NULL, NULL, NULL, 'bittu', 'tst', NULL, 'bittu@gmail.com', NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-01 11:49:15', '2021-01-01 11:49:15'),
(85, NULL, NULL, NULL, 'tstFname', 'tstLname', NULL, 'test@gmail.com', NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-01 11:53:07', '2021-01-01 11:53:07'),
(86, NULL, NULL, NULL, 'kaur', 'kaur', NULL, '35546545646456', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-01 12:17:42', '2021-01-01 12:17:42'),
(89, NULL, NULL, NULL, 'bittu', 'singh', NULL, '9855748751', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 10:05:16', '2021-01-02 10:05:16'),
(92, NULL, NULL, NULL, 'kaur1', 'kaur', 'er11170@gmail.com', '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 10:09:14', '2021-01-02 10:09:14'),
(93, NULL, NULL, NULL, 'kaur1', 'kaur', NULL, '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 10:11:42', '2021-01-02 10:11:42'),
(94, NULL, NULL, NULL, 'kaur1', 'kaur', NULL, '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 10:11:55', '2021-01-02 10:11:55'),
(95, NULL, NULL, NULL, 'abc', 'av', 'bittu1@gmail.com', NULL, NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 10:15:53', '2021-01-02 10:15:53'),
(97, NULL, NULL, NULL, 'bittu', 'singh', 'bittu3232@gmail.com', NULL, NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 10:27:49', '2021-01-02 10:27:49'),
(98, NULL, NULL, NULL, 'qwe', 'asdf', 'asdfg@gmail.com', NULL, NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 10:39:58', '2021-01-02 10:39:58'),
(99, NULL, NULL, NULL, 'aaaa', 'ddd', 'usert@gmail.com', NULL, NULL, 'Male', NULL, 'a31b9ec48d2f44968054546f3a1a5315', '1', '1', NULL, '2021-01-02 10:45:34', '2021-02-26 13:12:57'),
(121, NULL, NULL, NULL, 'asd', 'zxc', NULL, '9876543210', NULL, 'Female', NULL, '827ccb0eea8a706c4c34a16891f84e7b', '1', '1', NULL, '2021-01-02 13:44:36', '2021-01-02 13:44:36'),
(122, NULL, NULL, NULL, 'asd', 'uji', NULL, '9876543211', NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 14:03:14', '2021-01-02 14:03:14'),
(123, NULL, NULL, NULL, 'ddd', 'hhh', NULL, '9639639635', NULL, 'Female', NULL, '46d045ff5190f6ea93739da6c0aa19bc', '1', '1', NULL, '2021-01-02 14:10:20', '2021-01-02 14:10:20'),
(124, NULL, '1111111', NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-02 14:10:32', '2021-01-02 14:10:32'),
(125, '25252522', NULL, NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-02 14:10:39', '2021-01-02 14:10:39'),
(126, NULL, NULL, NULL, 'ghj', 'jkl', 'asdf12@gmail.com', NULL, NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-02 14:12:42', '2021-01-02 14:12:42'),
(127, NULL, '111111132', NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-02 14:15:47', '2021-01-02 14:15:47'),
(128, '2525252211', NULL, NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-02 14:21:32', '2021-01-02 14:21:32'),
(129, '25252522111111', NULL, NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-02 14:22:50', '2021-01-02 14:22:50'),
(130, '12121176', NULL, NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-02 14:24:00', '2021-01-02 14:24:00'),
(131, '5555', NULL, NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-02 14:24:13', '2021-01-02 14:24:13'),
(132, NULL, NULL, NULL, 'testingFName', 'testingLName', NULL, '9998887770', NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-04 11:51:23', '2021-01-04 11:51:23'),
(133, '1111', NULL, NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-04 13:05:11', '2021-01-04 13:05:11'),
(134, NULL, '1111', NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-04 13:05:36', '2021-01-04 13:05:36'),
(135, NULL, '116651301522966069501', NULL, 'Aden', 'Dainty', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-07 06:09:38', '2021-01-07 06:09:38'),
(136, NULL, NULL, NULL, 'test', 'test', NULL, '2812363179', NULL, 'Male', NULL, 'c06db68e819be6ec3d26c6038d8e8d1f', '1', '1', NULL, '2021-01-07 10:33:22', '2021-01-07 10:33:22'),
(137, NULL, '104770205605648569397', NULL, 'Albert', 'souil', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-08 11:36:14', '2021-01-08 11:36:14'),
(138, NULL, '113298986552994128244', NULL, 'Kaur', 'Rajvir', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-08 13:38:35', '2021-01-08 13:38:35'),
(139, NULL, '110939020918152676583', NULL, 'Bittu', 'Singh', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-08 15:17:38', '2021-01-08 15:17:38'),
(140, NULL, NULL, NULL, 'jam', 'babu', NULL, '1244', NULL, 'Male', NULL, 'c624bbd382c2bb3dc5ef069b3df1eacf', '1', '1', NULL, '2021-01-08 18:11:20', '2021-01-08 18:11:20'),
(141, NULL, NULL, NULL, 'Jam', 'Babu', NULL, '1723359183', NULL, 'Male', NULL, '80e43ec2d1e732cb836237595842eb75', '1', '1', NULL, '2021-01-08 19:39:56', '2021-01-08 19:39:56'),
(142, NULL, NULL, NULL, 'Jam', 'Babu', NULL, '123456789', NULL, 'Male', NULL, '41b2fb28b9e1f30bd6e4167045194979', '1', '1', NULL, '2021-01-08 19:50:48', '2021-01-08 19:50:48'),
(143, NULL, NULL, NULL, 'jamu', 'lama', 'jam@gmail.com', NULL, NULL, 'Male', NULL, '06e397851cc52cfab30e0683487c9d41', '1', '1', NULL, '2021-01-08 21:33:57', '2021-01-08 21:33:57'),
(144, NULL, NULL, NULL, 'jambu', 'mambu', NULL, '634643', NULL, 'Male', NULL, '5af2ef6021514f80f1a24e0dd9f281e6', '1', '1', NULL, '2021-01-08 21:39:22', '2021-01-08 21:39:22'),
(145, NULL, NULL, NULL, 'jaman', 'man', NULL, '222', NULL, 'Male', NULL, 'bcbe3365e6ac95ea2c0343a2395834dd', '1', '1', NULL, '2021-01-11 06:15:55', '2021-01-11 06:15:55'),
(146, NULL, '101131030874862386695', NULL, 'Android', 'B', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-11 11:40:17', '2021-01-11 11:40:17'),
(147, NULL, '117103015840099869019', NULL, 'Robert', 'luios', 'testjunior029@gmail.com', NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-15 12:50:23', '2021-07-09 07:18:32'),
(148, NULL, NULL, NULL, 'david', 'larocque', 'dave393939@hotmail.com', NULL, NULL, 'Male', NULL, 'ed2b1f468c5f915f3f1cf75d7068baae', '1', '1', NULL, '2021-01-16 01:34:47', '2021-01-16 01:34:47'),
(149, NULL, NULL, NULL, 'baby', '1', NULL, 'y', NULL, 'Male', NULL, '19b6d4a977f1e9d8797c61bf1e7b5651', '1', '1', NULL, '2021-01-16 06:44:33', '2021-01-16 06:44:33'),
(150, NULL, '105169096493990006592', NULL, 'Rajvir', 'Kaur', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-22 15:09:50', '2021-01-22 15:09:50'),
(151, NULL, NULL, NULL, 'newUsr', 'tstLstname', NULL, '9988776655', NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-01-25 06:30:58', '2021-01-25 06:30:58'),
(152, NULL, '114060454960956820250', NULL, 'Parul', 'kalsy', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-01-30 04:40:38', '2021-01-30 04:40:38'),
(153, NULL, NULL, NULL, 'hfh', 'fgh', NULL, '87654321', NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-02-03 12:31:20', '2021-02-03 12:31:20'),
(154, NULL, '109157192024099251830', NULL, 'David', 'Larocque', '', NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-02-03 21:36:11', '2021-07-09 18:04:18'),
(155, NULL, NULL, NULL, 'Abhay', 'rai', NULL, '8699105363', NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-02-05 06:09:52', '2021-02-05 06:09:52'),
(156, NULL, NULL, NULL, 'sus', 'kaur', 'sus@gmail.com', '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-02-05 13:38:14', '2021-02-05 13:38:14'),
(157, NULL, NULL, NULL, 'David', 'Larocque', NULL, '5145743985', NULL, 'Male', NULL, 'ed2b1f468c5f915f3f1cf75d7068baae', '1', '1', NULL, '2021-02-05 20:13:26', '2021-02-05 20:13:26'),
(158, NULL, NULL, NULL, 'jghj', 'ghj', NULL, '7248635678', NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-02-06 06:33:21', '2021-02-06 06:33:21'),
(159, NULL, NULL, NULL, 'hjkhj', 'hjk', 'harjinder.singh@gmail.com', '+917508730053', NULL, 'Male', NULL, 'b80ad62c8e09664e54409b76da16afff', '1', '1', NULL, '2021-02-06 07:30:39', '2021-02-26 13:12:26'),
(160, NULL, NULL, NULL, 'sdf', 'sdf', 'asd@gmail.com', NULL, 'srkxZtKChD_avatar.png', 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-02-10 09:43:46', '2021-02-10 09:43:46'),
(161, NULL, NULL, NULL, 'asdf', 'asdf', 'asdf@gmail.com', NULL, NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-02-11 10:50:51', '2021-02-11 10:50:51'),
(162, NULL, NULL, NULL, 'dave', 'larocque', 'earlypotty@gmail.com', NULL, NULL, 'Male', NULL, '81dc9bdb52d04dc20036dbd8313ed055', '1', '1', NULL, '2021-02-11 15:14:17', '2021-02-11 15:14:17'),
(163, NULL, NULL, NULL, 'kaur', 'kaur', 'er21@gmail.com', '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-02-23 09:43:08', '2021-02-23 09:43:08'),
(164, NULL, NULL, NULL, 'ruhi', 'sharma', 'dcsa.ankita11@gmail.com', NULL, NULL, 'Female', NULL, '9a6a6a2ecf7a35118899824159090321', '1', '1', NULL, '2021-03-17 05:54:51', '2021-03-31 12:41:46'),
(165, NULL, '105699788316888099121', NULL, 'Test', 'B', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-03-18 11:08:21', '2021-03-18 11:08:21'),
(166, NULL, '117180504380468674233', NULL, 'Ranjit', 'singh', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-03-19 09:31:17', '2021-03-19 09:31:17'),
(167, NULL, NULL, NULL, 'arvi', 'arvi', 'xyz12@gmail.com', NULL, NULL, 'Female', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-03-19 10:30:02', '2021-03-19 10:30:02'),
(168, NULL, NULL, NULL, 'adani', 'jp', NULL, '7973747512', NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-03-25 09:47:43', '2021-03-25 09:47:43'),
(169, NULL, NULL, NULL, 'anamika', 'shrma', 'abcd@gmail.com', NULL, NULL, 'Female', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-03-25 12:26:53', '2021-03-25 12:26:53'),
(170, NULL, NULL, NULL, 'anu', 'sharma', NULL, '8284809193', NULL, 'Female', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-03-25 12:54:46', '2021-03-25 12:54:46'),
(171, NULL, NULL, NULL, 'amit', 'singh', NULL, '7710603939', NULL, 'Male', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-03-30 09:36:57', '2021-03-30 09:36:57'),
(172, NULL, NULL, NULL, 'Abhii', 'Sharma', NULL, '987654321', NULL, 'Male', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-03-30 09:46:12', '2021-03-30 09:46:12'),
(173, '888888', NULL, NULL, 'Pawan', 'Gupta', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-03-31 05:43:34', '2021-03-31 05:43:34'),
(174, 'geetrps1@gmail.com', NULL, NULL, 'Gurpreet', 'Kaur', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-03-31 06:37:22', '2021-03-31 06:37:22'),
(175, NULL, NULL, NULL, 'kaur', 'kaur', NULL, '355465', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-03-31 06:41:05', '2021-03-31 06:41:05'),
(176, NULL, NULL, NULL, 'kaur', 'kaur', NULL, '35546577', NULL, 'female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-03-31 07:49:10', '2021-03-31 07:49:10'),
(177, NULL, NULL, NULL, 'abxx', 'sharma', 'abx@yopmail.com', NULL, NULL, 'Female', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-03-31 10:11:32', '2021-03-31 10:11:32'),
(178, NULL, NULL, NULL, 'rst', 'yff', 'harjinder.singh@richestsoft.in', '+917888604798', NULL, 'Female', NULL, 'ff20a221a3b7dee206029aa065f5bc2d', '1', '1', NULL, '2021-03-31 10:50:57', '2021-03-31 12:35:20'),
(179, NULL, NULL, NULL, 'abhik', 'sharma', 'abhik@gmail.com', NULL, NULL, 'Male', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-04-05 06:57:47', '2021-04-05 06:57:47'),
(180, NULL, NULL, NULL, 'dny', 'as', 'appwebtesting7@gmail.com', NULL, NULL, 'Female', NULL, '3896f4839a8691971eabe602201f0803', '1', '1', NULL, '2021-04-06 06:21:33', '2021-04-06 06:25:59'),
(181, NULL, NULL, NULL, 'david', 'san', 'appwebtesting@gmail.com', NULL, NULL, 'Female', NULL, '7904b060e872acbd608ab77b2619983b', '1', '1', NULL, '2021-04-07 03:55:25', '2021-04-07 04:52:44'),
(182, NULL, NULL, NULL, 'bb', 'hbb', 'asdr@gmail.com', NULL, NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-04-07 04:51:33', '2021-04-07 04:51:33'),
(183, NULL, NULL, NULL, 'abhiga', 'ffh', 'abhiga@gmail.com', NULL, NULL, 'Female', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-04-07 10:06:52', '2021-04-07 10:06:52'),
(184, NULL, NULL, NULL, 'anbs', 'xbxh', 'aqwe@gmail.com', NULL, NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-04-07 11:26:23', '2021-04-07 11:26:23'),
(185, NULL, NULL, NULL, 'as', 'jk', 'robo@gmail.com', NULL, NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-04-08 04:07:32', '2021-04-08 04:07:32'),
(186, NULL, NULL, NULL, 'paras', 'shrma', 'paras@gmail.com', NULL, NULL, 'Male', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-04-08 10:26:12', '2021-04-08 10:26:12'),
(187, NULL, NULL, NULL, 'ngio', 'aw', 'devi@gmail.com', NULL, NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-04-15 04:49:10', '2021-04-15 04:49:10'),
(188, NULL, NULL, NULL, 'nbio', 'b', 'davide@gmail.com', NULL, NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-04-15 10:24:47', '2021-04-15 10:24:47'),
(189, NULL, NULL, NULL, 'novak', 'jomovic', 'novak@gmail.com', NULL, NULL, 'Male', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-04-15 10:58:17', '2021-04-15 10:58:17'),
(190, NULL, NULL, NULL, 'sapna', 'sapna', 'sapna@gmail.com', NULL, NULL, 'Female', NULL, '25f9e794323b453885f5181f1b624d0b', '1', '1', NULL, '2021-04-15 11:02:28', '2021-04-15 11:02:28'),
(191, NULL, '108787192957662006137', NULL, 'Ankita', 'Singh', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-04-16 07:06:11', '2021-04-16 07:06:11'),
(192, NULL, NULL, NULL, 'bui', 'g', 'sarb@gmail.com', NULL, NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-04-16 09:28:32', '2021-04-16 09:28:32'),
(193, NULL, NULL, NULL, 'hsj', 'hsh', 'lorim@gmail.com', NULL, NULL, 'Male', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-04-16 10:00:55', '2021-04-16 10:00:55'),
(194, NULL, NULL, NULL, 'sandeep', 'kaur', 'sandy@gmail.com', NULL, NULL, 'Female', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-05-14 10:11:48', '2021-05-14 10:11:48'),
(195, NULL, NULL, NULL, 'abc', 'cd', 'rajvirrichestsoft@gmail.com', NULL, NULL, 'Female', NULL, 'e10adc3949ba59abbe56e057f20f883e', '1', '1', NULL, '2021-05-15 10:06:10', '2021-05-15 10:06:10'),
(196, NULL, NULL, NULL, 'john', 'john', 'john@gmail.com', NULL, NULL, 'Male', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-05-18 03:37:23', '2021-05-18 03:37:23'),
(197, NULL, NULL, NULL, 'bacti', 'as', 'qa@gmail.com', NULL, NULL, 'Male', NULL, 'd6b0ab7f1c8ab8f514db9a6d85de160a', '1', '1', NULL, '2021-05-18 10:55:51', '2021-05-18 10:55:51'),
(198, NULL, NULL, NULL, 'sandeep', 'kaur', 'sandeep12@gmail.com', NULL, 'wQsyF9mhkL_scaled_image_picker6812039903884319078.jpg', 'female', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-05-24 09:41:38', '2021-07-09 04:08:35'),
(199, NULL, NULL, NULL, 'onkar', 'singh', 'onkar@gmail.com', NULL, '2UsJLgxBUc_scaled_image_picker285362650820550417.jpg', 'male', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-05-25 04:03:13', '2021-05-25 11:10:09'),
(200, NULL, NULL, NULL, 'karlo wah', 'bst', 'karlo@gmail.com', NULL, 'N3dzhDKrFr_scaled_image_picker6732646682133068372.jpg', 'female', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-05-26 03:41:58', '2021-05-26 09:53:58'),
(201, NULL, NULL, NULL, 'jass', 'kaur', 'jasi@gmail.com', NULL, NULL, 'Female', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-05-26 06:44:30', '2021-05-26 06:44:30'),
(202, NULL, NULL, NULL, 'jasi', 'jasi', 'jas@gmail.com', NULL, 'YJC0kbnN16_scaled_image_picker126731356729291153.jpg', 'male', NULL, '25d55ad283aa400af464c76d713c07ad', '1', '1', NULL, '2021-06-18 04:24:07', '2021-06-18 04:26:00'),
(204, NULL, '113938549222193883279', NULL, 'Prashant', 'Koradiya', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-07-28 11:01:50', '2021-07-28 11:01:50'),
(205, NULL, '113758750379497901366', NULL, 'Bhavesh', 'Shah', NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, '2021-07-29 10:07:20', '2021-07-29 10:07:20');

-- --------------------------------------------------------

--
-- Table structure for table `user_otps`
--

CREATE TABLE `user_otps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phoneOremail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Otp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_otps`
--

INSERT INTO `user_otps` (`id`, `phoneOremail`, `Otp`, `created_at`, `updated_at`) VALUES
(5, 'manpreet@gmail.com', '2338', '2020-11-16 10:09:45', '2020-11-16 10:09:45'),
(6, 'manpreet@gmail.com', '1788', '2020-11-16 10:10:14', '2020-11-16 10:10:14'),
(8, '+919839571074', '1626', '2020-12-01 09:58:35', '2020-12-01 09:58:35'),
(9, '+919839571074', '1626', '2020-12-01 10:03:02', '2020-12-01 10:03:02'),
(10, '+919839571074', '1626', '2020-12-01 10:07:36', '2020-12-01 10:07:36'),
(11, '+919839571074', '4345', '2020-12-02 05:45:43', '2020-12-02 05:45:43'),
(12, '+919839571074', '5602', '2020-12-02 09:57:33', '2020-12-02 09:57:33'),
(13, '+919839571074', '2561', '2020-12-02 09:57:39', '2020-12-02 09:57:39'),
(14, '+9198439571074', '8018', '2020-12-04 14:18:12', '2020-12-04 14:18:12'),
(15, '7888581721', '6250', '2020-12-05 18:02:21', '2020-12-05 18:02:21'),
(16, '+91983958990', '2056', '2020-12-05 18:29:11', '2020-12-05 18:29:11'),
(17, '+917888581721', '4882', '2020-12-06 18:33:32', '2020-12-06 18:33:32'),
(18, '7888581721', '6969', '2020-12-11 06:22:22', '2020-12-11 06:22:22'),
(19, '7888581721', '8888', '2020-12-11 06:22:37', '2020-12-11 06:22:37'),
(20, '7888581721', '1329', '2020-12-11 06:23:13', '2020-12-11 06:23:13'),
(21, '7888581721', '4919', '2020-12-11 06:23:45', '2020-12-11 06:23:45'),
(22, '7888581721', '8010', '2020-12-11 06:24:09', '2020-12-11 06:24:09'),
(23, '7888581721', '7394', '2020-12-11 06:24:52', '2020-12-11 06:24:52'),
(24, '7888581721', '1839', '2020-12-11 06:28:19', '2020-12-11 06:28:19'),
(25, '7888581721', '2089', '2020-12-11 06:39:03', '2020-12-11 06:39:03'),
(26, '7888581721', '5904', '2020-12-11 06:57:39', '2020-12-11 06:57:39'),
(27, '7888581721', '6965', '2020-12-11 07:04:42', '2020-12-11 07:04:42'),
(28, '8699105363', '5027', '2020-12-11 07:08:46', '2020-12-11 07:08:46'),
(29, '34343434', '8817', '2020-12-11 07:09:23', '2020-12-11 07:09:23'),
(30, '9865321245', '9540', '2020-12-11 09:21:44', '2020-12-11 09:21:44'),
(31, '7888581721', '6908', '2020-12-11 09:28:29', '2020-12-11 09:28:29'),
(32, '7888581721', '6682', '2020-12-11 10:26:22', '2020-12-11 10:26:22'),
(33, '9855748751', '2845', '2020-12-22 13:22:17', '2020-12-22 13:22:17'),
(34, '9855748751', '7297', '2020-12-22 13:29:39', '2020-12-22 13:29:39'),
(35, '9855748751', '2201', '2020-12-22 14:02:26', '2020-12-22 14:02:26'),
(36, '9865321245', '8054', '2020-12-22 14:15:07', '2020-12-22 14:15:07'),
(37, '9855748751', '2201', '2020-12-22 15:01:56', '2020-12-22 15:01:56'),
(38, '+919839571074', '2388', '2020-12-23 07:03:28', '2020-12-23 07:03:28'),
(39, '+919839571074', '7606', '2020-12-23 07:14:43', '2020-12-23 07:14:43'),
(40, '+919839571074', '7686', '2020-12-23 07:19:10', '2020-12-23 07:19:10'),
(41, '+919839571074', '4375', '2020-12-23 07:19:13', '2020-12-23 07:19:13'),
(42, '+919839571074', '4557', '2020-12-23 07:19:31', '2020-12-23 07:19:31'),
(43, '+919839571074', '4221', '2020-12-23 07:19:34', '2020-12-23 07:19:34'),
(44, '+919839571074', '3832', '2020-12-23 07:19:40', '2020-12-23 07:19:40'),
(45, '+919839571074', '2353', '2020-12-23 07:20:00', '2020-12-23 07:20:00'),
(46, '+919839571074', '9494', '2020-12-23 07:20:02', '2020-12-23 07:20:02'),
(47, '+919839571074', '9159', '2020-12-23 07:20:34', '2020-12-23 07:20:34'),
(48, '+919839571074', '8047', '2020-12-23 07:22:31', '2020-12-23 07:22:31'),
(49, '+919839571074', '6988', '2020-12-23 07:25:36', '2020-12-23 07:25:36'),
(50, '+919839571074', '7415', '2020-12-23 07:26:22', '2020-12-23 07:26:22'),
(51, '+919839571074', '9483', '2020-12-23 07:27:25', '2020-12-23 07:27:25'),
(52, '9855748751', '3124', '2020-12-23 07:31:42', '2020-12-23 07:31:42'),
(53, '9855748751', '3672', '2020-12-23 07:37:37', '2020-12-23 07:37:37'),
(54, '9855748751', '8829', '2020-12-23 07:39:49', '2020-12-23 07:39:49'),
(55, '9855748751', '8649', '2020-12-23 07:43:18', '2020-12-23 07:43:18'),
(56, '9855748751', '9383', '2020-12-23 07:50:26', '2020-12-23 07:50:26'),
(57, '9855748751', '1821', '2020-12-23 08:02:53', '2020-12-23 08:02:53'),
(58, '9855748751', '1936', '2020-12-23 08:09:03', '2020-12-23 08:09:03'),
(59, '9855748751', '7005', '2020-12-23 09:25:31', '2020-12-23 09:25:31'),
(60, '9855748751', '2170', '2020-12-23 09:30:23', '2020-12-23 09:30:23'),
(61, '9855748751', '4046', '2020-12-23 09:33:42', '2020-12-23 09:33:42'),
(62, '9855748751', '8029', '2020-12-23 09:36:10', '2020-12-23 09:36:10'),
(63, 'bittu@gmail.com', '9722', '2020-12-24 13:52:37', '2020-12-24 13:52:37'),
(64, '+919839571074', '7076', '2020-12-24 13:54:22', '2020-12-24 13:54:22'),
(65, '+919839571074', '5817', '2020-12-24 13:56:42', '2020-12-24 13:56:42'),
(66, 'bittu@gmail.com', '2744', '2020-12-24 13:56:49', '2020-12-24 13:56:49'),
(67, 'bittu@gmail.com', '8592', '2020-12-24 14:00:53', '2020-12-24 14:00:53'),
(68, '+919839571074', '6481', '2020-12-24 14:01:31', '2020-12-24 14:01:31'),
(69, '9855748751', '8928', '2020-12-24 14:02:59', '2020-12-24 14:02:59'),
(70, '9855748751', '4294', '2020-12-24 14:03:12', '2020-12-24 14:03:12'),
(71, 'bittu@gmail.com', '2878', '2020-12-24 14:05:04', '2020-12-24 14:05:04'),
(72, 'abctest@gmail.com', '3618', '2020-12-24 15:31:19', '2020-12-24 15:31:19'),
(73, 'abctest@gmail.com', '3193', '2020-12-24 15:31:41', '2020-12-24 15:31:41'),
(74, '85295412585', '7076', '2020-12-27 11:50:55', '2020-12-27 11:50:55'),
(75, '9855748751', '4923', '2020-12-28 05:04:57', '2020-12-28 05:04:57'),
(76, 'bitttu@gmail.com', '8996', '2020-12-28 05:22:06', '2020-12-28 05:22:06'),
(77, '+919839571074', '5697', '2020-12-28 05:25:47', '2020-12-28 05:25:47'),
(78, 'bittu@gmail.com', '1792', '2020-12-28 05:26:25', '2020-12-28 05:26:25'),
(79, 'bittu@gmail.com', '9031', '2020-12-28 05:30:37', '2020-12-28 05:30:37'),
(80, '+919839571074', '5624', '2020-12-28 05:30:47', '2020-12-28 05:30:47'),
(81, '+919839571074', '9954', '2020-12-28 06:09:51', '2020-12-28 06:09:51'),
(82, '+919839571074', '3762', '2020-12-28 06:11:44', '2020-12-28 06:11:44'),
(83, '+9198395710742', '1590', '2020-12-28 06:12:05', '2020-12-28 06:12:05'),
(84, '+91983957107422', '7679', '2020-12-28 06:12:16', '2020-12-28 06:12:16'),
(85, NULL, '9828', '2020-12-28 06:12:22', '2020-12-28 06:12:22'),
(86, NULL, '7308', '2020-12-28 06:12:52', '2020-12-28 06:12:52'),
(87, 'bittu@gmail.com', '3993', '2020-12-28 10:10:16', '2020-12-28 10:10:16'),
(88, 'bittu@gmail.com', '5951', '2020-12-28 10:10:53', '2020-12-28 10:10:53'),
(89, '+919839571074', '1054', '2020-12-28 10:11:00', '2020-12-28 10:11:00'),
(90, 'bittu@gmail.com', '1145', '2020-12-28 10:11:24', '2020-12-28 10:11:24'),
(91, '+919839571074', '6311', '2020-12-28 10:13:15', '2020-12-28 10:13:15'),
(92, 'bittu@gmail.com', '3083', '2020-12-28 10:32:06', '2020-12-28 10:32:06'),
(93, 'bittu@gmail.com', '3851', '2020-12-28 10:33:56', '2020-12-28 10:33:56'),
(94, '9855748751', '4107', '2020-12-28 10:34:46', '2020-12-28 10:34:46'),
(95, 'bittu@gmail.com', '2342', '2020-12-28 10:35:06', '2020-12-28 10:35:06'),
(96, '+919839571074', '6267', '2020-12-28 10:35:38', '2020-12-28 10:35:38'),
(97, 'bittu@gmail.com', '4596', '2020-12-28 10:40:11', '2020-12-28 10:40:11'),
(98, '9855748751', '8442', '2020-12-28 10:42:35', '2020-12-28 10:42:35'),
(99, 'bittu@gmail.com', '2481', '2020-12-28 10:42:49', '2020-12-28 10:42:49'),
(100, 'bittu@gmail.com', '7565', '2020-12-28 10:45:04', '2020-12-28 10:45:04'),
(101, 'bittu@gmail.com', '5418', '2020-12-28 11:29:59', '2020-12-28 11:29:59'),
(102, 'tesa123@gmail.com', '1448', '2020-12-28 11:33:56', '2020-12-28 11:33:56'),
(103, '+919839571074', '3276', '2020-12-31 12:17:32', '2020-12-31 12:17:32'),
(104, 'tesa123@gmail.com', '5196', '2020-12-31 12:36:08', '2020-12-31 12:36:08'),
(105, NULL, '5974', '2020-12-31 12:36:14', '2020-12-31 12:36:14'),
(106, NULL, '6671', '2020-12-31 12:36:28', '2020-12-31 12:36:28'),
(107, 'tesa123@gmail.com', '5421', '2020-12-31 12:36:32', '2020-12-31 12:36:32'),
(108, NULL, '7203', '2020-12-31 12:36:40', '2020-12-31 12:36:40'),
(109, 'bittu@gmail.com', '3910', '2021-01-01 10:42:27', '2021-01-01 10:42:27'),
(110, 'bittu@gmail.com', '8195', '2021-01-01 10:44:32', '2021-01-01 10:44:32'),
(111, 'bittu@gmail.com', '6979', '2021-01-01 10:52:58', '2021-01-01 10:52:58'),
(112, 'bittu@gmail.com', '2712', '2021-01-01 11:00:12', '2021-01-01 11:00:12'),
(113, 'gupta@gmail.com', '1812', '2021-01-01 11:06:28', '2021-01-01 11:06:28'),
(114, 'gupta@gmail.com', '3519', '2021-01-01 11:06:48', '2021-01-01 11:06:48'),
(115, 'tesa123@gmail.com', '2268', '2021-01-01 11:13:46', '2021-01-01 11:13:46'),
(116, NULL, '8983', '2021-01-01 11:13:57', '2021-01-01 11:13:57'),
(117, NULL, '2792', '2021-01-01 11:14:07', '2021-01-01 11:14:07'),
(118, NULL, '5030', '2021-01-01 11:14:26', '2021-01-01 11:14:26'),
(119, 'bittu@gmail.com', '2310', '2021-01-01 11:25:02', '2021-01-01 11:25:02'),
(120, 'bittu@gmail.com', '6766', '2021-01-01 11:27:18', '2021-01-01 11:27:18'),
(121, 'bittu@gmail.com', '4696', '2021-01-01 11:38:29', '2021-01-01 11:38:29'),
(122, '+919839571074', '7394', '2021-01-01 11:43:08', '2021-01-01 11:43:08'),
(123, 'bittu@gmail.com', '6933', '2021-01-01 11:48:33', '2021-01-01 11:48:33'),
(124, 'test@gmail.com', '5923', '2021-01-01 11:52:36', '2021-01-01 11:52:36'),
(125, 'test@gmail.com', '5330', '2021-01-01 11:54:51', '2021-01-01 11:54:51'),
(126, 'bittu@gmail.com', '1393', '2021-01-01 12:06:12', '2021-01-01 12:06:12'),
(127, 'bittu@gmail.com', '2759', '2021-01-01 12:06:17', '2021-01-01 12:06:17'),
(128, 'bittu@gmail.com', '4037', '2021-01-01 12:12:06', '2021-01-01 12:12:06'),
(129, 'bittu@gmail.com', '6309', '2021-01-01 12:21:21', '2021-01-01 12:21:21'),
(130, 'bittu@gmail.com', '8078', '2021-01-01 12:30:32', '2021-01-01 12:30:32'),
(131, 'bittu@gmail.com', '2310', '2021-01-01 12:31:42', '2021-01-01 12:31:42'),
(132, '+91983957107456', '6029', '2021-01-01 12:33:31', '2021-01-01 12:33:31'),
(133, '355465852222', '1418', '2021-01-01 12:37:46', '2021-01-01 12:37:46'),
(134, 'bittu222@gmail.com', '1605', '2021-01-01 12:38:00', '2021-01-01 12:38:00'),
(135, '355465852222', '4643', '2021-01-01 12:41:17', '2021-01-01 12:41:17'),
(136, '3554658522', '9421', '2021-01-01 12:41:28', '2021-01-01 12:41:28'),
(137, '35546585222', '4072', '2021-01-01 12:41:32', '2021-01-01 12:41:32'),
(138, 'bittgu@gmail.com', '6317', '2021-01-01 12:47:04', '2021-01-01 12:47:04'),
(139, 'bittgu@gmail.com', '5883', '2021-01-01 12:47:16', '2021-01-01 12:47:16'),
(140, 'usert@gmail.com', '8118', '2021-01-02 09:42:07', '2021-01-02 09:42:07'),
(141, 'yifexi3639@tibui.com', '4640', '2021-01-02 09:43:33', '2021-01-02 09:43:33'),
(142, 'yifexi3639@tibui.com', '2203', '2021-01-02 09:45:27', '2021-01-02 09:45:27'),
(143, 'yifexi3639@tibui.com', '9770', '2021-01-02 09:45:56', '2021-01-02 09:45:56'),
(144, 'yifexi3639@tibui.com', '3448', '2021-01-02 09:51:03', '2021-01-02 09:51:03'),
(145, 'bittu1@gmail.com', '9453', '2021-01-02 10:14:49', '2021-01-02 10:14:49'),
(146, 'bittu1@gmail.com', '7568', '2021-01-02 10:15:16', '2021-01-02 10:15:16'),
(147, 'bittu01@gmail.com', '4917', '2021-01-02 10:20:53', '2021-01-02 10:20:53'),
(148, 'yifexi3639@tivui.com', '9359', '2021-01-02 10:21:54', '2021-01-02 10:21:54'),
(149, 'abcd@tibui.com', '1481', '2021-01-02 10:23:09', '2021-01-02 10:23:09'),
(150, 'abcd@tibui.com', '4657', '2021-01-02 10:23:13', '2021-01-02 10:23:13'),
(151, 'abcd@gmail.com', '5019', '2021-01-02 10:24:27', '2021-01-02 10:24:27'),
(152, 'yifexi3639@tibui.com', '8912', '2021-01-02 10:25:11', '2021-01-02 10:25:11'),
(153, 'asdfg@gmail.com', '7689', '2021-01-02 10:38:43', '2021-01-02 10:38:43'),
(154, 'usert@gmail.com', '3613', '2021-01-02 10:43:05', '2021-01-02 10:43:05'),
(155, 'yifexi3639@tibui.com', '5937', '2021-01-02 11:19:37', '2021-01-02 11:19:37'),
(156, 'asdfqwe@gmail.com', '6349', '2021-01-02 12:04:58', '2021-01-02 12:04:58'),
(157, '1234567890', '8467', '2021-01-02 12:05:22', '2021-01-02 12:05:22'),
(158, 'bittu11@gmail.com', '2007', '2021-01-02 12:53:10', '2021-01-02 12:53:10'),
(159, 'bittu11@gmail.com', '2850', '2021-01-02 12:53:33', '2021-01-02 12:53:33'),
(160, '123456612345', '7660', '2021-01-02 12:55:43', '2021-01-02 12:55:43'),
(161, '9876543210', '7293', '2021-01-02 13:34:22', '2021-01-02 13:34:22'),
(162, '9876543210', '7408', '2021-01-02 13:41:55', '2021-01-02 13:41:55'),
(163, '1234567890', '3883', '2021-01-02 14:01:01', '2021-01-02 14:01:01'),
(164, '9876543211', '7839', '2021-01-02 14:02:40', '2021-01-02 14:02:40'),
(165, '9639639635', '9387', '2021-01-02 14:09:20', '2021-01-02 14:09:20'),
(166, 'asdf@gmail.com', '2722', '2021-01-02 14:11:16', '2021-01-02 14:11:16'),
(167, 'asdf12@gmail.com', '9279', '2021-01-02 14:12:04', '2021-01-02 14:12:04'),
(168, '9998887770', '6520', '2021-01-04 11:50:17', '2021-01-04 11:50:17'),
(169, '5345345345', '8210', '2021-01-05 11:29:16', '2021-01-05 11:29:16'),
(170, '01305098428', '4505', '2021-01-07 10:25:22', '2021-01-07 10:25:22'),
(171, 'ashrafujjaman.babu@gmail.com', '6464', '2021-01-07 10:26:17', '2021-01-07 10:26:17'),
(172, '2812363179', '2141', '2021-01-07 10:30:01', '2021-01-07 10:30:01'),
(173, '2812363179', '8525', '2021-01-07 10:30:35', '2021-01-07 10:30:35'),
(174, '1244', '6303', '2021-01-08 18:10:26', '2021-01-08 18:10:26'),
(175, '1723359183', '1235', '2021-01-08 19:38:43', '2021-01-08 19:38:43'),
(176, '123456789', '5272', '2021-01-08 19:49:46', '2021-01-08 19:49:46'),
(177, '3625688', '6347', '2021-01-08 21:20:55', '2021-01-08 21:20:55'),
(178, 'jam@gmail.com', '2937', '2021-01-08 21:33:14', '2021-01-08 21:33:14'),
(179, '634643', '2171', '2021-01-08 21:38:45', '2021-01-08 21:38:45'),
(180, '222', '4103', '2021-01-11 06:15:38', '2021-01-11 06:15:38'),
(181, '+919872194714', '3306', '2021-01-14 06:30:02', '2021-01-14 06:30:02'),
(182, '+919872194714', '2591', '2021-01-14 07:19:22', '2021-01-14 07:19:22'),
(183, '+919872194714', '7455', '2021-01-14 07:19:26', '2021-01-14 07:19:26'),
(184, '+919872194714', '9266', '2021-01-14 07:20:01', '2021-01-14 07:20:01'),
(185, 'pawan.k@richestsoft.in', '9469', '2021-01-14 07:45:27', '2021-01-14 07:45:27'),
(186, 'dave393939@hotmail.com', '8226', '2021-01-16 01:33:09', '2021-01-16 01:33:09'),
(187, '9999999999', '9319', '2021-01-16 06:22:22', '2021-01-16 06:22:22'),
(188, 'rajvirrichestsoft@gmail.com', '5255', '2021-01-16 06:23:02', '2021-01-16 06:23:02'),
(189, 'rajvirrichestsoft@gmail.com', '3380', '2021-01-16 06:23:14', '2021-01-16 06:23:14'),
(190, '996633250', '7220', '2021-01-22 11:39:59', '2021-01-22 11:39:59'),
(191, '13152104488', '6147', '2021-01-22 15:04:43', '2021-01-22 15:04:43'),
(192, '13152104488', '7553', '2021-01-22 15:06:09', '2021-01-22 15:06:09'),
(193, '7973747512', '1788', '2021-01-22 15:08:29', '2021-01-22 15:08:29'),
(194, '9638527410', '7245', '2021-01-25 06:25:45', '2021-01-25 06:25:45'),
(195, '9988776655', '2134', '2021-01-25 06:26:09', '2021-01-25 06:26:09'),
(196, '6686866986869868', '5634', '2021-01-25 06:47:57', '2021-01-25 06:47:57'),
(197, '986996', '4589', '2021-01-25 07:00:09', '2021-01-25 07:00:09'),
(198, '323234', '1126', '2021-01-25 07:45:09', '2021-01-25 07:45:09'),
(199, '2131316', '3892', '2021-01-25 07:46:40', '2021-01-25 07:46:40'),
(200, 'yifexi3639@tibui.com', '7666', '2021-01-25 12:07:01', '2021-01-25 12:07:01'),
(201, '343437353835', '8364', '2021-02-03 05:24:14', '2021-02-03 05:24:14'),
(202, '87654321', '7245', '2021-02-03 12:30:43', '2021-02-03 12:30:43'),
(203, '8699105363', '1630', '2021-02-05 06:09:19', '2021-02-05 06:09:19'),
(204, 'jenny@exemplarymarketing.com', '4149', '2021-02-05 15:27:54', '2021-02-05 15:27:54'),
(205, '3152104488', '6219', '2021-02-05 16:03:41', '2021-02-05 16:03:41'),
(206, '5145743985', '7841', '2021-02-05 20:12:38', '2021-02-05 20:12:38'),
(207, 'earlypotty@gmail.com', '8371', '2021-02-05 20:23:13', '2021-02-05 20:23:13'),
(208, 'cdavsvz@gmail.com', '5167', '2021-02-06 04:05:52', '2021-02-06 04:05:52'),
(209, '7248635678', '1701', '2021-02-06 06:32:57', '2021-02-06 06:32:57'),
(210, '7888581723', '8459', '2021-02-06 07:29:58', '2021-02-06 07:29:58'),
(211, '8954756225', '7852', '2021-02-10 09:40:51', '2021-02-10 09:40:51'),
(212, 'asd@gmail.com', '9347', '2021-02-10 09:43:26', '2021-02-10 09:43:26'),
(213, '9648582010', '5150', '2021-02-11 05:36:36', '2021-02-11 05:36:36'),
(214, '9648582010', '8902', '2021-02-11 05:37:50', '2021-02-11 05:37:50'),
(215, '9648582010', '7702', '2021-02-11 06:21:37', '2021-02-11 06:21:37'),
(216, '+13094498162', '9476', '2021-02-11 06:53:49', '2021-02-11 06:53:49'),
(217, '+91 96485 82010', '6092', '2021-02-11 07:29:56', '2021-02-11 07:29:56'),
(218, '+91 96485 82010', '5020', '2021-02-11 07:30:34', '2021-02-11 07:30:34'),
(219, 'jenny@exemplarymarketing.com', '5796', '2021-02-11 10:03:59', '2021-02-11 10:03:59'),
(220, '+91 96485 82010', '7869', '2021-02-11 10:40:23', '2021-02-11 10:40:23'),
(221, 'asdf@gmail.com', '7620', '2021-02-11 10:50:32', '2021-02-11 10:50:32'),
(222, 'earlypotty@gmail.com', '5629', '2021-02-11 15:12:47', '2021-02-11 15:12:47'),
(223, 'gah@hshs.dbdd', '3961', '2021-02-25 11:44:55', '2021-02-25 11:44:55'),
(224, 'abc12@gmail.com', '3448', '2021-03-17 04:55:16', '2021-03-17 04:55:16'),
(225, 'ankita.s@richestsoft.in', '1725', '2021-03-17 04:55:52', '2021-03-17 04:55:52'),
(226, 'dcsa.ankita11@gmail.com', '5880', '2021-03-17 05:53:24', '2021-03-17 05:53:24'),
(227, 'dcsa.ankita11@gmail.com', '7762', '2021-03-17 05:54:03', '2021-03-17 05:54:03'),
(228, 'xyz12@gmail.com', '1498', '2021-03-19 10:29:33', '2021-03-19 10:29:33'),
(229, 'hello@gmail.com', '4361', '2021-03-22 08:17:50', '2021-03-22 08:17:50'),
(230, 'abcd@gmail.com', '8813', '2021-03-25 12:26:25', '2021-03-25 12:26:25'),
(231, 'aasada@gmail.com', '6264', '2021-03-31 08:02:13', '2021-03-31 08:02:13'),
(234, '+917888604798', '2275', '2021-03-31 09:43:36', '2021-03-31 09:43:36'),
(235, 'haha@ty.ok', '6369', '2021-03-31 09:44:28', '2021-03-31 09:44:28'),
(236, 'anc@yopmail.com', '9417', '2021-03-31 10:02:13', '2021-03-31 10:02:13'),
(238, 'abx@yopmail.com', '6194', '2021-03-31 10:10:40', '2021-03-31 10:10:40'),
(239, '+917888604798', '3392', '2021-03-31 10:50:03', '2021-03-31 10:50:03'),
(240, 'abhik@gmail.com', '4896', '2021-04-05 06:55:20', '2021-04-05 06:55:20'),
(241, 'abhik@gmail.com', '1663', '2021-04-05 06:57:13', '2021-04-05 06:57:13'),
(242, 'malikkurdi@yahoo.com', '9439', '2021-04-05 10:52:27', '2021-04-05 10:52:27'),
(243, 'malikkurdi@yahoo.com', '3293', '2021-04-06 05:49:05', '2021-04-06 05:49:05'),
(244, 'malikkurdi@yahoo.com', '4593', '2021-04-06 05:50:05', '2021-04-06 05:50:05'),
(245, 'appwebtesting7@gmail.com', '1345', '2021-04-06 06:19:36', '2021-04-06 06:19:36'),
(246, 'loop@gmail.com', '8754', '2021-04-06 06:22:36', '2021-04-06 06:22:36'),
(247, 'ghi@gmail.com', '1169', '2021-04-06 12:53:16', '2021-04-06 12:53:16'),
(248, 'appwebtesting@gmail.com', '1165', '2021-04-07 03:41:12', '2021-04-07 03:41:12'),
(249, 'appwebtesting@gmail.com', '7645', '2021-04-07 03:53:14', '2021-04-07 03:53:14'),
(250, 'asdr@gmail.com', '1942', '2021-04-07 04:51:13', '2021-04-07 04:51:13'),
(251, 'any@gmail.com', '8005', '2021-04-07 10:01:55', '2021-04-07 10:01:55'),
(252, 'abhiga@gmail.com', '8110', '2021-04-07 10:06:20', '2021-04-07 10:06:20'),
(253, 'aqwe@gmail.com', '6400', '2021-04-07 11:22:37', '2021-04-07 11:22:37'),
(254, 'robo@gmail.com', '2700', '2021-04-08 04:02:38', '2021-04-08 04:02:38'),
(255, 'paras@gmail.com', '6104', '2021-04-08 10:25:48', '2021-04-08 10:25:48'),
(256, 'devi@gmail.com', '9730', '2021-04-15 04:48:20', '2021-04-15 04:48:20'),
(257, 'app@gmail.com', '3561', '2021-04-15 09:57:37', '2021-04-15 09:57:37'),
(258, 'davide@gmail.com', '3168', '2021-04-15 10:24:24', '2021-04-15 10:24:24'),
(259, 'novak@gmail.com', '3880', '2021-04-15 10:57:37', '2021-04-15 10:57:37'),
(260, 'sapna@gmail.com', '6491', '2021-04-15 11:02:04', '2021-04-15 11:02:04'),
(261, 'sarb@gmail.com', '6445', '2021-04-16 09:27:13', '2021-04-16 09:27:13'),
(262, 'lorim@gmail.com', '7459', '2021-04-16 10:00:22', '2021-04-16 10:00:22'),
(263, 'Mazhar8416@gmail.com', '7491', '2021-04-30 17:28:15', '2021-04-30 17:28:15'),
(264, 'Mazhar8416@gmail.com', '2405', '2021-04-30 17:29:41', '2021-04-30 17:29:41'),
(265, 'sandy@gmail.com', '7227', '2021-05-14 10:10:29', '2021-05-14 10:10:29'),
(266, 'rajvirrichestsoft@gmail.com', '3585', '2021-05-15 10:03:32', '2021-05-15 10:03:32'),
(267, 'rajvirrichestsoft@gmail.com', '3601', '2021-05-15 10:03:42', '2021-05-15 10:03:42'),
(268, 'john@gmail.com', '8264', '2021-05-18 03:36:58', '2021-05-18 03:36:58'),
(269, 'qa@gmail.com', '8273', '2021-05-18 10:53:39', '2021-05-18 10:53:39'),
(270, 'sandeep12@gmail.com', '4242', '2021-05-24 09:40:57', '2021-05-24 09:40:57'),
(271, 'onkar@gmail.com', '2054', '2021-05-25 04:02:53', '2021-05-25 04:02:53'),
(272, 'karlo@gmail.com', '8243', '2021-05-26 03:40:09', '2021-05-26 03:40:09'),
(273, 'jasi@gmail.com', '1125', '2021-05-26 06:44:07', '2021-05-26 06:44:07'),
(274, 'jas@gmail.com', '4363', '2021-06-18 04:23:46', '2021-06-18 04:23:46'),
(275, 'bifip70367@britted.com', '3957', '2021-07-27 10:31:25', '2021-07-27 10:31:25'),
(276, 'bifip70367@britted.com', '6471', '2021-07-27 10:34:01', '2021-07-27 10:34:01'),
(277, 'bifip70367@britted.com', '7936', '2021-07-27 10:37:00', '2021-07-27 10:37:00'),
(278, 'shahbhavesh748@gmail.com', '5505', '2021-07-27 10:38:35', '2021-07-27 10:38:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addcards`
--
ALTER TABLE `addcards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_logins`
--
ALTER TABLE `app_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baby_profiles`
--
ALTER TABLE `baby_profiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocked_users`
--
ALTER TABLE `blocked_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_to_user_foreign` (`to_user`),
  ADD KEY `notifications_created_by_id_foreign` (`created_by_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_comments`
--
ALTER TABLE `post_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_likes`
--
ALTER TABLE `post_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_share`
--
ALTER TABLE `post_share`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_sharedby_users`
--
ALTER TABLE `post_sharedby_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_collections`
--
ALTER TABLE `product_collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review_rating`
--
ALTER TABLE `review_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_collection`
--
ALTER TABLE `shop_collection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tracking`
--
ALTER TABLE `tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_otps`
--
ALTER TABLE `user_otps`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addcards`
--
ALTER TABLE `addcards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `app_logins`
--
ALTER TABLE `app_logins`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT for table `baby_profiles`
--
ALTER TABLE `baby_profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;
--
-- AUTO_INCREMENT for table `blocked_users`
--
ALTER TABLE `blocked_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
--
-- AUTO_INCREMENT for table `post_comments`
--
ALTER TABLE `post_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `post_likes`
--
ALTER TABLE `post_likes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `post_share`
--
ALTER TABLE `post_share`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `post_sharedby_users`
--
ALTER TABLE `post_sharedby_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_collections`
--
ALTER TABLE `product_collections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `review_rating`
--
ALTER TABLE `review_rating`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shop_collection`
--
ALTER TABLE `shop_collection`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tracking`
--
ALTER TABLE `tracking`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=307;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;
--
-- AUTO_INCREMENT for table `user_otps`
--
ALTER TABLE `user_otps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_created_by_id_foreign` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_to_user_foreign` FOREIGN KEY (`to_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
